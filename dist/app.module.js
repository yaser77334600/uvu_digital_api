"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const classifications_module_1 = require("./modules/classifications/classifications.module");
const mongoose_1 = require("@nestjs/mongoose");
const auth_module_1 = require("./modules/auth/auth.module");
const users_module_1 = require("./modules/users/users.module");
const languages_module_1 = require("./modules/languages/languages.module");
const genres_module_1 = require("./modules/genres/genres.module");
const persons_module_1 = require("./modules/persons/persons.module");
const multimedia_module_1 = require("./modules/multimedia/multimedia.module");
const files_module_1 = require("./modules/files/files.module");
const subscriptions_module_1 = require("./modules/subscriptions/subscriptions.module");
const transcoder_module_1 = require("./modules/transcoder/transcoder.module");
const tasks_module_1 = require("./modules/tasks/tasks.module");
const activation_module_1 = require("./modules/activation/activation.module");
const reproduction_module_1 = require("./modules/reproduction/reproduction.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [classifications_module_1.ClassificationsModule,
            mongoose_1.MongooseModule.forRoot(process.env.URIDB, { useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true }),
            auth_module_1.AuthModule,
            users_module_1.UsersModule,
            languages_module_1.LanguagesModule,
            genres_module_1.GenresModule,
            persons_module_1.PersonsModule,
            multimedia_module_1.MultimediaModule,
            files_module_1.FilesModule,
            subscriptions_module_1.SubscriptionsModule,
            transcoder_module_1.TranscoderModule,
            tasks_module_1.TasksModule,
            activation_module_1.ActivationModule,
            reproduction_module_1.ReproductionModule,],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map