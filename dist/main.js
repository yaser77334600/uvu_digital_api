"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const common_1 = require("@nestjs/common");
const jwt_error_filter_1 = require("./common/filters/jwt-error.filter");
const helmet = require("helmet");
const compression = require("compression");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.use(helmet());
    app.enableCors();
    app.useGlobalFilters(new jwt_error_filter_1.JwtErrorFilter());
    app.useGlobalPipes(new common_1.ValidationPipe());
    app.use(compression());
    await app.listen(process.env.PORT);
}
bootstrap();
//# sourceMappingURL=main.js.map