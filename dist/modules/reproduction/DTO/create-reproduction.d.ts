export declare class CreateReproduction {
    idCotenido: string;
    videoView: boolean;
    watchTime: number;
    fecha: Date;
}
