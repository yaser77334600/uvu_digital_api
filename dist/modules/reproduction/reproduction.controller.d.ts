import { ReproductionService } from './reproduction.service';
import { CreateReproduction } from './DTO/create-reproduction';
import { Reproduction } from './interfaces/reproduction.interface';
export declare class ReproductionController {
    private reproductionAPI;
    constructor(reproductionAPI: ReproductionService);
    getPeople(query: any): Promise<{
        reproduction: Reproduction[];
    }>;
    CreateReproduction(reproduction: CreateReproduction): Promise<Reproduction>;
}
