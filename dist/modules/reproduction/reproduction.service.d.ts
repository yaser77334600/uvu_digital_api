import { Model } from 'mongoose';
import { Reproduction } from './interfaces/reproduction.interface';
import { CreateReproduction } from './DTO/create-reproduction';
export declare class ReproductionService {
    private ReprocutionModel;
    constructor(ReprocutionModel: Model<Reproduction>);
    getReproduction(query?: any): Promise<Reproduction[]>;
    postReproduction(Reproduction: CreateReproduction): Promise<Reproduction>;
}
