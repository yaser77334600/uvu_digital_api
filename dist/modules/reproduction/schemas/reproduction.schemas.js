"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.ReproductionSchema = new mongoose_1.Schema({
    idCotenido: {
        type: String,
        required: true,
        unique: true,
    },
    videoView: {
        type: Boolean,
        required: true,
    },
    watchTime: {
        type: Number,
        required: true,
    },
    fecha: {
        type: Date,
        required: true,
    }
});
//# sourceMappingURL=reproduction.schemas.js.map