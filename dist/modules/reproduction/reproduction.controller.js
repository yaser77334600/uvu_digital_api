"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const reproduction_service_1 = require("./reproduction.service");
const auth_guard_1 = require("../../common/guard/auth.guard");
const mongo_error_filter_1 = require("../../common/filters/mongo-error.filter");
const match_query_pipe_1 = require("../../common/pipes/match-query.pipe");
const uuid_pipe_1 = require("../../common/pipes/uuid.pipe");
const pick_pipe_1 = require("../../common/pipes/pick.pipe");
const create_reproduction_1 = require("./DTO/create-reproduction");
let ReproductionController = class ReproductionController {
    constructor(reproductionAPI) {
        this.reproductionAPI = reproductionAPI;
    }
    async getPeople(query) {
        console.log(query);
        const reproduction = await this.reproductionAPI.getReproduction(query);
        return { reproduction };
    }
    async CreateReproduction(reproduction) {
        console.log(reproduction);
        return await this.reproductionAPI.postReproduction(reproduction);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Query(new match_query_pipe_1.MatchQueryPipe(['videoView', 'watchTime']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ReproductionController.prototype, "getPeople", null);
__decorate([
    common_1.Post(),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter),
    __param(0, common_1.Body(new uuid_pipe_1.UuidPipe(), new pick_pipe_1.PickPipe(['idCotenido', 'videoView', 'watchTime', 'fecha']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_reproduction_1.CreateReproduction]),
    __metadata("design:returntype", Promise)
], ReproductionController.prototype, "CreateReproduction", null);
ReproductionController = __decorate([
    common_1.Controller('reproduction'),
    __metadata("design:paramtypes", [reproduction_service_1.ReproductionService])
], ReproductionController);
exports.ReproductionController = ReproductionController;
//# sourceMappingURL=reproduction.controller.js.map