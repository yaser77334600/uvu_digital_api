/// <reference types="mongodb" />
import { Model } from 'mongoose';
import { Activation } from './interfaces/activation.interface';
import { User } from '../users/interfaces/user.interface';
import { Postman } from '../postman/interfaces/postman.interface';
import { PostmanService } from '../postman/postman.service';
export declare class ActivationService {
    private ActivationModel;
    private PostmanAPI;
    constructor(ActivationModel: Model<Activation>, PostmanAPI: PostmanService);
    getActivation(options: any): Promise<Activation>;
    deleteActivation(options: any): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
    createActivation(user: User): Promise<Activation>;
    resendActivation(user: User): Promise<Postman>;
    sendActivation(user: User, activation: Activation): Promise<Postman>;
}
