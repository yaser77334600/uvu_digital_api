"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const moment = require("moment");
const postman_service_1 = require("../postman/postman.service");
let ActivationService = class ActivationService {
    constructor(ActivationModel, PostmanAPI) {
        this.ActivationModel = ActivationModel;
        this.PostmanAPI = PostmanAPI;
    }
    async getActivation(options) {
        return await this.ActivationModel.findOne(options);
    }
    async deleteActivation(options) {
        return await this.ActivationModel.deleteOne(options);
    }
    async createActivation(user) {
        const hash = Math.floor(100000 + Math.random() * 900000);
        const expiresOn = moment(new Date()).add(30, 'minutes').toISOString();
        const activation = new this.ActivationModel({ user: user._id, hash, expiresOn });
        return await activation.save();
    }
    async resendActivation(user) {
        await this.deleteActivation({ user: user._id });
        const newActivation = await this.createActivation(user);
        return await this.sendActivation(user, newActivation);
    }
    async sendActivation(user, activation) {
        const mail = {
            to: user.email,
            from: 'no-reply@uvu.digital',
            subject: `Activation code UVU`,
            body: `${activation.hash}`,
        };
        const postman = await this.PostmanAPI.createMail(mail);
        return postman;
    }
};
ActivationService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('Activation')),
    __metadata("design:paramtypes", [mongoose_2.Model, postman_service_1.PostmanService])
], ActivationService);
exports.ActivationService = ActivationService;
//# sourceMappingURL=activation.service.js.map