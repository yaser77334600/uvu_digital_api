"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.ActivationSchema = new mongoose_1.Schema({
    user: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'Invalid user'],
    },
    hash: {
        type: Number,
        required: [true, 'Invalid hash'],
        unique: true,
    },
    expiresOn: {
        type: Date,
        required: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
});
//# sourceMappingURL=activation.schema.js.map