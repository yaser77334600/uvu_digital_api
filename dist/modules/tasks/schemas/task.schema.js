"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.TaskSchema = new mongoose_1.Schema({
    jobID: {
        type: String,
    },
    source: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Multimedia',
        required: true,
    },
    transcoded: {
        type: String,
    },
    completed: {
        type: Boolean,
        default: false,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    isMaster: {
        type: Boolean,
        default: false,
    },
    isUploaded: {
        type: Boolean,
        default: false,
    },
});
//# sourceMappingURL=task.schema.js.map