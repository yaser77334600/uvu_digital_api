"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const tasks_service_1 = require("./tasks.service");
const mongoose_1 = require("@nestjs/mongoose");
const task_schema_1 = require("./schemas/task.schema");
const nest_schedule_1 = require("nest-schedule");
const transcoder_module_1 = require("../transcoder/transcoder.module");
const files_module_1 = require("../files/files.module");
let TasksModule = class TasksModule {
};
TasksModule = __decorate([
    common_1.Global(),
    common_1.Module({
        providers: [tasks_service_1.TasksService],
        imports: [nest_schedule_1.ScheduleModule.register(), mongoose_1.MongooseModule.forFeature([{ name: 'Task', schema: task_schema_1.TaskSchema }]), transcoder_module_1.TranscoderModule,
            files_module_1.FilesModule],
        exports: [tasks_service_1.TasksService],
    })
], TasksModule);
exports.TasksModule = TasksModule;
//# sourceMappingURL=tasks.module.js.map