import { Model } from 'mongoose';
import { Task } from './interfaces/task.interface';
import { NestSchedule } from 'nest-schedule';
import { TranscoderService } from '../transcoder/transcoder.service';
import { MultimediaService } from '../multimedia/multimedia.service';
import { SourcesService } from '../multimedia/sources.service';
import { FilesService } from '../files/files.service';
export declare class TasksService extends NestSchedule {
    private TaskModel;
    private transcoderAPI;
    private multimediaAPI;
    private sourceAPI;
    private filesAPI;
    constructor(TaskModel: Model<Task>, transcoderAPI: TranscoderService, multimediaAPI: MultimediaService, sourceAPI: SourcesService, filesAPI: FilesService);
    createTask(task: any): Promise<Task>;
    getTasks(query: any): Promise<Task[]>;
    getList(): Promise<void>;
    search(): Promise<boolean>;
}
