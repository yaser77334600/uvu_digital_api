"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const nest_schedule_1 = require("nest-schedule");
const transcoder_service_1 = require("../transcoder/transcoder.service");
const multimedia_service_1 = require("../multimedia/multimedia.service");
const sources_service_1 = require("../multimedia/sources.service");
const files_service_1 = require("../files/files.service");
let TasksService = class TasksService extends nest_schedule_1.NestSchedule {
    constructor(TaskModel, transcoderAPI, multimediaAPI, sourceAPI, filesAPI) {
        super();
        this.TaskModel = TaskModel;
        this.transcoderAPI = transcoderAPI;
        this.multimediaAPI = multimediaAPI;
        this.sourceAPI = sourceAPI;
        this.filesAPI = filesAPI;
    }
    async createTask(task) {
        const taskDB = new this.TaskModel(task);
        return await taskDB.save();
    }
    async getTasks(query) {
        return await this.TaskModel.find(query);
    }
    async getList() {
        this.search();
    }
    async search() {
        const task = await this.TaskModel.findOne({ completed: false });
        if (!task) {
            return true;
        }
        if (!task.isUploaded && task.isMaster) {
            const multimedia = await this.multimediaAPI.getMedia({ _id: task.source });
            const tmpMasterFile = multimedia.file.split('/');
            const masterName = tmpMasterFile.pop();
            const masterExist = await this.filesAPI.fileExist(tmpMasterFile.join('/'), masterName, false);
            if (masterExist) {
                task.isUploaded = true;
                const output = 'movies/' + multimedia._id.toString() + '/transcoded/';
                const jobID = await this.transcoderAPI.createJob(multimedia.file, output, masterName.split('.')[0]);
                task.jobID = jobID.Job.Id;
                await task.save();
                common_1.Logger.log('Tarea actualizada');
            }
            else {
                return true;
            }
        }
        else {
            const status = await this.transcoderAPI.getJobStatus(task.jobID);
            if (!status) {
                return true;
            }
            if (status.Job.Status === 'Complete') {
                const transcoded = status.Job.OutputKeyPrefix;
                const query = task.isMaster ? { _id: task.source } : { 'sources._id': task.source };
                const multimedia = await this.multimediaAPI.getMedia(query);
                if (!multimedia) {
                    return true;
                }
                if (!task.isMaster) {
                    const source = multimedia.sources.find(s => s.jobID === task.jobID);
                    if (!source) {
                        return true;
                    }
                    source.audioTranscoded = `${transcoded}${status.Job.Output.Key}.m3u8`;
                }
                else {
                    multimedia.transcoded = `${transcoded}index.m3u8`;
                }
                task.completed = true;
                task.transcoded = transcoded;
                multimedia.transcoding = false;
                await task.save();
                await multimedia.save();
                this.sourceAPI.rebuildMasterList(multimedia);
                common_1.Logger.log(`${multimedia.name}, has been successfully transcoded`);
            }
            return true;
        }
    }
};
__decorate([
    nest_schedule_1.Interval(30000, { waiting: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], TasksService.prototype, "getList", null);
TasksService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('Task')),
    __metadata("design:paramtypes", [mongoose_2.Model, transcoder_service_1.TranscoderService,
        multimedia_service_1.MultimediaService, sources_service_1.SourcesService, files_service_1.FilesService])
], TasksService);
exports.TasksService = TasksService;
//# sourceMappingURL=tasks.service.js.map