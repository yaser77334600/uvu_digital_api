"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.GenreSchema = new mongoose_1.Schema({
    name: {
        type: String,
        minlength: 1,
        maxlength: 29,
        required: true,
        unique: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    featuredMedia: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: 'Multimedia',
        }],
});
//# sourceMappingURL=genres.schema.js.map