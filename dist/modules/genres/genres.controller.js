"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const validate_object_id_pipe_1 = require("../../common/pipes/validate-object-id.pipe");
const match_query_pipe_1 = require("../../common/pipes/match-query.pipe");
const mongo_error_filter_1 = require("../../common/filters/mongo-error.filter");
const auth_guard_1 = require("../../common/guard/auth.guard");
const role_guard_1 = require("../../common/guard/role.guard");
const create_genre_dto_1 = require("./DTO/create-genre.dto");
const update_genre_dto_1 = require("./DTO/update-genre.dto");
const genres_service_1 = require("./genres.service");
const multimedia_service_1 = require("../multimedia/multimedia.service");
let GenresController = class GenresController {
    constructor(genreAPI, mediaAPI) {
        this.genreAPI = genreAPI;
        this.mediaAPI = mediaAPI;
    }
    async getGenres(query) {
        const genresRaw = await this.genreAPI.getGenres(query);
        const genres = [];
        for (const genre of genresRaw) {
            const count = await this.mediaAPI.countMedia({ options: { status: 'published', active: true, genres: { $in: [genre._id] } } });
            if (count > 0) {
                genres.push(genre);
            }
        }
        return genres;
    }
    async getGenre(genreID) {
        const genre = await this.genreAPI.getGenre({ _id: genreID, active: true });
        if (!genre) {
            throw new common_1.NotFoundException('Genre not found', '5001');
        }
        return genre;
    }
    async createGenre(genre) {
        return await this.genreAPI.createGenre(genre);
    }
    async updateGenre(genreID, genre) {
        const genreDB = await this.genreAPI.getGenre({ _id: genreID, active: true });
        if (!genreDB) {
            throw new common_1.NotFoundException('Genre not found', '5001');
        }
        if (genre.name && genre.name !== '') {
            const exist = await this.genreAPI.getGenre({ name: genre.name.trim() });
            if (exist) {
                throw new common_1.ConflictException('This genre already exist', '5001');
            }
            genreDB.name = genre.name.trim();
        }
        if (genre.featuredMedia && genre.featuredMedia.length > 0) {
            const uniqueIDs = new Set(genre.featuredMedia);
            const IDsDB = genreDB.featuredMedia.map(media => media._id.toString());
            for (const uniqueID of uniqueIDs.values()) {
                if (IDsDB.indexOf(uniqueID) === -1) {
                    genreDB.featuredMedia.push(uniqueID);
                }
            }
        }
        if (genre.removeFromFeatured && genre.removeFromFeatured.length > 0) {
            const uniqueIDs = new Set(genre.removeFromFeatured);
            const IDsDB = genreDB.featuredMedia.map(media => media._id.toString());
            for (const uniqueID of uniqueIDs.values()) {
                const pos = IDsDB.indexOf(uniqueID);
                if (pos !== -1) {
                    genreDB.featuredMedia.splice(pos, 1);
                }
            }
        }
        return await genreDB.save();
    }
    async deleteGenre(genreID) {
        return await this.genreAPI.deleteGenre(genreID);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Query(new match_query_pipe_1.MatchQueryPipe(['name']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], GenresController.prototype, "getGenres", null);
__decorate([
    common_1.Get(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], GenresController.prototype, "getGenre", null);
__decorate([
    common_1.Post(),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_genre_dto_1.GenreDTO]),
    __metadata("design:returntype", Promise)
], GenresController.prototype, "createGenre", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, update_genre_dto_1.UpdateGenreDTO]),
    __metadata("design:returntype", Promise)
], GenresController.prototype, "updateGenre", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN'])),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], GenresController.prototype, "deleteGenre", null);
GenresController = __decorate([
    common_1.Controller('genres'),
    __metadata("design:paramtypes", [genres_service_1.GenresService, multimedia_service_1.MultimediaService])
], GenresController);
exports.GenresController = GenresController;
//# sourceMappingURL=genres.controller.js.map