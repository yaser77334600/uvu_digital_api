"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
let GenresService = class GenresService {
    constructor(GenreModel) {
        this.GenreModel = GenreModel;
    }
    async getGenres(query) {
        const { limit, skip, options } = query;
        return await this.GenreModel.find(options)
            .limit(Number(limit) > 100 ? 100 : Number(limit))
            .skip(Math.abs(Number(skip)) || 0)
            .populate({
            path: 'featuredMedia',
            select: 'name coverHorizontal coverVertical description',
            match: { status: 'published' },
        });
    }
    async getGenre(options) {
        return await this.GenreModel.findOne(options)
            .populate({
            path: 'featuredMedia',
            select: 'name coverHorizontal coverVertical description',
            match: { status: 'published' },
        });
    }
    async createGenre(gender) {
        const genderDB = new this.GenreModel(gender);
        return await genderDB.save();
    }
    async updateGenre(gender, query) {
        return await this.GenreModel.findOneAndUpdate(query, gender, { new: true, runValidators: true, context: 'query' })
            .populate({
            path: 'featuredMedia',
            select: 'name coverHorizontal coverVertical description',
            match: { status: 'published' },
        });
    }
    async deleteGenre(ID) {
        return await this.GenreModel.findOneAndDelete({ _id: ID });
    }
};
GenresService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Genre')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], GenresService);
exports.GenresService = GenresService;
//# sourceMappingURL=genres.service.js.map