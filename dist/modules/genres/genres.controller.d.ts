import { Genre } from './interfaces/genre.interface';
import { GenreDTO } from './DTO/create-genre.dto';
import { UpdateGenreDTO } from './DTO/update-genre.dto';
import { GenresService } from './genres.service';
import { MultimediaService } from '../multimedia/multimedia.service';
export declare class GenresController {
    private genreAPI;
    private mediaAPI;
    constructor(genreAPI: GenresService, mediaAPI: MultimediaService);
    getGenres(query: any): Promise<Genre[]>;
    getGenre(genreID: any): Promise<Genre>;
    createGenre(genre: GenreDTO): Promise<Genre>;
    updateGenre(genreID: any, genre: UpdateGenreDTO): Promise<Genre>;
    deleteGenre(genreID: any): Promise<Genre>;
}
