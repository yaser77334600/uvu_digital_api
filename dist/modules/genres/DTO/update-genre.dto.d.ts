export declare class UpdateGenreDTO {
    name: string;
    featuredMedia: string[];
    removeFromFeatured: string[];
}
