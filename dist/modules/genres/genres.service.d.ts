import { Model } from 'mongoose';
import { GenreDTO } from './DTO/create-genre.dto';
import { Genre } from './interfaces/genre.interface';
export declare class GenresService {
    private GenreModel;
    constructor(GenreModel: Model<Genre>);
    getGenres(query?: any): Promise<Genre[]>;
    getGenre(options: any): Promise<Genre>;
    createGenre(gender: GenreDTO): Promise<Genre>;
    updateGenre(gender: GenreDTO, query: any): Promise<Genre>;
    deleteGenre(ID: string): Promise<Genre>;
}
