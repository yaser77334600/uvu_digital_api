import { CreateClassificationDTO } from './DTO/create-classification.dto';
import { ClassificationsService } from './classifications.service';
import { Classification } from './interfaces/classification.interface';
export declare class ClassificationsController {
    private classificationAPI;
    constructor(classificationAPI: ClassificationsService);
    getClassifications(query: any): Promise<Classification[]>;
    getClassification(classificationID: any): Promise<Classification>;
    createClassification(classification: CreateClassificationDTO): Promise<Classification>;
    updateClassification(classificationID: any, classification: CreateClassificationDTO): Promise<Classification>;
    deleteClassification(classificationID: any): Promise<Classification>;
}
