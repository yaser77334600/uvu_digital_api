import { Classification } from './interfaces/classification.interface';
import { Model } from 'mongoose';
import { CreateClassificationDTO } from './DTO/create-classification.dto';
export declare class ClassificationsService {
    private ClassificationModel;
    constructor(ClassificationModel: Model<Classification>);
    getClassifications(query?: any): Promise<Classification[]>;
    getClassification(options: any): Promise<Classification>;
    createClassification(classification: CreateClassificationDTO): Promise<Classification>;
    updateClassification(classification: CreateClassificationDTO, ID: any): Promise<Classification>;
    deleteClassification(ID: string): Promise<Classification>;
}
