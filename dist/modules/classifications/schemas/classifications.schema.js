"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.ClassificationSchema = new mongoose_1.Schema({
    name: {
        type: String,
        minlength: 1,
        maxlength: 29,
        required: true,
        unique: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
});
//# sourceMappingURL=classifications.schema.js.map