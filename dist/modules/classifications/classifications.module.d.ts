import { NestModule, MiddlewareConsumer } from '@nestjs/common';
export declare class ClassificationsModule implements NestModule {
    configure(consumer: MiddlewareConsumer): void;
}
