"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const classifications_controller_1 = require("./classifications.controller");
const auth_middleware_1 = require("../../middlewares/auth.middleware");
const classifications_service_1 = require("./classifications.service");
const mongoose_1 = require("@nestjs/mongoose");
const classifications_schema_1 = require("./schemas/classifications.schema");
let ClassificationsModule = class ClassificationsModule {
    configure(consumer) {
        consumer.apply(auth_middleware_1.AuthMiddleware).forRoutes('classifications');
    }
};
ClassificationsModule = __decorate([
    common_1.Module({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: 'Classification', schema: classifications_schema_1.ClassificationSchema }])],
        controllers: [classifications_controller_1.ClassificationsController],
        providers: [classifications_service_1.ClassificationsService],
    })
], ClassificationsModule);
exports.ClassificationsModule = ClassificationsModule;
//# sourceMappingURL=classifications.module.js.map