"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const create_classification_dto_1 = require("./DTO/create-classification.dto");
const classifications_service_1 = require("./classifications.service");
const validate_object_id_pipe_1 = require("../../common/pipes/validate-object-id.pipe");
const match_query_pipe_1 = require("../../common/pipes/match-query.pipe");
const mongo_error_filter_1 = require("../../common/filters/mongo-error.filter");
const auth_guard_1 = require("../../common/guard/auth.guard");
const role_guard_1 = require("../../common/guard/role.guard");
let ClassificationsController = class ClassificationsController {
    constructor(classificationAPI) {
        this.classificationAPI = classificationAPI;
    }
    async getClassifications(query) {
        return await this.classificationAPI.getClassifications(query);
    }
    async getClassification(classificationID) {
        const classification = await this.classificationAPI.getClassification({ _id: classificationID, active: true });
        if (!classification) {
            throw new common_1.NotFoundException('Classification not found', '3001');
        }
        return classification;
    }
    async createClassification(classification) {
        return await this.classificationAPI.createClassification(classification);
    }
    async updateClassification(classificationID, classification) {
        const exist = await this.classificationAPI.getClassification({ name: classification.name, active: true });
        if (exist) {
            throw new common_1.ConflictException('This classification already exist', '3002');
        }
        return await this.classificationAPI.updateClassification(classification, classificationID);
    }
    async deleteClassification(classificationID) {
        return await this.classificationAPI.deleteClassification(classificationID);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Query(new match_query_pipe_1.MatchQueryPipe(['']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ClassificationsController.prototype, "getClassifications", null);
__decorate([
    common_1.Get(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ClassificationsController.prototype, "getClassification", null);
__decorate([
    common_1.Post(),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_classification_dto_1.CreateClassificationDTO]),
    __metadata("design:returntype", Promise)
], ClassificationsController.prototype, "createClassification", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_classification_dto_1.CreateClassificationDTO]),
    __metadata("design:returntype", Promise)
], ClassificationsController.prototype, "updateClassification", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ClassificationsController.prototype, "deleteClassification", null);
ClassificationsController = __decorate([
    common_1.Controller('classifications'),
    __metadata("design:paramtypes", [classifications_service_1.ClassificationsService])
], ClassificationsController);
exports.ClassificationsController = ClassificationsController;
//# sourceMappingURL=classifications.controller.js.map