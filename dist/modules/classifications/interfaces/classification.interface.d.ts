import { Document } from 'mongoose';
import { Pagination } from '../../../common/interfaces/pagination.interface';
export interface Classification extends Document, Pagination {
    name: string;
    active: boolean;
    _id: string;
}
