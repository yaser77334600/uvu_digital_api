"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const users_service_1 = require("./users.service");
const activation_service_1 = require("../activation/activation.service");
const create_recovery_dto_1 = require("./DTO/create-recovery.dto");
const pick_pipe_1 = require("../../common/pipes/pick.pipe");
const recovery_service_1 = require("./recovery.service");
const change_password_dto_1 = require("./DTO/change-password.dto");
const moment = require("moment");
const password_encrypt_pipe_1 = require("../../common/pipes/password-encrypt.pipe");
let RecoveryController = class RecoveryController {
    constructor(userAPI, activationAPI, recoveryAPI) {
        this.userAPI = userAPI;
        this.activationAPI = activationAPI;
        this.recoveryAPI = recoveryAPI;
    }
    async sendRecovery(recover, res) {
        const user = await this.userAPI.getUser({ email: recover.email, active: true });
        if (!user) {
            throw new common_1.NotFoundException('User not found', '1001');
        }
        const recoveries = await this.recoveryAPI.getRecoveries({ user: user._id });
        if (recoveries.length >= 3) {
            throw new common_1.BadRequestException('Maximum number of emails exceeded', '0008');
        }
        const mail = await this.recoveryAPI.resendRecovery(user);
        if (!mail) {
            throw new common_1.GatewayTimeoutException('Could not send mail.', 'A005');
        }
        return res.json({ sended: true });
    }
    async changePassword(recover, res) {
        const recovery = await this.recoveryAPI.getRecovery({ hash: recover.hash, active: true });
        if (!recovery) {
            throw new common_1.BadRequestException('Invalid activation token', '0004');
        }
        const now = moment(new Date());
        const expires = moment(recovery.expiresOn);
        const diff = expires.diff(now, 'minutes');
        if (diff <= 0) {
            await this.recoveryAPI.deleteRecovery({ hash: recovery.hash });
            throw new common_1.BadRequestException('Code Expired', '0005');
        }
        await this.recoveryAPI.deleteRecovery({ hash: recovery.hash });
        await this.userAPI.updateUser({ _id: recovery.user._id }, { password: recover.password });
        return res.json({ changed: true });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Body(new pick_pipe_1.PickPipe(['email']))), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_recovery_dto_1.CreateRecoveryDTO, Object]),
    __metadata("design:returntype", Promise)
], RecoveryController.prototype, "sendRecovery", null);
__decorate([
    common_1.Put(),
    __param(0, common_1.Body(new password_encrypt_pipe_1.PasswordEncryptPipe())), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [change_password_dto_1.ChangePasswordDTO, Object]),
    __metadata("design:returntype", Promise)
], RecoveryController.prototype, "changePassword", null);
RecoveryController = __decorate([
    common_1.Controller('recovery'),
    __metadata("design:paramtypes", [users_service_1.UsersService, activation_service_1.ActivationService,
        recovery_service_1.RecoveryService])
], RecoveryController);
exports.RecoveryController = RecoveryController;
//# sourceMappingURL=recovery.controller.js.map