import { UsersService } from './users.service';
import { ActivationService } from '../activation/activation.service';
import { CreateRecoveryDTO } from './DTO/create-recovery.dto';
import { RecoveryService } from './recovery.service';
import { ChangePasswordDTO } from './DTO/change-password.dto';
export declare class RecoveryController {
    private userAPI;
    private activationAPI;
    private recoveryAPI;
    constructor(userAPI: UsersService, activationAPI: ActivationService, recoveryAPI: RecoveryService);
    sendRecovery(recover: CreateRecoveryDTO, res: any): Promise<any>;
    changePassword(recover: ChangePasswordDTO, res: any): Promise<any>;
}
