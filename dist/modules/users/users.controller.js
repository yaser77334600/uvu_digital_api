"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const validate_object_id_pipe_1 = require("../../common/pipes/validate-object-id.pipe");
const users_service_1 = require("./users.service");
const create_user_dto_1 = require("./DTO/create-user.dto");
const uuid_pipe_1 = require("../../common/pipes/uuid.pipe");
const password_encrypt_pipe_1 = require("../../common/pipes/password-encrypt.pipe");
const update_user_dto_1 = require("./DTO/update-user.dto");
const match_query_pipe_1 = require("../../common/pipes/match-query.pipe");
const pick_pipe_1 = require("../../common/pipes/pick.pipe");
const mongo_error_filter_1 = require("../../common/filters/mongo-error.filter");
const auth_guard_1 = require("../../common/guard/auth.guard");
const role_guard_1 = require("../../common/guard/role.guard");
const moment = require("moment");
const auth_service_1 = require("../auth/auth.service");
const activation_service_1 = require("../activation/activation.service");
const subscriptions_service_1 = require("../subscriptions/subscriptions.service");
const multimedia_service_1 = require("../multimedia/multimedia.service");
let UsersController = class UsersController {
    constructor(userAPI, activationAPI, authAPI, BillingAPI, multimediaAPI) {
        this.userAPI = userAPI;
        this.activationAPI = activationAPI;
        this.authAPI = authAPI;
        this.BillingAPI = BillingAPI;
        this.multimediaAPI = multimediaAPI;
    }
    async getUsers(query) {
        const users = await this.userAPI.getUsers(query);
        const total = await this.userAPI.countUsers(query);
        return { users, total };
    }
    async getUser(userID, req) {
        const tokenUser = req.token.user;
        if (tokenUser.role !== 'ADMIN' && tokenUser._id.toString() !== userID) {
            throw new common_1.UnauthorizedException('Insufficient permissions', '0007');
        }
        const user = await this.userAPI.getUser({ _id: userID, active: true });
        if (!user) {
            throw new common_1.NotFoundException('User not found', '1001');
        }
        return user;
    }
    async getOwnInfo(req) {
        const tokenUser = req.token.user;
        console.log(tokenUser + " entro aqui");
        let subscription;
        const user = await this.userAPI.getUser({ _id: tokenUser._id, active: true });
        if (!user) {
            throw new common_1.NotFoundException('User not found', '1001');
        }
        if (user.customer && user.customer !== '') {
            subscription = await this.BillingAPI.getCustomer(user.customer);
        }
        return { user, subscription };
    }
    async createUser(user) {
        const existUser = await this.userAPI.getUser({ email: user.email });
        if (existUser) {
            throw new common_1.ConflictException('This user already exist', '1002');
        }
        const userDB = await this.userAPI.createUser(user);
        const activation = await this.activationAPI.createActivation(userDB);
        await this.activationAPI.sendActivation(userDB, activation);
        return userDB;
    }
    async updateUser(userID, user, req) {
        const tokenUser = req.token.user;
        const userDB = await this.userAPI.getUser({ _id: tokenUser._id, active: true });
        if (!userDB) {
            throw new common_1.NotFoundException('User not found', '1001');
        }
        if (tokenUser._id.toString() !== userID && tokenUser.role !== 'ADMIN') {
            throw new common_1.UnauthorizedException('insufficient permissions', '0007');
        }
        if (tokenUser.role !== 'ADMIN') {
            delete user.role;
        }
        return await this.userAPI.updateUser({ _id: userID, active: true }, user);
    }
    async getMyList(req) {
        let exclude = '';
        const tokenUser = req.token.user;
        if (tokenUser.role === 'USER') {
            exclude += '-status -rejectedReason -jobID -transcoded -transcoding -insertBy';
        }
        const userDB = await this.userAPI.getUser({ _id: tokenUser._id, active: true });
        if (!userDB) {
            throw new common_1.NotFoundException('User not found', '1001');
        }
        return await this.multimediaAPI.getMultimedia({ options: { _id: { $in: userDB.myList }, active: true, status: 'published' } });
    }
    async addToMyList(multimediaID, req) {
        const tokenUser = req.token.user;
        const userDB = await this.userAPI.getUser({ _id: tokenUser._id, active: true });
        if (!userDB) {
            throw new common_1.NotFoundException('User not found', '1001');
        }
        const multimedia = await this.multimediaAPI.getMedia({ _id: multimediaID, active: true, status: 'published' });
        if (!multimedia) {
            throw new common_1.NotFoundException('Media not found', '8001');
        }
        const pos = userDB.myList.map(media => media._id.toString()).indexOf(multimediaID);
        if (pos === -1) {
            userDB.myList.push(multimediaID);
        }
        return await userDB.save();
    }
    async deleteFromMyList(multimediaID, req) {
        const tokenUser = req.token.user;
        const userDB = await this.userAPI.getUser({ _id: tokenUser._id, active: true });
        if (!userDB) {
            throw new common_1.NotFoundException('User not found', '1001');
        }
        return await this.userAPI.updateUser({ _id: userDB._id }, { $pull: { myList: multimediaID } });
    }
    async deleteUser(userID, req) {
        const tokenUser = req.token.user;
        if (tokenUser.role !== 'ADMIN' && tokenUser._id.toString() !== userID) {
            throw new common_1.UnauthorizedException('Insufficient permissions', '0007');
        }
        const userDB = await this.userAPI.getUser({ _id: userID, active: true });
        if (!userDB) {
            throw new common_1.NotFoundException('User not found', '1001');
        }
        if (userDB.customer) {
            await this.BillingAPI.deleteSub(userDB.customer);
        }
        userDB.active = false;
        return await userDB.save();
    }
    async verifyEmailUser(userID, hash) {
        const user = await this.userAPI.getUser({ _id: userID, active: true });
        if (!user) {
            throw new common_1.NotFoundException('User not found', '1001');
        }
        if (user.verifiedEmail) {
            throw new common_1.BadRequestException('This user is already verified', '0003');
        }
        const activation = await this.activationAPI.getActivation({ hash, active: true });
        if (!activation) {
            throw new common_1.BadRequestException('Invalid activation token', '0004');
        }
        if (activation.user.toString() !== user._id.toString()) {
            throw new common_1.BadRequestException('Invalid activation token', '0004');
        }
        const now = moment(new Date());
        const expires = moment(activation.expiresOn);
        const diff = expires.diff(now, 'minutes');
        if (diff <= 0) {
            await this.activationAPI.deleteActivation({ hash });
            throw new common_1.BadRequestException('Code Expired', '0005');
        }
        await this.activationAPI.deleteActivation({ hash });
        const userDB = await this.userAPI.updateUser({ _id: user._id }, { verifiedEmail: true });
        return { auth: this.authAPI.getToken(userDB) };
    }
    async resendCode(userID) {
        const user = await this.userAPI.getUser({ _id: userID, active: true });
        if (!user) {
            throw new common_1.NotFoundException('User not found', '1001');
        }
        if (user.verifiedEmail) {
            throw new common_1.BadRequestException('This user is already verified', '0003');
        }
        const mail = await this.activationAPI.resendActivation(user);
        if (!mail) {
            throw new common_1.GatewayTimeoutException('Failed to send activation email', '0005');
        }
        return { sended: true };
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN'])),
    __param(0, common_1.Query(new match_query_pipe_1.MatchQueryPipe(['name', 'email', 'country', 'address', 'phone']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getUsers", null);
__decorate([
    common_1.Get(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())), __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getUser", null);
__decorate([
    common_1.Get('/info/me'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getOwnInfo", null);
__decorate([
    common_1.Post(),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter),
    __param(0, common_1.Body(new uuid_pipe_1.UuidPipe(), new password_encrypt_pipe_1.PasswordEncryptPipe(), new pick_pipe_1.PickPipe(['name', 'email', 'phone', 'address', 'country', 'password', 'uuid']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_user_dto_1.CreateUserDTO]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "createUser", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __param(1, common_1.Body(new pick_pipe_1.PickPipe(['name', 'country', 'address', 'phone', 'role']))), __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_user_dto_1.UpdateUserDTO, Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "updateUser", null);
__decorate([
    common_1.Get('mylist/all'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getMyList", null);
__decorate([
    common_1.Put('mylist/:id'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())), __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "addToMyList", null);
__decorate([
    common_1.Delete('mylist/:id'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())), __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "deleteFromMyList", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())), __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "deleteUser", null);
__decorate([
    common_1.Get(':id/verify/:hash'),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())), __param(1, common_1.Param('hash')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "verifyEmailUser", null);
__decorate([
    common_1.Get(':id/resend'),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "resendCode", null);
UsersController = __decorate([
    common_1.Controller('users'),
    __metadata("design:paramtypes", [users_service_1.UsersService, activation_service_1.ActivationService, auth_service_1.AuthService,
        subscriptions_service_1.SubscriptionsService, multimedia_service_1.MultimediaService])
], UsersController);
exports.UsersController = UsersController;
//# sourceMappingURL=users.controller.js.map