"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.UserSchema = new mongoose_1.Schema({
    role: {
        type: String,
        enum: {
            values: ['ADMIN', 'CREATOR', 'USER'],
            message: 'Invalid type user',
        },
        default: 'USER',
    },
    name: {
        type: String,
        required: [true, 'name is required'],
        maxlength: 30,
        trim: true,
    },
    email: {
        type: String,
        required: [true, 'email is required'],
        unique: true,
        maxlength: 60,
        trim: true,
    },
    phone: {
        type: String,
        maxlength: 15,
    },
    country: {
        type: String,
    },
    verifiedEmail: {
        type: Boolean,
        default: false,
    },
    password: {
        type: String,
        required: [true, 'password is required'],
        trim: true,
        select: false,
    },
    active: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    uuid: {
        type: String,
        required: true,
    },
    address: {
        type: String,
        maxlength: 240,
        minlength: 5,
        trim: true,
        required: [true, 'invalid Address'],
    },
    customer: {
        type: String,
        required: false,
    },
    defaultLang: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Language',
        required: false,
    },
    current_period_end: {
        type: String,
        required: false,
    },
    lastActivity: {
        type: Date,
    },
    myList: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: 'Multimedia',
        }],
});
//# sourceMappingURL=users.schema.js.map