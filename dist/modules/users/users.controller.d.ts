import { UsersService } from './users.service';
import { CreateUserDTO } from './DTO/create-user.dto';
import { User } from './interfaces/user.interface';
import { UpdateUserDTO } from './DTO/update-user.dto';
import { AuthService } from '../auth/auth.service';
import { ActivationService } from '../activation/activation.service';
import { SubscriptionsService } from '../subscriptions/subscriptions.service';
import { MultimediaService } from '../multimedia/multimedia.service';
import { Multimedia } from '../multimedia/interfaces/multimedia.interface';
export declare class UsersController {
    private userAPI;
    private activationAPI;
    private authAPI;
    private BillingAPI;
    private multimediaAPI;
    constructor(userAPI: UsersService, activationAPI: ActivationService, authAPI: AuthService, BillingAPI: SubscriptionsService, multimediaAPI: MultimediaService);
    getUsers(query: any): Promise<{
        users: User[];
        total: number;
    }>;
    getUser(userID: string, req: any): Promise<User>;
    getOwnInfo(req: any): Promise<{
        user: User;
        subscription: any;
    }>;
    createUser(user: CreateUserDTO): Promise<User>;
    updateUser(userID: string, user: UpdateUserDTO, req: any): Promise<User>;
    getMyList(req: any): Promise<Multimedia[]>;
    addToMyList(multimediaID: string, req: any): Promise<User>;
    deleteFromMyList(multimediaID: string, req: any): Promise<User>;
    deleteUser(userID: any, req: any): Promise<User>;
    verifyEmailUser(userID: string, hash: string): Promise<{
        auth: string;
    }>;
    resendCode(userID: string): Promise<{
        sended: boolean;
    }>;
}
