"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
let UsersService = class UsersService {
    constructor(UserModel) {
        this.UserModel = UserModel;
    }
    async getUsers(query) {
        const { limit, skip, options } = query;
        return await this.UserModel.find(options)
            .limit(Number(limit) > 100 ? 100 : Number(limit))
            .skip(Math.abs(Number(skip)) || 0);
    }
    async countUsers(query) {
        const { options } = query;
        return await this.UserModel.countDocuments(options);
    }
    async getUser(query) {
        return await this.UserModel.findOne(query);
    }
    async getFullUser(query) {
        return await this.UserModel.findOne(query).select('+password');
    }
    async createUser(user) {
        const userDB = await new this.UserModel(user);
        return await userDB.save();
    }
    async updateUser(query, update) {
        return await this.UserModel.findOneAndUpdate(query, update, { new: true, runValidators: true, context: 'query' });
    }
    async deleteUser(ID) {
        return await this.UserModel.findOneAndUpdate({ _id: ID }, { active: false }, { new: true, runValidators: true, context: 'query' });
    }
};
UsersService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('User')),
    __metadata("design:paramtypes", [mongoose_2.Model])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map