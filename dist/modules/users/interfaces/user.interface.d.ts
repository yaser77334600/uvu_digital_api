import { Document } from 'mongoose';
import { Multimedia } from 'src/modules/multimedia/interfaces/multimedia.interface';
export interface User extends Document {
    role: string;
    name: string;
    email: string;
    verifiedEmail: boolean;
    password: string;
    active: boolean;
    createdOn: Date;
    uuid: string;
    phone: string;
    country: string;
    address: string;
    customer?: string;
    current_period_end?: any;
    lastActivity?: Date;
    myList: Multimedia[];
}
