export declare class CreateUserDTO {
    name: string;
    email: string;
    phone: string;
    country: string;
    address: string;
    password: string;
}
