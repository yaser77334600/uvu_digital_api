export declare class ChangePasswordDTO {
    hash: string;
    password: string;
}
