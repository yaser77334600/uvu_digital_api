export declare class UpdateUserDTO {
    name: string;
    phone?: string;
    country?: string;
    address?: string;
    role: string;
}
