"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const moment = require("moment");
const uuid = require("uuid");
const postman_service_1 = require("../postman/postman.service");
let RecoveryService = class RecoveryService {
    constructor(RecoverModel, PostmanAPI) {
        this.RecoverModel = RecoverModel;
        this.PostmanAPI = PostmanAPI;
    }
    async getRecoveries(query) {
        return await this.RecoverModel.find(query)
            .populate('user', 'name email');
    }
    async getRecovery(query) {
        return await this.RecoverModel.findOne(query)
            .populate('user', 'name email');
    }
    async deleteRecovery(options) {
        return await this.RecoverModel.findOneAndUpdate(options, { active: false }, { new: true, runValidators: true, context: 'query' });
    }
    async resendRecovery(user) {
        await this.deleteRecovery({ user: user._id, active: true });
        const newRecover = await this.createRecover(user);
        return await this.sendRecover(user, newRecover);
    }
    async createRecover(user) {
        const hash = uuid.v4();
        const expiresOn = moment(new Date()).add(30, 'minutes').toISOString();
        const recoverDB = new this.RecoverModel({ user: user._id, hash, expiresOn });
        return await recoverDB.save();
    }
    async sendRecover(user, recover) {
        const mail = {
            to: user.email,
            from: 'no-reply@uvu.digital',
            subject: 'Your Password Reset Request',
            body: `To reset the password for your ${user.email}, click \n the web address below or copy and paste it into your browser: \nhttps://uvu.digital/account/change-password/${recover.hash}`,
        };
        const postman = await this.PostmanAPI.createMail(mail);
        return postman;
    }
};
RecoveryService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('Recovery')),
    __metadata("design:paramtypes", [mongoose_2.Model, postman_service_1.PostmanService])
], RecoveryService);
exports.RecoveryService = RecoveryService;
//# sourceMappingURL=recovery.service.js.map