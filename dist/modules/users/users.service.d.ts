import { Model } from 'mongoose';
import { User } from './interfaces/user.interface';
import { CreateUserDTO } from './DTO/create-user.dto';
export declare class UsersService {
    private UserModel;
    constructor(UserModel: Model<User>);
    getUsers(query?: any): Promise<User[]>;
    countUsers(query?: any): Promise<number>;
    getUser(query: any): Promise<User>;
    getFullUser(query: any): Promise<User>;
    createUser(user: CreateUserDTO): Promise<User>;
    updateUser(query: any, update: any): Promise<User>;
    deleteUser(ID: string): Promise<User>;
}
