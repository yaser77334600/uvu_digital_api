import { Model } from 'mongoose';
import { Recover } from './interfaces/recover.interface';
import { User } from './interfaces/user.interface';
import { PostmanService } from '../postman/postman.service';
export declare class RecoveryService {
    private RecoverModel;
    private PostmanAPI;
    constructor(RecoverModel: Model<Recover>, PostmanAPI: PostmanService);
    getRecoveries(query?: any): Promise<Recover[]>;
    getRecovery(query?: any): Promise<Recover>;
    deleteRecovery(options: any): Promise<Recover>;
    resendRecovery(user: any): Promise<import("../postman/interfaces/postman.interface").Postman>;
    createRecover(user: User): Promise<Recover>;
    sendRecover(user: User, recover: Recover): Promise<import("../postman/interfaces/postman.interface").Postman>;
}
