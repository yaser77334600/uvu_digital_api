"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const languages_service_1 = require("./languages.service");
const validate_object_id_pipe_1 = require("../../common/pipes/validate-object-id.pipe");
const language_dto_1 = require("./DTO/language.dto");
const mongo_error_filter_1 = require("../../common/filters/mongo-error.filter");
const auth_guard_1 = require("../../common/guard/auth.guard");
const role_guard_1 = require("../../common/guard/role.guard");
let LanguagesController = class LanguagesController {
    constructor(languagesAPI) {
        this.languagesAPI = languagesAPI;
    }
    async getLanguages(query) {
        return await this.languagesAPI.getLanguages(query);
    }
    async getLanguage(languageID) {
        const language = await this.languagesAPI.getLanguage({ _id: languageID, active: true });
        if (!language) {
            throw new common_1.NotFoundException('Language not found', '6001');
        }
        return language;
    }
    async createLanguage(language) {
        console.log(language);
        return await this.languagesAPI.createLanguage(language);
    }
    async updateLanguage(languageID, language) {
        const exist = await this.languagesAPI.getLanguage({ code: language.code, active: true });
        if (exist) {
            throw new common_1.ConflictException('This language already exist', '6002');
        }
        return await this.languagesAPI.updateLanguage(language, { _id: languageID });
    }
    async deleteLanguage(languageID) {
        return await this.languagesAPI.deleteLanguage(languageID);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LanguagesController.prototype, "getLanguages", null);
__decorate([
    common_1.Get(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LanguagesController.prototype, "getLanguage", null);
__decorate([
    common_1.Post(),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [language_dto_1.LanguageDTO]),
    __metadata("design:returntype", Promise)
], LanguagesController.prototype, "createLanguage", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, language_dto_1.LanguageDTO]),
    __metadata("design:returntype", Promise)
], LanguagesController.prototype, "updateLanguage", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN'])),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LanguagesController.prototype, "deleteLanguage", null);
LanguagesController = __decorate([
    common_1.Controller('languages'),
    __metadata("design:paramtypes", [languages_service_1.LanguagesService])
], LanguagesController);
exports.LanguagesController = LanguagesController;
//# sourceMappingURL=languages.controller.js.map