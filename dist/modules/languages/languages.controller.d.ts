import { LanguagesService } from './languages.service';
import { Language } from './interfaces/language.interface';
import { LanguageDTO } from './DTO/language.dto';
export declare class LanguagesController {
    private languagesAPI;
    constructor(languagesAPI: LanguagesService);
    getLanguages(query: any): Promise<Language[]>;
    getLanguage(languageID: any): Promise<Language>;
    createLanguage(language: LanguageDTO): Promise<Language>;
    updateLanguage(languageID: any, language: LanguageDTO): Promise<Language>;
    deleteLanguage(languageID: any): Promise<Language>;
}
