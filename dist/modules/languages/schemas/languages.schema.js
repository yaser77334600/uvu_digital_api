"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.LanguageSchema = new mongoose_1.Schema({
    code: {
        type: String,
        maxlength: 3,
        minlength: 3,
        required: true,
        unique: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
});
//# sourceMappingURL=languages.schema.js.map