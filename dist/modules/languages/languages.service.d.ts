import { Model } from 'mongoose';
import { Language } from './interfaces/language.interface';
import { LanguageDTO } from './DTO/language.dto';
export declare class LanguagesService {
    private LanguageModel;
    constructor(LanguageModel: Model<Language>);
    getLanguages(query?: any): Promise<Language[]>;
    getLanguage(options: any): Promise<Language>;
    createLanguage(language: LanguageDTO): Promise<Language>;
    updateLanguage(language: LanguageDTO, query: any): Promise<Language>;
    deleteLanguage(ID: string): Promise<Language>;
}
