"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const persons_service_1 = require("./persons.service");
const match_query_pipe_1 = require("../../common/pipes/match-query.pipe");
const validate_object_id_pipe_1 = require("../../common/pipes/validate-object-id.pipe");
const create_person_dto_1 = require("./DTO/create-person.dto");
const pick_pipe_1 = require("../../common/pipes/pick.pipe");
const uuid_pipe_1 = require("../../common/pipes/uuid.pipe");
const mongo_error_filter_1 = require("../../common/filters/mongo-error.filter");
const update_person_dto_1 = require("./DTO/update-person.dto");
const files_service_1 = require("../files/files.service");
const valid_filed_extensions_interceptor_1 = require("../../common/interceptors/valid-filed-extensions.interceptor");
const auth_guard_1 = require("../../common/guard/auth.guard");
const role_guard_1 = require("../../common/guard/role.guard");
const path = require("path");
const uuid = require("uuid");
let PersonsController = class PersonsController {
    constructor(peopleAPI, filesService) {
        this.peopleAPI = peopleAPI;
        this.filesService = filesService;
    }
    async getPeople(query) {
        const people = await this.peopleAPI.getPeople(query);
        const total = await this.peopleAPI.countPeople(query);
        return { people, total };
    }
    async getPeopleByName(name) {
        return await this.peopleAPI.getPerson({ name });
    }
    async getPerson(personID) {
        const person = await this.peopleAPI.getPerson({ _id: personID, active: true });
        if (!person) {
            throw new common_1.NotFoundException('Person not found', '9001');
        }
        return person;
    }
    async createPerson(person) {
        return await this.peopleAPI.createPerson(person);
    }
    async updatePerson(personID, person) {
        return await this.peopleAPI.updatePerson(person, personID);
    }
    async deletePerson(personID) {
        return await this.peopleAPI.deletePerson(personID);
    }
    async uploadFile(file, id, res) {
        const person = await this.getPerson(id);
        if (!person) {
            throw new common_1.NotFoundException('Person not found', '9001');
        }
        file.originalname = uuid.v4() + path.extname(file.originalname);
        const uploaded = await this.filesService.uploadFile(file, `people/${person._id}/`, true);
        if (!uploaded) {
            throw new common_1.BadRequestException('Can\t upload', '7002');
        }
        const update = await this.peopleAPI.updatePerson(({ profileImg: uploaded.Key }), { _id: person._id });
        let existFile = false;
        let dir;
        if (person.profileImg) {
            dir = person.profileImg.split('/');
            existFile = await this.filesService.fileExist(dir.slice(0, 2).join('/'), dir[dir.length - 1], true);
        }
        if (existFile) {
            await this.filesService.deleteFile(dir.slice(0, 2).join('/'), dir[dir.length - 1], true);
            return res.json(update);
        }
        else {
            return res.json(update);
        }
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Query(new match_query_pipe_1.MatchQueryPipe(['category', 'name']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PersonsController.prototype, "getPeople", null);
__decorate([
    common_1.Get('byname/:name'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Param('name')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PersonsController.prototype, "getPeopleByName", null);
__decorate([
    common_1.Get(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PersonsController.prototype, "getPerson", null);
__decorate([
    common_1.Post(),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Body(new uuid_pipe_1.UuidPipe(), new pick_pipe_1.PickPipe(['name', 'genre', 'birthdate', 'biography', 'category', 'uuid']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_person_dto_1.CreatePersonDTO]),
    __metadata("design:returntype", Promise)
], PersonsController.prototype, "createPerson", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __param(1, common_1.Body(new pick_pipe_1.PickPipe(['name', 'genre', 'birthdate', 'category', 'biography']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, update_person_dto_1.UpdatePersonDTO]),
    __metadata("design:returntype", Promise)
], PersonsController.prototype, "updatePerson", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PersonsController.prototype, "deletePerson", null);
__decorate([
    common_1.Put(':id/upload'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file', { limits: { fileSize: 5000000 } }), new valid_filed_extensions_interceptor_1.ValidFiledExtensionsInterceptor(['image/jpeg', 'image/png'])),
    __param(0, common_1.UploadedFile('file')), __param(1, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())), __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], PersonsController.prototype, "uploadFile", null);
PersonsController = __decorate([
    common_1.Controller('persons'),
    __metadata("design:paramtypes", [persons_service_1.PersonsService, files_service_1.FilesService])
], PersonsController);
exports.PersonsController = PersonsController;
//# sourceMappingURL=persons.controller.js.map