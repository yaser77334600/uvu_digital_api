/// <reference types="multer" />
import { PersonsService } from './persons.service';
import { Person } from './interfaces/person.interface';
import { CreatePersonDTO } from './DTO/create-person.dto';
import { UpdatePersonDTO } from './DTO/update-person.dto';
import { FilesService } from '../files/files.service';
export declare class PersonsController {
    private peopleAPI;
    private filesService;
    constructor(peopleAPI: PersonsService, filesService: FilesService);
    getPeople(query: any): Promise<{
        people: Person[];
        total: number;
    }>;
    getPeopleByName(name: any): Promise<Person>;
    getPerson(personID: any): Promise<Person>;
    createPerson(person: CreatePersonDTO): Promise<Person>;
    updatePerson(personID: any, person: UpdatePersonDTO): Promise<Person>;
    deletePerson(personID: any): Promise<Person>;
    uploadFile(file: Express.Multer.File, id: any, res: any): Promise<any>;
}
