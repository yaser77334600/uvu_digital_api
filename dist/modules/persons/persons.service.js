"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
let PersonsService = class PersonsService {
    constructor(PersonModel) {
        this.PersonModel = PersonModel;
    }
    async getPeople(query) {
        const { limit, skip, options } = query;
        return await this.PersonModel.find(options)
            .limit(Number(limit) > 100 ? 100 : Number(limit))
            .skip(Math.abs(Number(skip)) || 0);
    }
    async countPeople(query) {
        const { options } = query;
        return await this.PersonModel.countDocuments(options);
    }
    async getPerson(options) {
        return await this.PersonModel.findOne(options);
    }
    async createPerson(person) {
        const personDB = await new this.PersonModel(person);
        return await personDB.save();
    }
    async updatePerson(person, query) {
        return await this.PersonModel.findOneAndUpdate(query, person, { new: true, runValidators: true, context: 'query' });
    }
    async deletePerson(ID) {
        const person = await this.getPerson({ _id: ID, active: true });
        const update = { active: false, name: person.name + '-disabled' };
        return await this.PersonModel.findOneAndUpdate({ _id: person._id }, update, { new: true, runValidators: true, context: 'query' });
    }
};
PersonsService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('Person')),
    __metadata("design:paramtypes", [mongoose_2.Model])
], PersonsService);
exports.PersonsService = PersonsService;
//# sourceMappingURL=persons.service.js.map