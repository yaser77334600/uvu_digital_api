import { Model } from 'mongoose';
import { Person } from './interfaces/person.interface';
import { CreatePersonDTO } from './DTO/create-person.dto';
import { UpdatePersonDTO } from './DTO/update-person.dto';
export declare class PersonsService {
    private PersonModel;
    constructor(PersonModel: Model<Person>);
    getPeople(query?: any): Promise<Person[]>;
    countPeople(query?: any): Promise<number>;
    getPerson(options: any): Promise<Person>;
    createPerson(person: CreatePersonDTO): Promise<Person>;
    updatePerson(person: UpdatePersonDTO | {}, query: any): Promise<Person>;
    deletePerson(ID: string): Promise<Person>;
}
