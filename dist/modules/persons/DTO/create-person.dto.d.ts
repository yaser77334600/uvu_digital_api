export declare class CreatePersonDTO {
    name: string;
    gender: string;
    birthdate: Date;
    biography: string;
    category: string;
}
