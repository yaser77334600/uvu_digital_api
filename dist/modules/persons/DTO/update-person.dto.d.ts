export declare class UpdatePersonDTO {
    name: string;
    gender: string;
    birthdate: Date;
    biography: string;
    category: string;
}
