"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const users_service_1 = require("../users/users.service");
const pick_pipe_1 = require("../../common/pipes/pick.pipe");
const login_user_dto_1 = require("./DTO/login-user.dto");
const auth_service_1 = require("./auth.service");
const subscriptions_service_1 = require("../subscriptions/subscriptions.service");
let AuthController = class AuthController {
    constructor(userAPI, authAPI, subscriptionAPI) {
        this.userAPI = userAPI;
        this.authAPI = authAPI;
        this.subscriptionAPI = subscriptionAPI;
    }
    async login(user) {
        const userDB = await this.userAPI.getFullUser({ email: user.email, active: true });
        if (!userDB) {
            throw new common_1.UnauthorizedException('Invalid email or password', '0001');
        }
        if (!this.authAPI.mathpassword(user.password, userDB.password)) {
            throw new common_1.UnauthorizedException('Invalid email or password', '0001');
        }
        if (!userDB.verifiedEmail) {
            throw new common_1.UnauthorizedException(`${userDB._id.toString()}`, '0002');
        }
        return { auth: this.authAPI.getToken(userDB) };
    }
};
__decorate([
    common_1.Post('login'),
    __param(0, common_1.Body(new pick_pipe_1.PickPipe(['email', 'password']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [login_user_dto_1.LoginUserDTO]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "login", null);
AuthController = __decorate([
    common_1.Controller('auth'),
    __metadata("design:paramtypes", [users_service_1.UsersService, auth_service_1.AuthService, subscriptions_service_1.SubscriptionsService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map