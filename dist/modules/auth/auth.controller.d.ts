import { UsersService } from '../users/users.service';
import { LoginUserDTO } from './DTO/login-user.dto';
import { AuthService } from './auth.service';
import { SubscriptionsService } from '../subscriptions/subscriptions.service';
export declare class AuthController {
    private userAPI;
    private authAPI;
    private subscriptionAPI;
    constructor(userAPI: UsersService, authAPI: AuthService, subscriptionAPI: SubscriptionsService);
    login(user: LoginUserDTO): Promise<{
        auth: string;
    }>;
}
