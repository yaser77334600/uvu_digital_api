import { User } from '../users/interfaces/user.interface';
export declare class AuthService {
    mathpassword(password: any, encrypted: any): any;
    getToken(user: User): string;
    verifyToken(token: any): string | object;
}
