"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const stripe = require("stripe");
let SubscriptionsService = class SubscriptionsService {
    constructor() {
        this.stripeAPI = new stripe(process.env.STRIPE_API);
    }
    async getFullCustomer(userID) {
        let error;
        const customer = await this.stripeAPI.customers.retrieve(userID).catch((e) => error = null);
        return error === null ? error : customer;
    }
    async getCustomer(userID) {
        const subscription = { current_period_end: '', last4: '' };
        let error;
        const customer = await this.stripeAPI.customers.retrieve(userID).catch((e) => error = null);
        if (!error && customer && customer.subscriptions && customer.subscriptions.data && customer.subscriptions.data.length > 0) {
            subscription.current_period_end = customer.subscriptions.data[0].current_period_end;
            if (customer.invoice_settings &&
                customer.invoice_settings.default_payment_method &&
                customer.invoice_settings.default_payment_method !== '') {
                const PM = await this.getPaymentDetails(customer.invoice_settings.default_payment_method).catch(e => error = null);
                if (!error && PM.card) {
                    subscription.last4 = PM.card.last4;
                }
            }
        }
        return subscription;
    }
    async deleteSub(customerID) {
        const existCustomer = await this.getFullCustomer(customerID);
        if (existCustomer && !existCustomer.deleted) {
            if (existCustomer.subscriptions && existCustomer.subscriptions.data && existCustomer.subscriptions.data.length > 0) {
                const subID = existCustomer.subscriptions.data[0].id;
                await this.stripeAPI.subscriptions.del(subID).catch(e => common_1.Logger.log(e));
            }
        }
    }
    async createCustomer(user, token) {
        await this.stripeAPI.customers.create({ email: user.email, source: token });
    }
    async createSubscription(customer) {
        return await this.stripeAPI.subscriptions.create({ customer, items: [{ plan: 'prod_F2e6n0OzpfBXJk' }] });
    }
    async getSources(customer) {
        let error;
        const cards = await this.stripeAPI.customers.listSources(customer, {}).catch((e) => error = null);
        return error === null ? error : cards;
    }
    async getPaymentDetails(id) {
        let error;
        const paymentMethods = await this.stripeAPI.paymentMethods.retrieve(id).catch((e) => error = null);
        return error === null ? error : paymentMethods;
    }
};
SubscriptionsService = __decorate([
    common_1.Injectable()
], SubscriptionsService);
exports.SubscriptionsService = SubscriptionsService;
//# sourceMappingURL=subscriptions.service.js.map