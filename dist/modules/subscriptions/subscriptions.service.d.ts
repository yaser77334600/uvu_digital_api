import * as stripe from 'stripe';
import { User } from '../users/interfaces/user.interface';
export declare class SubscriptionsService {
    private stripeAPI;
    getFullCustomer(userID: any): Promise<any>;
    getCustomer(userID: any): Promise<any>;
    deleteSub(customerID: any): Promise<void>;
    createCustomer(user: User, token: any): Promise<void>;
    createSubscription(customer: string): Promise<stripe.subscriptions.ISubscription>;
    getSources(customer: any): Promise<any>;
    getPaymentDetails(id: any): Promise<any>;
}
