import { SubscriptionsService } from './subscriptions.service';
import { UsersService } from '../users/users.service';
import * as Stripe from 'stripe';
import { UpdatePaymentDTO } from './DTO/update-payment.dto';
export declare class SubscriptionsController {
    private billinAPI;
    private userAPI;
    stripe: Stripe;
    constructor(billinAPI: SubscriptionsService, userAPI: UsersService);
    getSubscription(ID: any, req: any): Promise<any>;
    createSubscription(req: any, res: any, auth: UpdatePaymentDTO): Promise<void>;
}
