"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const subscriptions_service_1 = require("./subscriptions.service");
const auth_guard_1 = require("../../common/guard/auth.guard");
const users_service_1 = require("../users/users.service");
const Stripe = require("stripe");
const update_payment_dto_1 = require("./DTO/update-payment.dto");
const pick_pipe_1 = require("../../common/pipes/pick.pipe");
let SubscriptionsController = class SubscriptionsController {
    constructor(billinAPI, userAPI) {
        this.billinAPI = billinAPI;
        this.userAPI = userAPI;
        this.stripe = new Stripe(process.env.STRIPE_API);
    }
    async getSubscription(ID, req) {
        const userToken = req.token.user;
        if (userToken.role !== 'ADMIN') {
            if (userToken.hasOwnProperty('customer')) {
                ID = userToken.customer;
            }
            else {
                const user = await this.userAPI.getUser({ _id: userToken._id });
                ID = user.customer ? user.customer : '';
            }
        }
        return await this.billinAPI.getCustomer(ID);
    }
    async createSubscription(req, res, auth) {
        let StripeError;
        const userToken = req.token.user;
        const user = await this.userAPI.getUser({ _id: userToken._id });
        if (!user) {
            throw new common_1.NotFoundException('User not found', '1001');
        }
        let details;
        let customer;
        if (!user.customer) {
            details = {
                email: user.email, name: user.name, description: 'UVU Basic', payment_method: auth.paymentMethod,
                invoice_settings: { default_payment_method: auth.paymentMethod },
            };
            customer = await this.stripe.customers.create(details).catch(error => StripeError = error);
            if (customer) {
                user.customer = customer.id;
                await user.save();
            }
        }
        else {
            const existCustomer = await this.billinAPI.getFullCustomer(user.customer);
            if (existCustomer && existCustomer.deleted) {
                details = {
                    email: user.email, name: user.name, description: 'UVU Basic', payment_method: auth.paymentMethod,
                    invoice_settings: { default_payment_method: auth.paymentMethod },
                };
                customer = await this.stripe.customers.create(details).catch(error => StripeError = error);
                if (customer) {
                    user.customer = customer.id;
                    await user.save();
                }
            }
            else {
                const paymentMethod = await this.stripe.paymentMethods.attach(auth.paymentMethod, { customer: user.customer })
                    .catch(error => StripeError = error);
                details = { invoice_settings: { default_payment_method: paymentMethod.id } };
                customer = await this.stripe.customers.update(user.customer, details).catch(error => StripeError = error);
            }
        }
        if (StripeError) {
            common_1.Logger.log(StripeError);
            if (StripeError.hasOwnProperty('raw') && StripeError.raw.param === 'payment_method') {
                throw new common_1.BadRequestException('Invalid payment method', 'S001');
            }
            else if (StripeError.hasOwnProperty('raw') && StripeError.raw.param === 'customer') {
                throw new common_1.NotFoundException('Customer not found', 'S002');
            }
            else {
                throw new common_1.InternalServerErrorException('An error has occurred', '0000');
            }
        }
        if (customer.subscriptions.data.length > 0) {
            res.json({ ok: true });
        }
        else {
            await this.stripe.subscriptions.create({ customer: customer.id, items: [{ plan: 'plan_G9j4AyTXU1d9ml' }] })
                .catch(e => StripeError = e);
            if (StripeError) {
                throw new common_1.InternalServerErrorException('An error has occurred', '0000');
            }
            res.json({ ok: true });
        }
    }
};
__decorate([
    common_1.Get(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Param('id')), __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SubscriptionsController.prototype, "getSubscription", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Req()), __param(1, common_1.Res()), __param(2, common_1.Body(new pick_pipe_1.PickPipe(['paymentMethod']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, update_payment_dto_1.UpdatePaymentDTO]),
    __metadata("design:returntype", Promise)
], SubscriptionsController.prototype, "createSubscription", null);
SubscriptionsController = __decorate([
    common_1.Controller('subscriptions'),
    __metadata("design:paramtypes", [subscriptions_service_1.SubscriptionsService, users_service_1.UsersService])
], SubscriptionsController);
exports.SubscriptionsController = SubscriptionsController;
//# sourceMappingURL=subscriptions.controller.js.map