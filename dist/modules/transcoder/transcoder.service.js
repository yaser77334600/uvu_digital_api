"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const AWS = require("aws-sdk");
let TranscoderService = class TranscoderService {
    constructor() {
        AWS.config.update({
            secretAccessKey: process.env.AWS_SC_KEY,
            accessKeyId: process.env.AWS_KEY,
            region: process.env.AWS_REGION,
        });
        this.transcoder = new AWS.ElasticTranscoder();
    }
    async createJob(input, output, name) {
        const params = {
            PipelineId: '1572034412520-2mm5du',
            Input: { Key: input },
            OutputKeyPrefix: output,
            Outputs: [
                { PresetId: '1578927537583-0ob6fe', SegmentDuration: '10', Key: 'audio/audio' + name },
                { PresetId: '1587679275412-4d64t7', SegmentDuration: '10', Key: 'hd/mov' + name },
                { PresetId: '1578930956549-3wb29t', SegmentDuration: '10', Key: 'fhd/mov' + name },
            ],
            Playlists: [{ Name: 'index', Format: 'HLSv4', OutputKeys: ['hd/mov' + name, 'fhd/mov' + name, 'audio/audio' + name] }],
        };
        return await this.transcoder.createJob(params).promise();
    }
    async createAudioJob(input, output, name) {
        const params = {
            PipelineId: '1572034412520-2mm5du',
            Input: { Key: input },
            OutputKeyPrefix: output,
            Outputs: [{ PresetId: '1578927537583-0ob6fe', SegmentDuration: '10', Key: name }],
        };
        return await this.transcoder.createJob(params).promise();
    }
    async getJobStatus(Id) {
        return await this.transcoder.readJob({ Id }).promise();
    }
};
TranscoderService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [])
], TranscoderService);
exports.TranscoderService = TranscoderService;
//# sourceMappingURL=transcoder.service.js.map