import * as AWS from 'aws-sdk';
import { ElasticTranscoder } from 'aws-sdk';
export declare class TranscoderService {
    constructor();
    private transcoder;
    createJob(input: any, output: any, name: any): Promise<import("aws-sdk/lib/request").PromiseResult<ElasticTranscoder.CreateJobResponse, AWS.AWSError>>;
    createAudioJob(input: any, output: any, name: any): Promise<import("aws-sdk/lib/request").PromiseResult<ElasticTranscoder.CreateJobResponse, AWS.AWSError>>;
    getJobStatus(Id: any): Promise<import("aws-sdk/lib/request").PromiseResult<ElasticTranscoder.ReadJobResponse, AWS.AWSError>>;
}
