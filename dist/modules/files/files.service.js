"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const AWS = require("aws-sdk");
let FilesService = class FilesService {
    constructor() {
        AWS.config.update({
            secretAccessKey: process.env.AWS_SC_KEY,
            accessKeyId: process.env.AWS_KEY,
            region: process.env.AWS_REGION,
        });
        this.S3 = new AWS.S3();
    }
    async uploadFile(file, folder, publicResource = false) {
        if (!file) {
            return null;
        }
        const params = {
            Bucket: process.env.BUCKET,
            Key: folder + file.originalname,
            Body: file.buffer,
            ContentType: file.mimetype,
        };
        if (publicResource) {
            params.Bucket = process.env.PUBLIC_BUCKET;
        }
        return await this.S3.upload(params).promise();
    }
    async fileExist(folder, file, publicResource = false) {
        folder = folder.charAt(folder.length - 1) === '/' ? folder.substr(0, folder.length - 1) : folder;
        const fileList = await this.listFiles(folder, publicResource);
        if (fileList.KeyCount > 0) {
            let exist = false;
            fileList.Contents.forEach(fileS3 => fileS3.Key === `${folder}/${file}` ? exist = true : '');
            return exist;
        }
        else {
            return false;
        }
    }
    async listFiles(folder, publicResource = false) {
        const params = {
            Bucket: process.env.BUCKET,
            Prefix: folder,
        };
        if (publicResource) {
            params.Bucket = process.env.PUBLIC_BUCKET;
        }
        return await this.S3.listObjectsV2(params).promise();
    }
    async deleteFile(folder, file, publicResource = false) {
        const params = {
            Bucket: process.env.BUCKET,
            Key: `${folder}/${file}`,
        };
        if (publicResource) {
            params.Bucket = process.env.PUBLIC_BUCKET;
        }
        return await this.S3.deleteObject(params).promise();
    }
    async getFile(folder, file, publicResource = false) {
        const params = {
            Bucket: process.env.BUCKET,
            Key: `${folder}/${file}`,
        };
        if (publicResource) {
            params.Bucket = process.env.PUBLIC_BUCKET;
        }
        return await this.S3.getObject(params).promise();
    }
    getFileStream(folder, file) {
        const params = {
            Bucket: process.env.BUCKET,
            Key: `${folder}/${file}`,
        };
        return this.S3.getObject(params).createReadStream();
    }
    async getHeadObject(folder, file) {
        const params = {
            Bucket: process.env.BUCKET,
            Key: `${folder}/${file}`,
        };
        return this.S3.headObject(params).promise();
    }
};
FilesService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [])
], FilesService);
exports.FilesService = FilesService;
//# sourceMappingURL=files.service.js.map