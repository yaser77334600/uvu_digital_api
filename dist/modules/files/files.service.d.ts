/// <reference types="multer" />
/// <reference types="node" />
import * as AWS from 'aws-sdk';
import { S3 } from 'aws-sdk';
export declare class FilesService {
    constructor();
    private S3;
    uploadFile(file: Express.Multer.File, folder: string, publicResource?: boolean): Promise<S3.ManagedUpload.SendData>;
    fileExist(folder: any, file: any, publicResource?: boolean): Promise<boolean>;
    listFiles(folder: any, publicResource?: boolean): Promise<S3.ListObjectsV2Output>;
    deleteFile(folder: any, file: any, publicResource?: boolean): Promise<import("aws-sdk/lib/request").PromiseResult<S3.DeleteObjectOutput, AWS.AWSError>>;
    getFile(folder: any, file: any, publicResource?: boolean): Promise<import("aws-sdk/lib/request").PromiseResult<S3.GetObjectOutput, AWS.AWSError>>;
    getFileStream(folder: any, file: any): import("stream").Readable;
    getHeadObject(folder: any, file: any): Promise<import("aws-sdk/lib/request").PromiseResult<S3.HeadObjectOutput, AWS.AWSError>>;
}
