"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const multimedia_service_1 = require("./multimedia.service");
const match_query_pipe_1 = require("../../common/pipes/match-query.pipe");
const validate_object_id_pipe_1 = require("../../common/pipes/validate-object-id.pipe");
const mongo_error_filter_1 = require("../../common/filters/mongo-error.filter");
const create_multimedia_dto_1 = require("./DTO/create-multimedia.dto");
const pick_pipe_1 = require("../../common/pipes/pick.pipe");
const validation_error_filter_1 = require("../../common/filters/validation-error.filter");
const update_multimedia_dto_1 = require("./DTO/update-multimedia.dto");
const auth_guard_1 = require("../../common/guard/auth.guard");
const role_guard_1 = require("../../common/guard/role.guard");
const mongoose = require("mongoose");
const platform_express_1 = require("@nestjs/platform-express");
const valid_filed_extensions_interceptor_1 = require("../../common/interceptors/valid-filed-extensions.interceptor");
const image_size_1 = require("image-size");
const path = require("path");
const uuid = require("uuid");
const files_service_1 = require("../files/files.service");
const match_array_query_pipe_1 = require("../../common/pipes/match-array-query.pipe");
const set_master_dto_1 = require("./DTO/set-master.dto");
const transcoder_service_1 = require("../transcoder/transcoder.service");
const tasks_service_1 = require("../tasks/tasks.service");
const set_source_dto_1 = require("./DTO/set-source.dto");
const update_cover_dto_1 = require("./DTO/update-cover.dto");
const genres_service_1 = require("../genres/genres.service");
const authQuery_guard_1 = require("../../common/guard/authQuery.guard");
const subscriptions_service_1 = require("../subscriptions/subscriptions.service");
let MultimediaController = class MultimediaController {
    constructor(multimediaAPI, filesAPI, transcoderAPI, taskAPI, genresAPI, subAPI) {
        this.multimediaAPI = multimediaAPI;
        this.filesAPI = filesAPI;
        this.transcoderAPI = transcoderAPI;
        this.taskAPI = taskAPI;
        this.genresAPI = genresAPI;
        this.subAPI = subAPI;
    }
    async getMultimediaForHome() {
        const genres = await this.genresAPI.getGenres({ options: { active: true } });
        return genres;
    }
    async getMultimedia(req, query) {
        console.log(query);
        let exclude = '';
        const tokenUser = req.token.user;
        if (tokenUser.role === 'USER' || !query.onlyMe) {
            exclude += '-status -rejectedReason -jobID -transcoded -transcoding -insertBy';
        }
        if (tokenUser.role === 'CREATOR' && query.onlyMe) {
            query.options.insertBy = tokenUser._id;
        }
        if (query.hasOwnProperty('language') && mongoose.Types.ObjectId.isValid(query.language)) {
            query.options['sources.language'] = query.language;
        }
        const total = await this.multimediaAPI.countMedia(query);
        console.log(query);
        const multimedia = await this.multimediaAPI.getMultimedia(query, exclude);
        return { total, multimedia, mylist: tokenUser.myList };
    }
    async getMultimediaByName(name) {
        const multimedia = await this.multimediaAPI.getMedia({ name });
        if (!multimedia) {
            throw new common_1.NotFoundException('Media not found', '8001');
        }
        return multimedia;
    }
    async getMedia(req, multimediaID) {
        const tokenUser = req.token.user;
        let multimedia = await this.multimediaAPI.getMedia({ _id: multimediaID, active: true });
        if (!multimedia) {
            throw new common_1.NotFoundException('Media not found', '8001');
        }
        multimedia = multimedia.toObject();
        multimedia.inMyList = tokenUser.myList.map(media => media._id.toString()).indexOf(multimedia._id.toString()) !== -1;
        return multimedia;
    }
    async playMedia(multimediaID, req, res) {
        const tokenUser = req.token.user;
        const multimedia = await this.multimediaAPI.getMedia({ _id: multimediaID, active: true });
        if (!multimedia) {
            throw new common_1.NotFoundException('Media not found', '8001');
        }
        if (!multimedia.file) {
            throw new common_1.NotFoundException('Media not found', '8001');
        }
        else if (tokenUser.role !== 'ADMIN' && multimedia.status !== 'published') {
            throw new common_1.NotFoundException('Media not found', '8001');
        }
        if (tokenUser.role !== 'ADMIN' && tokenUser.role !== 'SUPERVISOR') {
            if (!tokenUser.customer) {
                throw new common_1.NotFoundException('Customer not found', 'S002');
            }
            const customer = await this.subAPI.getFullCustomer(tokenUser.customer);
            if (!customer && customer.deleted) {
                throw new common_1.NotFoundException('Customer not found', 'S002');
            }
            if (!customer.subscriptions || !customer.subscriptions.data || customer.subscriptions.data.length <= 0) {
                throw new common_1.NotFoundException('Subscription not found', 'S004');
            }
            if (customer.subscriptions.data[0].status !== 'active') {
                throw new common_1.UnauthorizedException('Subscription not found', 'S003');
            }
        }
        const route = multimedia.file.split('/');
        const file = route.pop();
        const headers = await this.filesAPI.getHeadObject(route.join('/'), file);
        const stream = this.filesAPI.getFileStream(route.join('/'), file);
        res.set('Content-Type', headers.ContentType);
        res.set('Content-Length', headers.ContentLength);
        res.set('Last-Modified', headers.LastModified);
        res.set('ETag', headers.ETag);
        return stream.pipe(res);
    }
    async createMultimedia(req, multimedia) {
        const tokenUser = req.token.user;
        Object.assign(multimedia, { insertBy: tokenUser._id });
        return await this.multimediaAPI.createMultimedia(multimedia);
    }
    async updateMultimedia(req, multimediaID, multimedia) {
        const tokenUser = req.token.user;
        const media = await this.multimediaAPI.getMedia({ _id: multimediaID });
        if (!media) {
            throw new common_1.NotFoundException('Media not found', '8001');
        }
        if (tokenUser.role !== 'ADMIN') {
            delete multimedia.rejectedReason;
            if (multimedia.status) {
                media.rejectedReason = '';
                multimedia.status = multimedia.status !== 'pending' ? 'draft' : 'pending';
            }
            else {
                multimedia.status = 'draft';
            }
        }
        return await this.multimediaAPI.updateMultimedia(multimedia, { _id: multimediaID });
    }
    async setMasterFiles(id, masterFiles) {
        const media = await this.multimediaAPI.getMedia({ _id: id });
        if (!media) {
            throw new common_1.NotFoundException('Media not found', '8001');
        }
        if (masterFiles.trailer && masterFiles.trailer !== '') {
            media.trailer = masterFiles.trailer;
        }
        if (masterFiles.file && masterFiles.file !== '') {
            const tmpMasterFile = masterFiles.file.split('/');
            const masterName = tmpMasterFile.pop();
            media.file = masterFiles.file;
            media.transcoding = true;
        }
        return await media.save();
    }
    deleteMultimedia(multimediaID) {
        return this.multimediaAPI.deleteMultimedia(multimediaID);
    }
    async uploadCover(file, id, query) {
        const media = await this.multimediaAPI.getMedia({ _id: id });
        if (!media) {
            throw new common_1.NotFoundException('Media not found', '8001');
        }
        if (!file) {
            throw new common_1.BadRequestException('Missing file', '7001');
        }
        const dimensions = image_size_1.imageSize(file.buffer);
        if (query.type === 'horizontal') {
            if (dimensions.width !== 1200 || dimensions.height !== 800) {
                throw new common_1.BadRequestException('Image must be 1200x800', '8006');
            }
        }
        else {
            if (dimensions.width !== 800 || dimensions.height !== 1200) {
                throw new common_1.BadRequestException('Image must be 800x1200', '8007');
            }
        }
        file.originalname = uuid.v4() + path.extname(file.originalname);
        const uploaded = await this.filesAPI.uploadFile(file, `movies/${media._id}/cover/main/`, true);
        if (!uploaded) {
            throw new common_1.InternalServerErrorException('Cant Upload', '7002');
        }
        query.type === 'horizontal' ? media.coverHorizontal = uploaded.Key : media.coverVertical = uploaded.Key;
        return await media.save();
    }
    async uploadMasterFile(file, id, res, query) {
        const media = await this.multimediaAPI.getMedia({ _id: id });
        if (!media) {
            throw new common_1.NotFoundException('Media not found', '8001');
        }
        if (!file) {
            throw new common_1.BadRequestException('Missing file', '7001');
        }
        const isTrailer = query.hasOwnProperty('trailer') && query.trailer;
        file.originalname = uuid.v4() + path.extname(file.originalname);
        file.originalname = uuid.v4() + path.extname(file.originalname);
        this.filesAPI.uploadFile(file, 'movies/' + id + '/raw/', isTrailer);
        return res.json({ key: `movies/${media._id.toString()}/raw/${file.originalname}` });
    }
    async setSource(id, source) {
        const media = await this.multimediaAPI.getMedia({ _id: id });
        if (!media) {
            throw new common_1.NotFoundException('Media not found', '8001');
        }
        let exist = false;
        media.sources.forEach((src) => {
            if (src.language.toString() === src.language) {
                exist = true;
            }
        });
        if (exist) {
            throw new common_1.ConflictException('The current language already exist in sources', '8002');
        }
        media.sources.push(source);
        return await media.save();
    }
};
__decorate([
    common_1.Get('home'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], MultimediaController.prototype, "getMultimediaForHome", null);
__decorate([
    common_1.Get(),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Req()), __param(1, common_1.Query(new validate_object_id_pipe_1.ValidateObjectIdPipe(['language']), new match_array_query_pipe_1.MatchArrayQueryPipe(['genres']), new match_query_pipe_1.MatchQueryPipe(['name', 'status']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MultimediaController.prototype, "getMultimedia", null);
__decorate([
    common_1.Get('byname/:name'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Param('name')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MultimediaController.prototype, "getMultimediaByName", null);
__decorate([
    common_1.Get(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Req()), __param(1, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MultimediaController.prototype, "getMedia", null);
__decorate([
    common_1.Get('play/:id'),
    common_1.UseGuards(authQuery_guard_1.AuthQueryGuard),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())), __param(1, common_1.Req()), __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MultimediaController.prototype, "playMedia", null);
__decorate([
    common_1.Post(),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter),
    common_1.UseFilters(validation_error_filter_1.ValidationErrorFilter),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Req()), __param(1, common_1.Body(new validate_object_id_pipe_1.ValidateObjectIdPipe(['genres', 'directors', 'actors', 'classification', 'language']), new pick_pipe_1.PickPipe(['name', 'tagline', 'description', 'genres', 'directors', 'actors', 'classification', 'keywords', 'language']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_multimedia_dto_1.CreateMultimediaDTO]),
    __metadata("design:returntype", Promise)
], MultimediaController.prototype, "createMultimedia", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter),
    common_1.UseFilters(validation_error_filter_1.ValidationErrorFilter),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Req()), __param(1, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())), __param(2, common_1.Body(new validate_object_id_pipe_1.ValidateObjectIdPipe(['genres', 'directors', 'actors', 'classification', 'language']), new pick_pipe_1.PickPipe(['name', 'tagline', 'description', 'genres', 'directors', 'actors', 'classification',
        'keywords', 'language', 'status', 'rejectedReason']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, update_multimedia_dto_1.UpdateMultimediaDTO]),
    __metadata("design:returntype", Promise)
], MultimediaController.prototype, "updateMultimedia", null);
__decorate([
    common_1.Put('setmaster/:id'),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter),
    common_1.UseFilters(validation_error_filter_1.ValidationErrorFilter),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())), __param(1, common_1.Body(new pick_pipe_1.PickPipe(['file', 'trailer']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, set_master_dto_1.SetMasterSourceDTO]),
    __metadata("design:returntype", Promise)
], MultimediaController.prototype, "setMasterFiles", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN'])),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MultimediaController.prototype, "deleteMultimedia", null);
__decorate([
    common_1.Put('upload/:id/cover'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file', { limits: { fileSize: 5000000 } }), new valid_filed_extensions_interceptor_1.ValidFiledExtensionsInterceptor(['image/jpeg', 'image/png'])),
    __param(0, common_1.UploadedFile('file')), __param(1, common_1.Param('id')), __param(2, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, update_cover_dto_1.UpdateCoverDTO]),
    __metadata("design:returntype", Promise)
], MultimediaController.prototype, "uploadCover", null);
__decorate([
    common_1.Post('upload/:id/media'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file', { limits: { fileSize: 300000000 } }), new valid_filed_extensions_interceptor_1.ValidFiledExtensionsInterceptor(['video/mp4'])),
    __param(0, common_1.UploadedFile('file')), __param(1, common_1.Param('id')), __param(2, common_1.Res()), __param(3, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MultimediaController.prototype, "uploadMasterFile", null);
__decorate([
    common_1.Post('addsource/:multimedia'),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter),
    common_1.UseFilters(validation_error_filter_1.ValidationErrorFilter),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    __param(0, common_1.Param('multimedia', new validate_object_id_pipe_1.ValidateObjectIdPipe())), __param(1, common_1.Body(new validate_object_id_pipe_1.ValidateObjectIdPipe(['language']), new pick_pipe_1.PickPipe(['name', 'tagline', 'description', 'keywords', 'language']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, set_source_dto_1.AddSourceDTO]),
    __metadata("design:returntype", Promise)
], MultimediaController.prototype, "setSource", null);
MultimediaController = __decorate([
    common_1.Controller('multimedia'),
    __metadata("design:paramtypes", [multimedia_service_1.MultimediaService, files_service_1.FilesService, transcoder_service_1.TranscoderService,
        tasks_service_1.TasksService, genres_service_1.GenresService, subscriptions_service_1.SubscriptionsService])
], MultimediaController);
exports.MultimediaController = MultimediaController;
//# sourceMappingURL=multimedia.controller.js.map