export declare class AddSourceDTO {
    name: string;
    tagline: string;
    description: string;
    language: string;
    keywords: string[];
}
