export declare class CreateMultimediaDTO {
    name: string;
    tagline: string;
    description: string;
    genres: string[];
    directors: string[];
    actors: string[];
    classification: string;
    keywords: string[];
}
