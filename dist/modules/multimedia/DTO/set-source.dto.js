"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class AddSourceDTO {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.MaxLength(120),
    __metadata("design:type", String)
], AddSourceDTO.prototype, "name", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(80),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AddSourceDTO.prototype, "tagline", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(800),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AddSourceDTO.prototype, "description", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.MaxLength(24),
    class_validator_1.MinLength(24),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AddSourceDTO.prototype, "language", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], AddSourceDTO.prototype, "keywords", void 0);
exports.AddSourceDTO = AddSourceDTO;
//# sourceMappingURL=set-source.dto.js.map