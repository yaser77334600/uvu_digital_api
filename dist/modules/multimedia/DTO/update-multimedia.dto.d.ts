export declare class UpdateMultimediaDTO {
    name: string;
    tagline: string;
    description: string;
    genres: string[];
    directors: string[];
    actors: string[];
    classification: string;
    keywords: string[];
    status: string;
    rejectedReason: string;
}
