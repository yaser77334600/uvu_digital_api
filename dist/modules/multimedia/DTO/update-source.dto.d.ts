export declare class UpdateSourceDTO {
    name: string;
    tagline: string;
    description: string;
    keywords: string[];
}
