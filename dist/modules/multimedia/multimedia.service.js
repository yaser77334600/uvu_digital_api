"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
let MultimediaService = class MultimediaService {
    constructor(MultimediaModel) {
        this.MultimediaModel = MultimediaModel;
    }
    async getMultimedia(query, exclude = '') {
        const { limit, skip, options } = query;
        return await this.MultimediaModel.find(options)
            .limit(Number(limit) > 100 ? 100 : Number(limit))
            .skip(Math.abs(Number(skip)) || 0)
            .populate('genres', 'name')
            .populate('directors', 'name category')
            .populate('actors', 'name category')
            .populate('language', 'code')
            .populate('sources.language', 'code')
            .populate('classification', 'name')
            .populate('insertBy', 'name')
            .select(exclude);
    }
    async getMedia(options) {
        return await this.MultimediaModel.findOne(options)
            .populate('genres', 'name')
            .populate('directors', 'name category')
            .populate('actors', 'name category')
            .populate('language', 'code')
            .populate('sources.language', 'code')
            .populate('classification', 'name')
            .populate('insertBy', 'name');
    }
    async countMedia(query) {
        const { options } = query;
        return await this.MultimediaModel.estimatedDocumentCount(options);
    }
    async createMultimedia(multimedia) {
        const multimediaDB = new this.MultimediaModel(multimedia);
        return await multimediaDB.save();
    }
    async updateMultimedia(multimedia, query) {
        return await this.MultimediaModel.findOneAndUpdate(query, multimedia, { new: true, runValidators: true, context: 'query' })
            .populate('genres', 'name')
            .populate('directors', 'name category')
            .populate('actors', 'name category')
            .populate('language', 'code')
            .populate('sources.language', 'code')
            .populate('classification', 'name');
    }
    async deleteMultimedia(ID) {
        const multimedia = await this.getMedia({ _id: ID, active: true });
        const update = { active: false, code: multimedia.name + '-disabled' };
        return await this.MultimediaModel.findOneAndUpdate({ _id: multimedia._id }, update, { new: true, runValidators: true, context: 'query' });
    }
};
MultimediaService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('Multimedia')),
    __metadata("design:paramtypes", [mongoose_2.Model])
], MultimediaService);
exports.MultimediaService = MultimediaService;
//# sourceMappingURL=multimedia.service.js.map