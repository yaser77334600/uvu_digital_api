"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const source_schema_1 = require("./source.schema");
exports.MultimediaSchema = new mongoose_1.Schema({
    language: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Language',
        required: true,
    },
    name: {
        type: String,
        trim: true,
        maxlength: 120,
        required: true,
        unique: true,
    },
    status: {
        type: String,
        enum: {
            values: ['draft', 'pending', 'published', 'rejected'],
            message: 'Invalid status',
        },
        default: 'draft',
    },
    rejectedReason: {
        type: String,
        maxlength: 3000,
    },
    tagline: {
        type: String,
        trim: true,
        maxlength: 80,
    },
    description: {
        type: String,
        trim: true,
        maxlength: 800,
        required: false,
    },
    genres: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: 'Genre',
            required: true,
        }],
    directors: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: 'Person',
        }],
    actors: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: 'Person',
        }],
    classification: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Classification',
        required: true,
    },
    keywords: [{
            type: String,
            maxlength: 20,
        }],
    active: {
        type: Boolean,
        default: true,
    },
    file: {
        type: String,
        required: false,
    },
    jobID: {
        type: String,
        required: false,
    },
    transcoded: {
        type: String,
        required: false,
    },
    trailer: {
        type: String,
    },
    coverVertical: {
        type: String,
        required: false,
    },
    coverHorizontal: {
        type: String,
        required: false,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    sources: [source_schema_1.SourceSchema],
    transcoding: {
        type: Boolean,
        default: false,
    },
    insertBy: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
});
//# sourceMappingURL=multimedia.schema.js.map