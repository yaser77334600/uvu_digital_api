"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.SourceSchema = new mongoose_1.Schema({
    name: {
        type: String,
        trim: true,
        maxlength: 120,
        required: true,
    },
    tagline: {
        type: String,
        trim: true,
        maxlength: 80,
    },
    description: {
        type: String,
        trim: true,
        maxlength: 800,
        required: false,
    },
    keywords: [{
            type: String,
            maxlength: 20,
        }],
    active: {
        type: Boolean,
        default: true,
    },
    language: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Language',
        required: true,
    },
    subtitle: {
        type: String,
        required: false,
    },
    audio: {
        type: String,
        required: false,
    },
    jobID: {
        type: String,
    },
    coverVertical: {
        type: String,
        required: false,
    },
    coverHorizontal: {
        type: String,
        required: false,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    trailer: {
        type: String,
    },
    audioTranscoded: {
        type: String,
    },
    lastAudioID: {
        type: String,
    },
    lastSubID: {
        type: String,
    },
});
//# sourceMappingURL=source.schema.js.map