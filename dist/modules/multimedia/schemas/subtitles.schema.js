"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.SubtitleSchema = new mongoose_1.Schema({
    code: {
        type: String,
        trim: true,
        required: true,
    },
    uri: {
        type: String,
        trim: true,
        required: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
});
//# sourceMappingURL=subtitles.schema.js.map