import { Document } from 'mongoose';
export interface Subtitle extends Document {
    code: string;
    uri: string;
    createdOn: Date;
}
