"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var status;
(function (status) {
    status[status["draft"] = 0] = "draft";
    status[status["pending"] = 1] = "pending";
    status[status["published"] = 2] = "published";
})(status || (status = {}));
//# sourceMappingURL=multimedia.interface.js.map