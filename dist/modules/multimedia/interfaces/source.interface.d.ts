import { Document } from 'mongoose';
export interface Source extends Document {
    name: string;
    tagline: string;
    description: string;
    keywords: string[];
    active: boolean;
    language: any;
    subtitle: string;
    audio: string;
    coverVertical: string;
    coverHorizontal: string;
    createdOn: Date;
    trailer: string;
    jobID: string;
    audioTranscoded: string;
    lastAudioID: string;
    lastSubID: string;
}
