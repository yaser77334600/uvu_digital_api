import { Document } from 'mongoose';
import { Source } from './source.interface';
export interface Multimedia extends Document {
    language: string;
    name: string;
    tagline: string;
    status: status;
    description?: string;
    genres: Document[];
    directors: Document[];
    actors: Document[];
    classification: string | Document;
    keywords: Document[];
    active: boolean;
    file: string;
    jobID: string;
    transcoded: string;
    trailer: string;
    coverVertical: string;
    coverHorizontal: string;
    createdOn: Date;
    sources: Source[];
    rejectedReason: string;
    transcoding: boolean;
    _id: string;
}
declare enum status {
    draft = 0,
    pending = 1,
    published = 2
}
export {};
