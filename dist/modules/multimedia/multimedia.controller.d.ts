/// <reference types="multer" />
import { MultimediaService } from './multimedia.service';
import { Multimedia } from './interfaces/multimedia.interface';
import { CreateMultimediaDTO } from './DTO/create-multimedia.dto';
import { UpdateMultimediaDTO } from './DTO/update-multimedia.dto';
import { FilesService } from '../files/files.service';
import { SetMasterSourceDTO } from './DTO/set-master.dto';
import { TranscoderService } from '../transcoder/transcoder.service';
import { TasksService } from '../tasks/tasks.service';
import { AddSourceDTO } from './DTO/set-source.dto';
import { UpdateCoverDTO } from './DTO/update-cover.dto';
import { GenresService } from '../genres/genres.service';
import { SubscriptionsService } from '../subscriptions/subscriptions.service';
export declare class MultimediaController {
    private multimediaAPI;
    private filesAPI;
    private transcoderAPI;
    private taskAPI;
    private genresAPI;
    private subAPI;
    constructor(multimediaAPI: MultimediaService, filesAPI: FilesService, transcoderAPI: TranscoderService, taskAPI: TasksService, genresAPI: GenresService, subAPI: SubscriptionsService);
    getMultimediaForHome(): Promise<import("../genres/interfaces/genre.interface").Genre[]>;
    getMultimedia(req: any, query: any): Promise<{
        multimedia: Multimedia[];
        total: number;
        mylist: any[];
    }>;
    getMultimediaByName(name: any): Promise<Multimedia>;
    getMedia(req: any, multimediaID: any): Promise<Multimedia>;
    playMedia(multimediaID: any, req: any, res: any): Promise<Multimedia>;
    createMultimedia(req: any, multimedia: CreateMultimediaDTO): Promise<Multimedia>;
    updateMultimedia(req: any, multimediaID: any, multimedia: UpdateMultimediaDTO): Promise<Multimedia>;
    setMasterFiles(id: any, masterFiles: SetMasterSourceDTO): Promise<Multimedia>;
    deleteMultimedia(multimediaID: any): Promise<Multimedia>;
    uploadCover(file: Express.Multer.File, id: any, query: UpdateCoverDTO): Promise<Multimedia>;
    uploadMasterFile(file: Express.Multer.File, id: any, res: any, query: any): Promise<any>;
    setSource(id: any, source: AddSourceDTO): Promise<Multimedia>;
}
