"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const files_service_1 = require("../files/files.service");
let SourcesService = class SourcesService {
    constructor(filesAPI) {
        this.filesAPI = filesAPI;
    }
    async rebuildMasterList(multimedia) {
        const masterListFile = await this.filesAPI.getFile('movies/' + multimedia._id.toString() + '/transcoded', 'index.m3u8');
        let masterList = masterListFile.Body.toString();
        if (!masterList) {
            throw new common_1.NotFoundException('Masterlist not found');
        }
        const tagPos = masterList.indexOf('##UVUCOMPILER');
        if (tagPos !== -1) {
            masterList = masterList.slice(0, tagPos - 1);
        }
        masterList += '\n##UVUCOMPILER\n';
        multimedia.sources.forEach(source => {
            if (source.subtitle) {
                masterList += this.addSubtitle(source);
            }
            if (source.audioTranscoded) {
                masterList += this.addTracks(source);
            }
        });
        masterList = masterList.replace(/AUDIO="audio-0"[\r\n]/g, 'AUDIO="audio-0",SUBTITLES="subs"\n');
        const uri = 'movies/' + multimedia._id.toString() + '/transcoded/';
        const file = { buffer: Buffer.from(masterList), originalname: 'index.m3u8', mimetype: 'text/vtt' };
        const upload = await this.filesAPI.uploadFile(file, uri);
        if (!upload) {
            throw new common_1.InternalServerErrorException('Cant upload master list');
        }
    }
    addSubtitle(source) {
        const pos = source.subtitle.indexOf('transcoded');
        if (pos !== -1) {
            const { language: { code }, subtitle } = source;
            const uri = subtitle.slice(subtitle.indexOf('transcoded') + 11, subtitle.length);
            return `\n#EXT-X-MEDIA:TYPE=SUBTITLES,GROUP-ID="subs",NAME="${code}",DEFAULT=NO,AUTOSELECT=NO,FORCED=NO,LANGUAGE="${code}",URI="${uri}"`;
        }
    }
    addTracks(source) {
        const pos = source.audio.indexOf('transcoded');
        if (pos !== -1) {
            const { language: { code }, audioTranscoded: audio } = source;
            const uri = audio.slice(audio.indexOf('transcoded') + 11, audio.length);
            return `#EXT-X-MEDIA:TYPE=AUDIO,GROUP-ID="audio-0",CODECS="mp4a.40.2",LANGUAGE="${code}",NAME="${code}",DEFAULT=NO,URI="${uri}"`;
        }
    }
};
SourcesService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [files_service_1.FilesService])
], SourcesService);
exports.SourcesService = SourcesService;
//# sourceMappingURL=sources.service.js.map