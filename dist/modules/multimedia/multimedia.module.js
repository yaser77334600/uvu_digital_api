"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const multimedia_controller_1 = require("./multimedia.controller");
const multimedia_service_1 = require("./multimedia.service");
const mongoose_1 = require("@nestjs/mongoose");
const multimedia_schema_1 = require("./schemas/multimedia.schema");
const sources_controller_1 = require("./sources.controller");
const files_module_1 = require("../files/files.module");
const transcoder_module_1 = require("../transcoder/transcoder.module");
const languages_module_1 = require("../languages/languages.module");
const sources_service_1 = require("./sources.service");
const genres_module_1 = require("../genres/genres.module");
const subscriptions_module_1 = require("../subscriptions/subscriptions.module");
let MultimediaModule = class MultimediaModule {
};
MultimediaModule = __decorate([
    common_1.Global(),
    common_1.Module({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: 'Multimedia', schema: multimedia_schema_1.MultimediaSchema }]), files_module_1.FilesModule, transcoder_module_1.TranscoderModule,
            languages_module_1.LanguagesModule, genres_module_1.GenresModule, subscriptions_module_1.SubscriptionsModule],
        controllers: [multimedia_controller_1.MultimediaController, sources_controller_1.SourcesController],
        providers: [multimedia_service_1.MultimediaService, sources_service_1.SourcesService],
        exports: [multimedia_service_1.MultimediaService, sources_service_1.SourcesService],
    })
], MultimediaModule);
exports.MultimediaModule = MultimediaModule;
//# sourceMappingURL=multimedia.module.js.map