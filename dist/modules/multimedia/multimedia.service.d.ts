import { Model } from 'mongoose';
import { Multimedia } from './interfaces/multimedia.interface';
import { CreateMultimediaDTO } from './DTO/create-multimedia.dto';
export declare class MultimediaService {
    private MultimediaModel;
    constructor(MultimediaModel: Model<Multimedia>);
    getMultimedia(query?: any, exclude?: string): Promise<Multimedia[]>;
    getMedia(options: any): Promise<Multimedia>;
    countMedia(query?: any): Promise<number>;
    createMultimedia(multimedia: CreateMultimediaDTO): Promise<Multimedia>;
    updateMultimedia(multimedia: any, query: any): Promise<Multimedia>;
    deleteMultimedia(ID: string): Promise<Multimedia>;
}
