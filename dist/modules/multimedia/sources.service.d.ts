import { Multimedia } from './interfaces/multimedia.interface';
import { FilesService } from '../files/files.service';
export declare class SourcesService {
    private filesAPI;
    constructor(filesAPI: FilesService);
    rebuildMasterList(multimedia: Multimedia): Promise<void>;
    private addSubtitle;
    private addTracks;
}
