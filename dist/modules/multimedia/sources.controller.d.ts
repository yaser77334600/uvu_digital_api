/// <reference types="multer" />
import { MultimediaService } from './multimedia.service';
import { FilesService } from '../files/files.service';
import { TranscoderService } from '../transcoder/transcoder.service';
import { TasksService } from '../tasks/tasks.service';
import { LanguagesService } from '../languages/languages.service';
import { SourcesService } from './sources.service';
import { Source } from './interfaces/source.interface';
import { UpdateCoverDTO } from './DTO/update-cover.dto';
import { Multimedia } from './interfaces/multimedia.interface';
import { UpdateSourceDTO } from './DTO/update-source.dto';
export declare class SourcesController {
    private multimediaAPI;
    private filesAPI;
    private langAPI;
    private transcoderAPI;
    private taskAPI;
    private sourceAPI;
    constructor(multimediaAPI: MultimediaService, filesAPI: FilesService, langAPI: LanguagesService, transcoderAPI: TranscoderService, taskAPI: TasksService, sourceAPI: SourcesService);
    getSoure(source: any): Promise<Source>;
    updateSource(sourceID: any, source: UpdateSourceDTO): Promise<Source>;
    getSourceStream(res: any, query: any): Promise<void>;
    uploadMedia(file: Express.Multer.File, id: any, res: any, query: any): Promise<any>;
    uploadSubtitles(file: Express.Multer.File, source: any): Promise<Multimedia>;
    setAudio(file: Express.Multer.File, source: any): Promise<Multimedia>;
    uploadCover(file: Express.Multer.File, source: any, query: UpdateCoverDTO): Promise<Multimedia>;
}
