"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const multimedia_service_1 = require("./multimedia.service");
const mongo_error_filter_1 = require("../../common/filters/mongo-error.filter");
const validation_error_filter_1 = require("../../common/filters/validation-error.filter");
const auth_guard_1 = require("../../common/guard/auth.guard");
const role_guard_1 = require("../../common/guard/role.guard");
const validate_object_id_pipe_1 = require("../../common/pipes/validate-object-id.pipe");
const platform_express_1 = require("@nestjs/platform-express");
const valid_filed_extensions_interceptor_1 = require("../../common/interceptors/valid-filed-extensions.interceptor");
const files_service_1 = require("../files/files.service");
const path = require("path");
const uuid = require("uuid");
const image_size_1 = require("image-size");
const transcoder_service_1 = require("../transcoder/transcoder.service");
const tasks_service_1 = require("../tasks/tasks.service");
const activesub_guard_1 = require("../../common/guard/activesub.guard");
const languages_service_1 = require("../languages/languages.service");
const webvtt = require("node-webvtt");
const sources_service_1 = require("./sources.service");
const update_cover_dto_1 = require("./DTO/update-cover.dto");
const uniqid = require("uniqid");
const pick_pipe_1 = require("../../common/pipes/pick.pipe");
const update_source_dto_1 = require("./DTO/update-source.dto");
let SourcesController = class SourcesController {
    constructor(multimediaAPI, filesAPI, langAPI, transcoderAPI, taskAPI, sourceAPI) {
        this.multimediaAPI = multimediaAPI;
        this.filesAPI = filesAPI;
        this.langAPI = langAPI;
        this.transcoderAPI = transcoderAPI;
        this.taskAPI = taskAPI;
        this.sourceAPI = sourceAPI;
    }
    async getSoure(source) {
        const media = await this.multimediaAPI.getMedia({ 'sources._id': source });
        if (!media) {
            throw new common_1.NotFoundException('Source not found', '8008');
        }
        return media.sources.id(source);
    }
    async updateSource(sourceID, source) {
        const media = await this.multimediaAPI.getMedia({ 'sources._id': sourceID });
        if (!media) {
            throw new common_1.NotFoundException('Source not found', '8008');
        }
        const sourceDB = media.sources.id(source);
        const update = {};
        for (const [key, value] of Object.entries(source)) {
            update[`sources.$.${key}`] = value;
        }
        await this.multimediaAPI.updateMultimedia({ $set: update }, { 'sources._id': sourceID });
        return await sourceDB;
    }
    async getSourceStream(res, query) {
        const movieRoute = query['0'].split('/');
        let route = '';
        let file = '';
        if (movieRoute.length <= 1) {
            route = `movies/${query.movie}/transcoded`;
            file = movieRoute.join();
        }
        else {
            file = movieRoute.pop();
            route = `movies/${query.movie}/transcoded/${movieRoute.join('/')}`;
        }
        const headers = await this.filesAPI.getHeadObject(route, file);
        const stream = await this.filesAPI.getFileStream(route, file);
        res.set('Content-Type', headers.ContentType);
        res.set('Content-Length', headers.ContentLength);
        res.set('Last-Modified', headers.LastModified);
        res.set('ETag', headers.ETag);
        stream.pipe(res);
    }
    async uploadMedia(file, id, res, query) {
        const media = await this.multimediaAPI.getMedia({ _id: id });
        if (!media) {
            throw new common_1.NotFoundException('Media not found', '8001');
        }
        if (!file) {
            throw new common_1.BadRequestException('Missing file', '7001');
        }
        const isTrailer = query.hasOwnProperty('trailer') && query.trailer;
        const exist = await this.filesAPI.fileExist('movies/' + id + '/raw/', file.originalname, isTrailer);
        if (exist) {
            throw new common_1.ConflictException('There is a file with this name, rename the file you are trying to upload and try again', '7003');
        }
        file.originalname = uuid.v4() + path.extname(file.originalname);
        const uploaded = await this.filesAPI.uploadFile(file, 'movies/' + id + '/raw/', isTrailer);
        if (!uploaded) {
            throw new common_1.InternalServerErrorException('Cant Upload', '7002');
        }
        common_1.Logger.log(uploaded);
        return res.json({ key: uploaded.Key });
    }
    async uploadSubtitles(file, source) {
        const media = await this.multimediaAPI.getMedia({ 'sources._id': source });
        if (!media) {
            throw new common_1.NotFoundException('Source not found', '8008');
        }
        if (!file) {
            throw new common_1.BadRequestException('Missing file', '7001');
        }
        const sourceDB = media.sources.id(source);
        file.originalname = uuid.v4() + path.extname(file.originalname);
        const masterList = webvtt.hls.hlsSegmentPlaylist(file.buffer.toString(), 10);
        const masterFile = { buffer: Buffer.from(masterList), originalname: 'index.m3u8', mimetype: 'application/x-mpegURL' };
        const uri = `movies/${media._id.toString()}/transcoded/subs/${sourceDB.language.code}/${uniqid.process()}/`;
        const subURI = await this.filesAPI.uploadFile(masterFile, uri);
        if (!subURI.hasOwnProperty('Key')) {
            throw new common_1.InternalServerErrorException('Cant upload master list', '7004');
        }
        const segments = webvtt.hls.hlsSegment(file.buffer.toString(), 10, 0);
        for (let i = 0; i <= segments.length; i++) {
            if (segments[i]) {
                const segmentFile = { buffer: Buffer.from(segments[i].content), originalname: segments[i].filename, mimetype: 'text/vtt' };
                await this.filesAPI.uploadFile(segmentFile, uri);
            }
        }
        await this.sourceAPI.rebuildMasterList(media);
        return await media.save();
    }
    async setAudio(file, source) {
        const media = await this.multimediaAPI.getMedia({ 'sources._id': source });
        if (!media) {
            throw new common_1.NotFoundException('Source not found', '8008');
        }
        if (!file) {
            throw new common_1.BadRequestException('Missing file', '7001');
        }
        const sourceDB = media.sources.id(source);
        file.originalname = uuid.v4() + path.extname(file.originalname);
        const uploaded = await this.filesAPI.uploadFile(file, `movies/${media._id.toString()}/raw/audios/${sourceDB.language.code}/`);
        if (!uploaded) {
            throw new common_1.InternalServerErrorException('Cant Upload', '7002');
        }
        const output = `movies/${media._id.toString()}/transcoded/tracks/${sourceDB.language.code}/${uniqid.process()}/`;
        const jobID = await this.transcoderAPI.createAudioJob(uploaded.Key, output, file.originalname.split('.')[0]);
        sourceDB.jobID = jobID.Job.Id;
        sourceDB.audio = output + file.originalname.split('.')[0] + '.m3u8';
        const task = await this.taskAPI.createTask({ jobID: jobID.Job.Id, source: sourceDB._id, isMaster: false });
        await task.save();
        await this.sourceAPI.rebuildMasterList(media);
        return await media.save();
    }
    async uploadCover(file, source, query) {
        const media = await this.multimediaAPI.getMedia({ 'sources._id': source });
        if (!media) {
            throw new common_1.NotFoundException('Source not found', '8008');
        }
        if (!file) {
            throw new common_1.BadRequestException('Missing file', '7001');
        }
        const sourceDB = media.sources.id(source);
        const dimensions = image_size_1.imageSize(file.buffer);
        if (query.type === 'horizontal') {
            if (dimensions.width !== 1200 || dimensions.height !== 800) {
                throw new common_1.BadRequestException('Image must be 1200x800', '8006');
            }
        }
        else {
            if (dimensions.width !== 800 || dimensions.height !== 1200) {
                throw new common_1.BadRequestException('Image must be 800x1200', '8007');
            }
        }
        file.originalname = uuid.v4() + path.extname(file.originalname);
        const uploaded = await this.filesAPI.uploadFile(file, `movies/${media._id}/cover/${sourceDB.language.code}/`, true);
        if (!uploaded) {
            throw new common_1.InternalServerErrorException('Cant Upload', '7002');
        }
        query.type === 'horizontal' ? sourceDB.coverHorizontal = uploaded.Key : sourceDB.coverVertical = uploaded.Key;
        await media.save();
        return sourceDB;
    }
};
__decorate([
    common_1.Get(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard, activesub_guard_1.ActivesubGuard),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], SourcesController.prototype, "getSoure", null);
__decorate([
    common_1.Put(':source'),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter),
    common_1.UseFilters(validation_error_filter_1.ValidationErrorFilter),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN'])),
    __param(0, common_1.Param('source', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __param(1, common_1.Body(new pick_pipe_1.PickPipe(['name', 'tagline', 'description', 'keywords']))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, update_source_dto_1.UpdateSourceDTO]),
    __metadata("design:returntype", Promise)
], SourcesController.prototype, "updateSource", null);
__decorate([
    common_1.Get(':movie/*'),
    __param(0, common_1.Res()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SourcesController.prototype, "getSourceStream", null);
__decorate([
    common_1.Post('uploadMedia/:id'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR', 'USER'])),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file', { limits: { fileSize: 300000000 } }), new valid_filed_extensions_interceptor_1.ValidFiledExtensionsInterceptor(['video/mp4'])),
    __param(0, common_1.UploadedFile('file')), __param(1, common_1.Param('id')), __param(2, common_1.Res()), __param(3, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], SourcesController.prototype, "uploadMedia", null);
__decorate([
    common_1.Put('subtitles/:source'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file', { limits: { fileSize: 5000000 } })),
    __param(0, common_1.UploadedFile('file')), __param(1, common_1.Param('source')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SourcesController.prototype, "uploadSubtitles", null);
__decorate([
    common_1.Put('audios/:source'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file', { limits: { fileSize: 5000000 } }), new valid_filed_extensions_interceptor_1.ValidFiledExtensionsInterceptor(['audio/mpeg', 'audio/mp3'])),
    __param(0, common_1.UploadedFile('file')), __param(1, common_1.Param('source')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SourcesController.prototype, "setAudio", null);
__decorate([
    common_1.Put('upload/:source/cover'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN', 'CREATOR'])),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file', { limits: { fileSize: 5000000 } }), new valid_filed_extensions_interceptor_1.ValidFiledExtensionsInterceptor(['image/jpeg', 'image/png'])),
    __param(0, common_1.UploadedFile('file')), __param(1, common_1.Param('source')), __param(2, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, update_cover_dto_1.UpdateCoverDTO]),
    __metadata("design:returntype", Promise)
], SourcesController.prototype, "uploadCover", null);
SourcesController = __decorate([
    common_1.Controller('sources'),
    __metadata("design:paramtypes", [multimedia_service_1.MultimediaService, files_service_1.FilesService, languages_service_1.LanguagesService,
        transcoder_service_1.TranscoderService, tasks_service_1.TasksService, sources_service_1.SourcesService])
], SourcesController);
exports.SourcesController = SourcesController;
//# sourceMappingURL=sources.controller.js.map