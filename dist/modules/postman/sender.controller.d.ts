import { SenderService } from './sender.service';
import { Sender } from './interfaces/sender.interface';
import { CreateSenderDTO } from './DTO/create-sender.dto';
export declare class SenderController {
    private senderAPI;
    constructor(senderAPI: SenderService);
    getSenders(): Promise<Sender[]>;
    deleteSender(id: string): Promise<Sender>;
    createSender(sender: CreateSenderDTO, req: any): Promise<Sender>;
}
