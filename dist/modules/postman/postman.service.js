"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
let PostmanService = class PostmanService {
    constructor(PostmanModel) {
        this.PostmanModel = PostmanModel;
    }
    async createMail(mail) {
        const mailDB = new this.PostmanModel(mail);
        return await mailDB.save();
    }
    async getMails(query) {
        const { limit, skip, options } = query;
        return await this.PostmanModel.find(options)
            .limit(Number(limit) > 100 ? 100 : Number(limit))
            .skip(Math.abs(Number(skip)) || 0)
            .populate('insertBy', 'name role')
            .sort({ createdOn: 'desc' });
    }
    async getMail(options) {
        return await this.PostmanModel.findOne(options)
            .populate('insertBy', 'email name');
    }
    async countMails(query) {
        const { options } = query;
        return await this.PostmanModel.countDocuments(options);
    }
};
PostmanService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('Postman')),
    __metadata("design:paramtypes", [mongoose_2.Model])
], PostmanService);
exports.PostmanService = PostmanService;
//# sourceMappingURL=postman.service.js.map