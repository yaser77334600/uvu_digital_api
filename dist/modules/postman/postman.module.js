"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const postman_controller_1 = require("./postman.controller");
const postman_service_1 = require("./postman.service");
const mongoose_1 = require("@nestjs/mongoose");
const postman_schema_1 = require("./schemas/postman.schema");
const sender_service_1 = require("./sender.service");
const sender_controller_1 = require("./sender.controller");
const sender_schema_1 = require("./schemas/sender.schema");
let PostmanModule = class PostmanModule {
};
PostmanModule = __decorate([
    common_1.Module({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: 'Postman', schema: postman_schema_1.PostmanSchema }]),
            mongoose_1.MongooseModule.forFeature([{ name: 'Sender', schema: sender_schema_1.senderSchema }])],
        providers: [postman_service_1.PostmanService, sender_service_1.SenderService],
        exports: [postman_service_1.PostmanService, sender_service_1.SenderService],
        controllers: [postman_controller_1.PostmanController, sender_controller_1.SenderController],
    })
], PostmanModule);
exports.PostmanModule = PostmanModule;
//# sourceMappingURL=postman.module.js.map