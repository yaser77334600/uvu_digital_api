import { Document } from 'mongoose';
export interface Sender extends Document {
    address: string;
    password: string;
    lastUsed: string;
    isLastUsed: boolean;
    createdOn: string;
    relays: number;
    insertBy: string;
}
