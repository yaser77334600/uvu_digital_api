import { Document } from 'mongoose';
import { Attempt } from './attempt.interface';
export interface Postman extends Document {
    body: string;
    from?: string[];
    to: string[];
    subject: string;
    attachments: string[];
    sended?: boolean;
    createdOn?: string;
    insertBy: string;
    attempts?: Attempt[];
    sendedTo: string[];
}
