import { Model } from 'mongoose';
import { Postman } from './interfaces/postman.interface';
export declare class PostmanService {
    private PostmanModel;
    constructor(PostmanModel: Model<Postman>);
    createMail(mail: Postman | any): Promise<Postman>;
    getMails(query?: any): Promise<Postman[]>;
    getMail(options: object): Promise<Postman>;
    countMails(query: any): Promise<number>;
}
