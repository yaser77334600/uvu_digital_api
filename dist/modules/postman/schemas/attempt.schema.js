"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.attemptSchema = new mongoose_1.Schema({
    triedAt: {
        type: Date,
        required: true,
    },
    succes: {
        type: Boolean,
        required: true,
    },
    reason: {
        type: String,
        required: true,
    },
    triedBy: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Sender',
        required: true,
    },
});
//# sourceMappingURL=attempt.schema.js.map