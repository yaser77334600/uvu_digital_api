"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const attempt_schema_1 = require("./attempt.schema");
exports.PostmanSchema = new mongoose_1.Schema({
    body: {
        type: String,
        required: true,
    },
    from: {
        type: String,
        default: 'no-reply@uvu.digital',
    },
    to: [String],
    attachments: [String],
    sended: {
        type: Boolean,
        default: false,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    insertBy: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'User',
        required: false,
    },
    subject: {
        type: String,
        trim: true,
        maxlength: 260,
    },
    sendedTo: [String],
    attempts: [attempt_schema_1.attemptSchema],
});
//# sourceMappingURL=postman.schema.js.map