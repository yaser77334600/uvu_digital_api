"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.senderSchema = new mongoose_1.Schema({
    address: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
        select: false,
    },
    lastUsed: {
        type: Date,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    relays: {
        type: Number,
        default: 0,
        max: 250,
    },
    insertBy: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
});
//# sourceMappingURL=sender.schema.js.map