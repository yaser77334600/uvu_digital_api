"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const sender_service_1 = require("./sender.service");
const mongo_error_filter_1 = require("../../common/filters/mongo-error.filter");
const cast_error_filter_1 = require("../../common/filters/cast-error.filter");
const auth_guard_1 = require("../../common/guard/auth.guard");
const role_guard_1 = require("../../common/guard/role.guard");
const validate_object_id_pipe_1 = require("../../common/pipes/validate-object-id.pipe");
const pick_pipe_1 = require("../../common/pipes/pick.pipe");
const create_sender_dto_1 = require("./DTO/create-sender.dto");
let SenderController = class SenderController {
    constructor(senderAPI) {
        this.senderAPI = senderAPI;
    }
    async getSenders() {
        return await this.senderAPI.getSenders();
    }
    async deleteSender(id) {
        return await this.senderAPI.deleteSender({ _id: id });
    }
    async createSender(sender, req) {
        const tokenUser = req.token.user;
        Object.assign(sender, { insertBy: tokenUser._id });
        return await this.senderAPI.createSender(sender);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter, cast_error_filter_1.CastErrorFilter),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN'])),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], SenderController.prototype, "getSenders", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN'])),
    __param(0, common_1.Param('id', new validate_object_id_pipe_1.ValidateObjectIdPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], SenderController.prototype, "deleteSender", null);
__decorate([
    common_1.Post(),
    common_1.UseFilters(mongo_error_filter_1.MongoErrorFilter),
    common_1.UseGuards(auth_guard_1.AuthGuard, new role_guard_1.RoleGuard(['ADMIN'])),
    __param(0, common_1.Body(new pick_pipe_1.PickPipe(['address', 'password']))),
    __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_sender_dto_1.CreateSenderDTO, Object]),
    __metadata("design:returntype", Promise)
], SenderController.prototype, "createSender", null);
SenderController = __decorate([
    common_1.Controller('sender'),
    __metadata("design:paramtypes", [sender_service_1.SenderService])
], SenderController);
exports.SenderController = SenderController;
//# sourceMappingURL=sender.controller.js.map