import { Model } from 'mongoose';
import { Sender } from './interfaces/sender.interface';
import { CreateSenderDTO } from './DTO/create-sender.dto';
export declare class SenderService {
    private SenderModel;
    constructor(SenderModel: Model<Sender>);
    createSender(sender: Sender | CreateSenderDTO): Promise<Sender>;
    getSenders(options?: any): Promise<Sender[]>;
    getSender(options: any): Promise<Sender>;
    getFullSender(options: any): Promise<Sender>;
    getFullSenders(options?: any): Promise<Sender[]>;
    deleteSender(options: any): Promise<Sender>;
}
