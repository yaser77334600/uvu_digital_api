import { CanActivate, ExecutionContext } from '@nestjs/common';
import { UsersService } from '../../modules/users/users.service';
import { SubscriptionsService } from '../../modules/subscriptions/subscriptions.service';
export declare class ActivesubGuard implements CanActivate {
    private userAPI;
    private subscribeAPI;
    constructor(userAPI: UsersService, subscribeAPI: SubscriptionsService);
    canActivate(context: ExecutionContext): Promise<boolean>;
    checkCustomer(customerID: any): Promise<void>;
}
