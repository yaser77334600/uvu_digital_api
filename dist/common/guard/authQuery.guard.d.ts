import { CanActivate, ExecutionContext } from '@nestjs/common';
import { AuthService } from '../../modules/auth/auth.service';
import { UsersService } from '../../modules/users/users.service';
export declare class AuthQueryGuard implements CanActivate {
    private authAPI;
    private userAPI;
    constructor(authAPI: AuthService, userAPI: UsersService);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
