"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const users_service_1 = require("../../modules/users/users.service");
const subscriptions_service_1 = require("../../modules/subscriptions/subscriptions.service");
let ActivesubGuard = class ActivesubGuard {
    constructor(userAPI, subscribeAPI) {
        this.userAPI = userAPI;
        this.subscribeAPI = subscribeAPI;
    }
    async canActivate(context) {
        const request = context.switchToHttp().getRequest();
        const resp = context.switchToHttp().getResponse();
        if (!request.token) {
            throw new common_1.UnauthorizedException('Subscription expired or nonexistent', '7404');
        }
        const userToken = request.token.user;
        const user = await this.userAPI.getUser({ _id: userToken._id });
        if (!user) {
            throw new common_1.NotFoundException('User not found', '8404');
        }
        if (!user.customer) {
            throw new common_1.UnauthorizedException('Subscription expired or nonexistent', '7404');
        }
        await this.checkCustomer(user.customer);
        return true;
    }
    async checkCustomer(customerID) {
        const customer = await this.subscribeAPI.getCustomer(customerID);
        if (!customer) {
            throw new common_1.UnauthorizedException('Subscription expired or nonexistent', '7404');
        }
        else if (customer.subscriptions.data.length <= 0 || customer.subscriptions.data[0].status !== 'active') {
            throw new common_1.UnauthorizedException('Subscription expired or nonexistent', '7404');
        }
    }
};
ActivesubGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [users_service_1.UsersService, subscriptions_service_1.SubscriptionsService])
], ActivesubGuard);
exports.ActivesubGuard = ActivesubGuard;
//# sourceMappingURL=activesub.guard.js.map