"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const auth_service_1 = require("../../modules/auth/auth.service");
const mongoose = require("mongoose");
const users_service_1 = require("../../modules/users/users.service");
let LoggerInterceptor = class LoggerInterceptor {
    constructor(authAPI, userAPI) {
        this.authAPI = authAPI;
        this.userAPI = userAPI;
    }
    async intercept(context, next) {
        const HandlerName = context.getHandler().name;
        if (HandlerName === 'getSourceStream') {
            return next.handle();
        }
        else {
            const req = context.switchToHttp().getRequest();
            const token = req.get('Authorization');
            try {
                const decoded = this.authAPI.verifyToken(token);
                const _id = decoded.user._id;
                if (mongoose.Types.ObjectId.isValid(_id)) {
                    const userDB = await this.userAPI.getUser({ _id });
                    if (userDB) {
                        userDB.lastActivity = new Date();
                        userDB.save();
                    }
                }
            }
            catch (e) {
                common_1.Logger.log(e);
            }
            return next.handle();
        }
    }
};
LoggerInterceptor = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [auth_service_1.AuthService, users_service_1.UsersService])
], LoggerInterceptor);
exports.LoggerInterceptor = LoggerInterceptor;
//# sourceMappingURL=logger.interceptor.js.map