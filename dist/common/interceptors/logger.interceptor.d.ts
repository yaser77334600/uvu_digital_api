import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { AuthService } from '../../modules/auth/auth.service';
import { UsersService } from '../../modules/users/users.service';
export declare class LoggerInterceptor implements NestInterceptor {
    private authAPI;
    private userAPI;
    constructor(authAPI: AuthService, userAPI: UsersService);
    intercept(context: ExecutionContext, next: CallHandler): Promise<import("rxjs").Observable<any>>;
}
