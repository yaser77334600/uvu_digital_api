"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
let ValidFiledExtensionsInterceptor = class ValidFiledExtensionsInterceptor {
    constructor(mimetypes) {
        this.mimetypes = mimetypes;
    }
    intercept(context, next) {
        if ('file' in context.switchToHttp().getRequest()) {
            const mime = context.switchToHttp().getRequest().file.mimetype;
            if (this.mimetypes.indexOf(mime) === -1) {
                throw new common_1.BadRequestException('Invalid file type', '4001');
            }
        }
        return next.handle();
    }
};
ValidFiledExtensionsInterceptor = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [Array])
], ValidFiledExtensionsInterceptor);
exports.ValidFiledExtensionsInterceptor = ValidFiledExtensionsInterceptor;
//# sourceMappingURL=valid-filed-extensions.interceptor.js.map