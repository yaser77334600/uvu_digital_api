import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
export declare class ValidFiledExtensionsInterceptor implements NestInterceptor {
    private mimetypes;
    constructor(mimetypes: string[]);
    intercept(context: ExecutionContext, next: CallHandler): Observable<any>;
}
