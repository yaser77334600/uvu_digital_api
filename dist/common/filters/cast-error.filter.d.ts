import { ArgumentsHost, ExceptionFilter } from '@nestjs/common';
import { CastError } from 'mongoose';
export declare class CastErrorFilter implements ExceptionFilter {
    catch(exception: CastError, host: ArgumentsHost): void;
}
