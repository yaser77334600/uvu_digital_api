import { ArgumentsHost, ExceptionFilter } from '@nestjs/common';
export declare class ValidationErrorFilter implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost): void;
}
