"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const jsonwebtoken_1 = require("jsonwebtoken");
let JwtErrorFilter = class JwtErrorFilter {
    catch(exception, host) {
        const response = host.switchToHttp().getResponse();
        response.status(401).json({ message: 'Invalid token' });
    }
};
JwtErrorFilter = __decorate([
    common_1.Catch(jsonwebtoken_1.JsonWebTokenError)
], JwtErrorFilter);
exports.JwtErrorFilter = JwtErrorFilter;
//# sourceMappingURL=jwt-error.filter.js.map