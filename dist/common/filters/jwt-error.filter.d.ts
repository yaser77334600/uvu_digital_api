import { ArgumentsHost, ExceptionFilter } from '@nestjs/common';
export declare class JwtErrorFilter<T> implements ExceptionFilter {
    catch(exception: T, host: ArgumentsHost): void;
}
