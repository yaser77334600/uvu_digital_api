import { PipeTransform } from '@nestjs/common';
export declare class UuidPipe implements PipeTransform {
    transform(value: any): any;
}
