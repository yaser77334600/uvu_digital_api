import { PipeTransform } from '@nestjs/common';
export declare class MatchArrayQueryPipe implements PipeTransform {
    private keys;
    constructor(keys: string[]);
    transform(value: any): any;
}
