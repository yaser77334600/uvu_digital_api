import { ArgumentMetadata, PipeTransform } from '@nestjs/common';
export declare class PickPipe implements PipeTransform {
    private keys;
    constructor(keys: any[]);
    transform(value: any, metadata: ArgumentMetadata): Pick<any, any>;
}
