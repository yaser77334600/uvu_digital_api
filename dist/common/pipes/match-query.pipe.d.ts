import { PipeTransform } from '@nestjs/common';
export declare class MatchQueryPipe implements PipeTransform {
    private keys;
    constructor(keys: string[]);
    transform(value: any): any;
}
