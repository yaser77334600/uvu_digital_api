"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const bcrypt = require("bcryptjs");
let PasswordEncryptPipe = class PasswordEncryptPipe {
    transform(value) {
        if (!value.password) {
            throw new common_1.BadRequestException('Missing password');
        }
        if (value.password.length < 8) {
            throw new common_1.BadRequestException('Minimun 8 characters', '8004');
        }
        value.password = bcrypt.hashSync(value.password, 10);
        return value;
    }
};
PasswordEncryptPipe = __decorate([
    common_1.Injectable()
], PasswordEncryptPipe);
exports.PasswordEncryptPipe = PasswordEncryptPipe;
//# sourceMappingURL=password-encrypt.pipe.js.map