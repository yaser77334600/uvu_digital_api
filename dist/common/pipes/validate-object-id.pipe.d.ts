import { PipeTransform } from '@nestjs/common';
export declare class ValidateObjectIdPipe implements PipeTransform {
    private keys?;
    constructor(keys?: string[]);
    transform(values: string): string;
}
