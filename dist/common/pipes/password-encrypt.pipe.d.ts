import { PipeTransform } from '@nestjs/common';
export declare class PasswordEncryptPipe implements PipeTransform {
    transform(value: any): any;
}
