"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose = require("mongoose");
let ValidateObjectIdPipe = class ValidateObjectIdPipe {
    constructor(keys) {
        this.keys = keys;
    }
    transform(values) {
        const errors = [];
        if (this.keys) {
            this.keys.forEach(key => {
                if (values[key]) {
                    if (Array.isArray(values[key])) {
                        values[key].forEach(value => !mongoose.Types.ObjectId.isValid(value) ? errors.push(key) : '');
                    }
                    else {
                        !mongoose.Types.ObjectId.isValid(values[key]) ? errors.push(key) : '';
                    }
                }
            });
        }
        else {
            !mongoose.Types.ObjectId.isValid(values) ? errors.push(values) : '';
        }
        if (errors.length > 0) {
            throw new common_1.BadRequestException(`Invalid ID for: ${errors.join(',')}`);
        }
        return values;
    }
};
ValidateObjectIdPipe = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [Array])
], ValidateObjectIdPipe);
exports.ValidateObjectIdPipe = ValidateObjectIdPipe;
//# sourceMappingURL=validate-object-id.pipe.js.map