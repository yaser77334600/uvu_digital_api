"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const _ = require("underscore");
let MatchQueryPipe = class MatchQueryPipe {
    constructor(keys) {
        this.keys = keys;
    }
    transform(value) {
        if (!value.hasOwnProperty('options')) {
            Object.assign(value, { options: { active: !value.active ? true : value.active === 'true' ? true : false } });
        }
        else {
            _.assign(value.options, { active: !value.active ? true : value.active === 'true' ? true : false });
        }
        this.keys.forEach(key => {
            if (value[key]) {
                value.options[key] = new RegExp(value[key].replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'gi');
            }
        });
        return value;
    }
};
MatchQueryPipe = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [Array])
], MatchQueryPipe);
exports.MatchQueryPipe = MatchQueryPipe;
//# sourceMappingURL=match-query.pipe.js.map