import { Document, Schema } from 'mongoose';

export interface Activation extends Document {
    user: Schema.Types.ObjectId;
    hash: number;
    expiresOn: Date;
    active: boolean;
}
