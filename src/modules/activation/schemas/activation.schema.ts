import { Schema } from 'mongoose';

export const ActivationSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'Invalid user'],
    },
    hash: {
        type: Number,
        required: [true, 'Invalid hash'],
        unique: true,
    },
    expiresOn: {
        type: Date,
        required: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
});
