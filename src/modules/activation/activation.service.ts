import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Activation } from './interfaces/activation.interface';
import { User } from '../users/interfaces/user.interface';
import * as moment from 'moment';
import { Postman } from '../postman/interfaces/postman.interface';
import { PostmanService } from '../postman/postman.service';

@Injectable()
export class ActivationService {
    constructor(@InjectModel('Activation') private ActivationModel: Model<Activation>, private PostmanAPI: PostmanService) {}

    async getActivation(options): Promise<Activation> {
        return await this.ActivationModel.findOne(options);
    }

    async deleteActivation(options) {
        return await this.ActivationModel.deleteOne(options);
    }

    async createActivation(user: User): Promise<Activation> {
        const hash = Math.floor(100000 + Math.random() * 900000);
        const expiresOn = moment(new Date()).add(30, 'minutes').toISOString();
        const activation = new this.ActivationModel({ user: user._id, hash, expiresOn });
        return await activation.save();
    }

    async resendActivation(user: User): Promise<Postman> {
        await this.deleteActivation({ user: user._id });
        const newActivation = await this.createActivation(user);
        return await this.sendActivation(user, newActivation);
    }

    async sendActivation(user: User, activation: Activation): Promise<Postman> {
        const mail = {
            to: user.email,
            from: 'no-reply@uvu.digital',
            subject: `Activation code UVU`,
            body: `${ activation.hash }`,
        };

        const postman = await this.PostmanAPI.createMail(mail);
        return postman;
    }
}
