import { Module } from '@nestjs/common';
import { ActivationService } from './activation.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ActivationSchema } from './schemas/activation.schema';
import { PostmanModule } from '../postman/postman.module';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Activation', schema: ActivationSchema }]), PostmanModule],
  providers: [ActivationService],
  exports: [ActivationService],
})
export class ActivationModule {}
