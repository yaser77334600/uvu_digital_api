import { Controller, Get, Query, Param, Post, Body, Delete, Put,
    UseFilters, NotFoundException, UseGuards, Req, UnauthorizedException,
    BadRequestException, GatewayTimeoutException, ConflictException, Res, Logger } from '@nestjs/common';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { UsersService } from './users.service';
import { CreateUserDTO } from './DTO/create-user.dto';
import { User } from './interfaces/user.interface';
import { UuidPipe } from '../../common/pipes/uuid.pipe';
import { PasswordEncryptPipe } from '../../common/pipes/password-encrypt.pipe';
import { UpdateUserDTO } from './DTO/update-user.dto';
import { MatchQueryPipe } from '../../common/pipes/match-query.pipe';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import * as moment from 'moment';
import { AuthService } from '../auth/auth.service';
import { ActivationService } from '../activation/activation.service';
import { SubscriptionsService } from '../subscriptions/subscriptions.service';
import { MultimediaService } from '../multimedia/multimedia.service';
import { Multimedia } from '../multimedia/interfaces/multimedia.interface';

@Controller('users')
export class UsersController {
    constructor(private userAPI: UsersService, private activationAPI: ActivationService, private authAPI: AuthService,
                private BillingAPI: SubscriptionsService, private multimediaAPI: MultimediaService) {}

    @Get()
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN']))
    async getUsers(@Query(new MatchQueryPipe(['name', 'email', 'country', 'address', 'phone'])) query): Promise<{ users: User[], total: number }> {
        const users = await this.userAPI.getUsers(query);
        const total = await this.userAPI.countUsers(query);
        return { users, total };
    }

    @Get(':id')
    @UseGuards(AuthGuard)
    async getUser(@Param('id', new ValidateObjectIdPipe()) userID: string, @Req() req): Promise<User> {
        // Only ADMIN can check other users
        const tokenUser = req.token.user as User;
        if (tokenUser.role !== 'ADMIN' && tokenUser._id.toString() !== userID) {
            throw new UnauthorizedException('Insufficient permissions', '0007');
        }

        const user = await this.userAPI.getUser({ _id: userID, active: true });
        if (!user) { throw new NotFoundException('User not found', '1001'); }
        return user;
    }

    @Get('/info/me')
    @UseGuards(AuthGuard)
    async getOwnInfo(@Req() req): Promise<{ user: User, subscription: any }> {
        const tokenUser = req.token.user as User;
        console.log(tokenUser+" entro aqui")
        let subscription;
        const user = await this.userAPI.getUser({ _id: tokenUser._id, active: true });
        if (!user) { throw new NotFoundException('User not found', '1001'); }

        if (user.customer && user.customer !== '') {
            subscription =  await this.BillingAPI.getCustomer(user.customer);
        }
        return { user, subscription };
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    async createUser(@Body(new UuidPipe(), new PasswordEncryptPipe(),
                     new PickPipe(['name', 'email', 'phone', 'address', 'country', 'password', 'uuid'])) user: CreateUserDTO): Promise<User> {
        const existUser = await this.userAPI.getUser({ email: user.email });
        if (existUser) { throw new ConflictException('This user already exist', '1002'); }
        const userDB = await this.userAPI.createUser(user);
        const activation = await this.activationAPI.createActivation(userDB);
        await this.activationAPI.sendActivation(userDB, activation);
        return userDB;
    }

    @Put(':id')
    @UseGuards(AuthGuard)
    async updateUser(@Param('id', new ValidateObjectIdPipe()) userID: string,
                     @Body(new PickPipe(['name', 'country', 'address', 'phone', 'role'])) user: UpdateUserDTO, @Req() req): Promise<User> {
        const tokenUser = req.token.user as User;
        const userDB = await this.userAPI.getUser({ _id: tokenUser._id, active: true });
        if (!userDB) { throw new NotFoundException('User not found', '1001'); }

        if (tokenUser._id.toString() !== userID && tokenUser.role !== 'ADMIN') {
            throw new UnauthorizedException('insufficient permissions', '0007');
        }

        if (tokenUser.role !== 'ADMIN') {
            delete user.role;
        }
        return await this.userAPI.updateUser({ _id: userID, active: true }, user);
    }

    @Get('mylist/all')
    @UseGuards(AuthGuard)
    async getMyList(@Req() req): Promise<Multimedia[]> {
        let exclude = '';
        const tokenUser = req.token.user as User;
        if (tokenUser.role === 'USER') {
            exclude += '-status -rejectedReason -jobID -transcoded -transcoding -insertBy';
        }

        const userDB = await this.userAPI.getUser({ _id: tokenUser._id, active: true });
        if (!userDB) { throw new NotFoundException('User not found', '1001'); }

        return await this.multimediaAPI.getMultimedia({ options: { _id: { $in: userDB.myList }, active: true, status: 'published' } });
    }

    @Put('mylist/:id')
    @UseGuards(AuthGuard)
    async addToMyList(@Param('id', new ValidateObjectIdPipe()) multimediaID: string, @Req() req): Promise<User> {
        const tokenUser = req.token.user as User;
        const userDB = await this.userAPI.getUser({ _id: tokenUser._id, active: true });
        if (!userDB) { throw new NotFoundException('User not found', '1001'); }

        const multimedia = await this.multimediaAPI.getMedia({ _id: multimediaID, active: true, status: 'published' });
        if (!multimedia) { throw new NotFoundException('Media not found', '8001'); }

        const pos = userDB.myList.map(media => media._id.toString()).indexOf(multimediaID);
        if (pos === -1) { userDB.myList.push(multimediaID as any); }

        return await userDB.save();
    }

    @Delete('mylist/:id')
    @UseGuards(AuthGuard)
    async deleteFromMyList(@Param('id', new ValidateObjectIdPipe()) multimediaID: string, @Req() req): Promise<User> {
        const tokenUser = req.token.user as User;
        const userDB = await this.userAPI.getUser({ _id: tokenUser._id, active: true });
        if (!userDB) { throw new NotFoundException('User not found', '1001'); }

        return await this.userAPI.updateUser({ _id: userDB._id }, { $pull: { myList: multimediaID }});
    }

    @Delete(':id')
    @UseGuards(AuthGuard)
    async deleteUser(@Param('id', new ValidateObjectIdPipe()) userID, @Req() req): Promise<User> {
        const tokenUser = req.token.user as User;
        if (tokenUser.role !== 'ADMIN' && tokenUser._id.toString() !== userID) {
            throw new UnauthorizedException('Insufficient permissions', '0007');
        }

        const userDB = await this.userAPI.getUser({ _id: userID, active: true });
        if (!userDB) { throw new NotFoundException('User not found', '1001'); }

        if (userDB.customer) {
            await this.BillingAPI.deleteSub(userDB.customer);
        }
        userDB.active = false;
        return await userDB.save();
    }

    @Get(':id/verify/:hash')
    async verifyEmailUser(@Param('id', new ValidateObjectIdPipe()) userID: string, @Param('hash') hash: string) {
        // Get User
        const user = await this.userAPI.getUser({ _id: userID, active: true });
        if (!user) { throw new NotFoundException('User not found', '1001'); }
        if (user.verifiedEmail) { throw new BadRequestException('This user is already verified', '0003'); }

        // Get Activation
        const activation = await this.activationAPI.getActivation({ hash, active: true });
        if (!activation) { throw new BadRequestException('Invalid activation token', '0004'); }

        if (activation.user.toString() !== user._id.toString()) { throw new BadRequestException('Invalid activation token', '0004'); }
        const now = moment(new Date());
        const expires = moment(activation.expiresOn);

        // Get difference between dates
        const diff = expires.diff(now, 'minutes');
        if (diff <= 0) {
            await this.activationAPI.deleteActivation({ hash });
            throw new BadRequestException('Code Expired', '0005');
        }

        // Update User
        await this.activationAPI.deleteActivation({ hash });
        const userDB = await this.userAPI.updateUser({ _id: user._id}, { verifiedEmail: true });
        return { auth: this.authAPI.getToken(userDB) };
    }

    @Get(':id/resend')
    async resendCode(@Param('id', new ValidateObjectIdPipe()) userID: string) {
        // Get User
        const user = await this.userAPI.getUser({ _id: userID, active: true });
        if (!user) { throw new NotFoundException('User not found', '1001'); }

        if (user.verifiedEmail) { throw new BadRequestException('This user is already verified', '0003'); }
        const mail = await this.activationAPI.resendActivation(user);
        if (!mail) { throw new GatewayTimeoutException('Failed to send activation email', '0005'); }
        return { sended: true };
    }
}
