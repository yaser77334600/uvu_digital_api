import { Document } from 'mongoose';

export interface Recover extends Document {
    user: string;
    active: boolean;
    createdOn: Date;
    hash: string;
    expiresOn: Date;
}
