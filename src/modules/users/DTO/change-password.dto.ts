import { IsNotEmpty, IsString, Length } from 'class-validator';

export class ChangePasswordDTO {
    @IsNotEmpty()
    @IsString()
    hash: string;

    @IsNotEmpty()
    @Length(8, 20)
    password: string;
}
