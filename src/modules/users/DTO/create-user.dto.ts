import { IsNotEmpty, IsString, MaxLength, IsEmail, MinLength, Length, IsISO31661Alpha2 } from 'class-validator';

export class CreateUserDTO {
    @IsNotEmpty()
    @MaxLength(30)
    @IsString()
    name: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    @Length(10, 15)
    phone: string;

    @IsNotEmpty()
    @IsISO31661Alpha2()
    country: string;

    @IsNotEmpty()
    @Length(5, 240)
    address: string;

    @IsNotEmpty()
    @Length(8, 20)
    password: string;
}
