import { IsNotEmpty, IsEmail } from 'class-validator';

export class CreateRecoveryDTO {
    @IsNotEmpty()
    @IsEmail()
    email: string;
}
