import { IsNotEmpty, IsString, MaxLength, IsEmail, MinLength, Length, IsISO31661Alpha2, IsOptional, IsEnum } from 'class-validator';

export class UpdateUserDTO {
    @IsOptional()
    @IsNotEmpty()
    @MaxLength(30)
    @IsString()
    name: string;

    @IsOptional()
    @IsNotEmpty()
    @Length(10, 15)
    phone?: string;

    @IsOptional()
    @IsNotEmpty()
    @IsISO31661Alpha2()
    country?: string;

    @IsOptional()
    @IsNotEmpty()
    @Length(5, 240)
    address?: string;

    @IsOptional()
    @IsEnum(['ADMIN', 'CREATOR', 'USER'])
    role: string;
}
