import { Schema } from 'mongoose';

export const RecoverSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    active: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    hash: {
        type: String,
        required: true,
    },
    expiresOn: {
        type: Date,
        required: true,
    },
});
