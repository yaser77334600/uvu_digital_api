import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Recover } from './interfaces/recover.interface';
import * as moment from 'moment';
import * as uuid from 'uuid';
import { User } from './interfaces/user.interface';
import { PostmanService } from '../postman/postman.service';

@Injectable()
export class RecoveryService {
    constructor(@InjectModel('Recovery') private RecoverModel: Model<Recover>, private PostmanAPI: PostmanService) { }

    async getRecoveries(query?): Promise<Recover[]> {
        return await this.RecoverModel.find(query)
        .populate('user', 'name email');
    }

    async getRecovery(query?): Promise<Recover> {
        return await this.RecoverModel.findOne(query)
        .populate('user', 'name email');
    }

    async deleteRecovery(options) {
        return await this.RecoverModel.findOneAndUpdate(options, { active: false }, { new: true, runValidators: true, context: 'query' });
    }

    async resendRecovery(user) {
        await this.deleteRecovery({ user: user._id, active: true });
        const newRecover = await this.createRecover(user);
        return await this.sendRecover(user, newRecover);
    }

    async createRecover(user: User) {
        const hash = uuid.v4();
        const expiresOn = moment(new Date()).add(30, 'minutes').toISOString();
        const recoverDB = new this.RecoverModel({ user: user._id, hash, expiresOn });
        return await recoverDB.save();
    }

    async sendRecover(user: User, recover: Recover) {
        const mail = {
            to: user.email,
            from: 'no-reply@uvu.digital',
            subject: 'Your Password Reset Request',
            // tslint:disable-next-line:max-line-length
            body: `To reset the password for your ${ user.email }, click \n the web address below or copy and paste it into your browser: \nhttps://uvu.digital/account/change-password/${ recover.hash }`,
        };

        const postman = await this.PostmanAPI.createMail(mail);
        return postman;
    }
}
