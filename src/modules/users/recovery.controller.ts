import { Controller, Post, NotFoundException, Body, BadRequestException, GatewayTimeoutException, Res, Put } from '@nestjs/common';
import { UsersService } from './users.service';
import { ActivationService } from '../activation/activation.service';
import { CreateRecoveryDTO } from './DTO/create-recovery.dto';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { RecoveryService } from './recovery.service';
import { ChangePasswordDTO } from './DTO/change-password.dto';
import * as moment from 'moment';
import { PasswordEncryptPipe } from '../../common/pipes/password-encrypt.pipe';

@Controller('recovery')
export class RecoveryController {
    constructor(private userAPI: UsersService, private activationAPI: ActivationService,
                private recoveryAPI: RecoveryService) {}

    @Post()
    async sendRecovery(@Body(new PickPipe(['email'])) recover: CreateRecoveryDTO, @Res() res) {
        const user = await this.userAPI.getUser({ email: recover.email, active: true });
        if (!user) { throw new NotFoundException('User not found', '1001'); }

        const recoveries = await this.recoveryAPI.getRecoveries({ user: user._id });

        if (recoveries.length >= 3) {
            throw new BadRequestException('Maximum number of emails exceeded', '0008');
        }

        const mail = await this.recoveryAPI.resendRecovery(user);
        if (!mail) { throw new GatewayTimeoutException('Could not send mail.' , 'A005'); }
        return res.json({ sended: true });
    }

    @Put()
    async changePassword(@Body(new PasswordEncryptPipe()) recover: ChangePasswordDTO, @Res() res) {
        const recovery = await this.recoveryAPI.getRecovery({ hash: recover.hash, active: true });
        if (!recovery) { throw new BadRequestException('Invalid activation token', '0004'); }

        const now = moment(new Date());
        const expires = moment(recovery.expiresOn);

        // Get difference between dates
        const diff = expires.diff(now, 'minutes');
        if (diff <= 0) {
            await this.recoveryAPI.deleteRecovery({ hash: recovery.hash });
            throw new BadRequestException('Code Expired', '0005');
        }

        // Update User
        await this.recoveryAPI.deleteRecovery({ hash: recovery.hash });
        await this.userAPI.updateUser({ _id: (recovery.user as any)._id }, { password: recover.password });

        return res.json({ changed: true });
    }
}
