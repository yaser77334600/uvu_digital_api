import { Module, Global } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './schemas/users.schema';
import { ActivationModule } from '../activation/activation.module';
import { RecoveryController } from './recovery.controller';
import { RecoverSchema } from './schemas/recovery.schema';
import { RecoveryService } from './recovery.service';
import { PostmanModule } from '../postman/postman.module';
import { MultimediaModule } from '../multimedia/multimedia.module';

@Global()
@Module({
  imports: [MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
  MongooseModule.forFeature([{ name: 'Recovery', schema: RecoverSchema }]), ActivationModule, PostmanModule, MultimediaModule],
  providers: [UsersService, RecoveryService],
  controllers: [UsersController, RecoveryController],
  exports: [UsersService],
})
export class UsersModule {}
