import { Module, Global } from '@nestjs/common';
import { MultimediaController } from './multimedia.controller';
import { MultimediaService } from './multimedia.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MultimediaSchema } from './schemas/multimedia.schema';
import { SourcesController } from './sources.controller';
import { FilesModule } from '../files/files.module';
import { TranscoderModule } from '../transcoder/transcoder.module';
import { LanguagesModule } from '../languages/languages.module';
import { SourcesService } from './sources.service';
import { GenresModule } from '../genres/genres.module';
import { SubscriptionsModule } from '../subscriptions/subscriptions.module';

@Global()
@Module({
  imports: [MongooseModule.forFeature([{ name: 'Multimedia', schema: MultimediaSchema }]), FilesModule, TranscoderModule,
  LanguagesModule, GenresModule, SubscriptionsModule],
  controllers: [MultimediaController, SourcesController],
  providers: [MultimediaService, SourcesService],
  exports: [MultimediaService, SourcesService],
})
export class MultimediaModule {}
