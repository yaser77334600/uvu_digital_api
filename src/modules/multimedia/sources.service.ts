import { Injectable, NotFoundException, InternalServerErrorException } from '@nestjs/common';
import { Multimedia } from './interfaces/multimedia.interface';
import { FilesService } from '../files/files.service';

@Injectable()
export class SourcesService {
    constructor(private filesAPI: FilesService) {}
    async rebuildMasterList(multimedia: Multimedia) {
        const masterListFile = await this.filesAPI.getFile('movies/' + multimedia._id.toString() + '/transcoded', 'index.m3u8');
        let masterList = masterListFile.Body.toString();
        if (!masterList) {
            throw new NotFoundException('Masterlist not found');
        }

        const tagPos = masterList.indexOf('##UVUCOMPILER');
        if (tagPos !== -1) {
            masterList = masterList.slice(0, tagPos - 1);
        }

        masterList += '\n##UVUCOMPILER\n';
        multimedia.sources.forEach(source => {
            if (source.subtitle) { masterList += this.addSubtitle(source); }
            if (source.audioTranscoded) { masterList += this.addTracks(source); }
        });
        masterList = masterList.replace(/AUDIO="audio-0"[\r\n]/g, 'AUDIO="audio-0",SUBTITLES="subs"\n');
        const uri = 'movies/' + multimedia._id.toString() + '/transcoded/';
        const file = { buffer: Buffer.from(masterList), originalname: 'index.m3u8', mimetype: 'text/vtt' };
        const upload = await this.filesAPI.uploadFile(file as Express.Multer.File, uri);
        if (!upload) { throw new InternalServerErrorException('Cant upload master list'); }
    }

    private addSubtitle(source) {
        const pos =  source.subtitle.indexOf('transcoded');
        if (pos !== -1) {
            const { language: { code }, subtitle } = source;
            const uri = subtitle.slice(subtitle.indexOf('transcoded') + 11, subtitle.length);
            // tslint:disable-next-line:max-line-length
            return `\n#EXT-X-MEDIA:TYPE=SUBTITLES,GROUP-ID="subs",NAME="${ code }",DEFAULT=NO,AUTOSELECT=NO,FORCED=NO,LANGUAGE="${ code }",URI="${ uri }"`;
        }
    }

    private addTracks(source) {
        const pos =  source.audio.indexOf('transcoded');
        if (pos !== -1) {
            const { language: { code }, audioTranscoded: audio  } = source;
            const uri = audio.slice(audio.indexOf('transcoded') + 11, audio.length);
            // tslint:disable-next-line:max-line-length
            return `#EXT-X-MEDIA:TYPE=AUDIO,GROUP-ID="audio-0",CODECS="mp4a.40.2",LANGUAGE="${ code }",NAME="${ code }",DEFAULT=NO,URI="${ uri }"`;
        }
    }

}
