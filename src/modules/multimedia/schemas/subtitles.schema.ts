import { Schema } from 'mongoose';

export const SubtitleSchema = new Schema({
    code: {
        type: String,
        trim: true,
        required: true,
    },
    uri: {
        type: String,
        trim: true,
        required: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
});
