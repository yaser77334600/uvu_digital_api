import {
    Controller, Get, Post, Put, Delete, Param, Body, Query, UseFilters,
    NotFoundException, UseGuards, Res, UploadedFile, UseInterceptors, BadRequestException,
    InternalServerErrorException, ConflictException, Req, Logger, UnauthorizedException
} from '@nestjs/common';
import { MultimediaService } from './multimedia.service';
import { MatchQueryPipe } from '../../common/pipes/match-query.pipe';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { Multimedia } from './interfaces/multimedia.interface';
import { CreateMultimediaDTO } from './DTO/create-multimedia.dto';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { ValidationErrorFilter } from '../../common/filters/validation-error.filter';
import { UpdateMultimediaDTO } from './DTO/update-multimedia.dto';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import * as mongoose from 'mongoose';
import { FileInterceptor } from '@nestjs/platform-express';
import { ValidFiledExtensionsInterceptor } from '../../common/interceptors/valid-filed-extensions.interceptor';
import { imageSize } from 'image-size';
import * as path from 'path';
import * as uuid from 'uuid';
import { FilesService } from '../files/files.service';
import { MatchArrayQueryPipe } from '../../common/pipes/match-array-query.pipe';
import { SetMasterSourceDTO } from './DTO/set-master.dto';
import { TranscoderService } from '../transcoder/transcoder.service';
import { TasksService } from '../tasks/tasks.service';
import { AddSourceDTO } from './DTO/set-source.dto';
import { UpdateCoverDTO } from './DTO/update-cover.dto';
import { GenresService } from '../genres/genres.service';
import { User } from '../users/interfaces/user.interface';
import { AuthQueryGuard } from '../../common/guard/authQuery.guard';
import { SubscriptionsService } from '../subscriptions/subscriptions.service';

@Controller('multimedia')
export class MultimediaController {
    constructor(private multimediaAPI: MultimediaService, private filesAPI: FilesService, private transcoderAPI: TranscoderService,
        private taskAPI: TasksService, private genresAPI: GenresService, private subAPI: SubscriptionsService) { }

    @Get('home')
    @UseGuards(AuthGuard)
    async getMultimediaForHome() {
        const genres = await this.genresAPI.getGenres({ options: { active: true } });
        return genres;
    }

    @Get()
    @UseGuards(AuthGuard)
    async getMultimedia(@Req() req, @Query(new ValidateObjectIdPipe(['language']), new MatchArrayQueryPipe(['genres']),
        new MatchQueryPipe(['name', 'status'])) query):
        Promise<{ multimedia: Multimedia[], total: number, mylist: any[] }> {
        console.log(query)
        let exclude = '';
        const tokenUser = req.token.user as User;
        if (tokenUser.role === 'USER' || !query.onlyMe) {
          //  query.options.status = 'published';
            exclude += '-status -rejectedReason -jobID -transcoded -transcoding -insertBy';
        }
        if (tokenUser.role === 'CREATOR' && query.onlyMe) { query.options.insertBy = tokenUser._id; }
        // Match Language
        if (query.hasOwnProperty('language') && mongoose.Types.ObjectId.isValid(query.language)) {
            query.options['sources.language'] = query.language;
        }

        const total = await this.multimediaAPI.countMedia(query);
        console.log(query)
        const multimedia = await this.multimediaAPI.getMultimedia(query, exclude);
        return { total, multimedia, mylist: tokenUser.myList };
    }

    @Get('byname/:name')
    @UseGuards(AuthGuard)
    async getMultimediaByName(@Param('name') name): Promise<Multimedia> {
        const multimedia = await this.multimediaAPI.getMedia({ name });
        if (!multimedia) { throw new NotFoundException('Media not found', '8001'); }
        return multimedia;
    }

    @Get(':id')
    @UseGuards(AuthGuard)
    async getMedia(@Req() req, @Param('id', new ValidateObjectIdPipe()) multimediaID): Promise<Multimedia> {
        const tokenUser = req.token.user as User;
        let multimedia = await this.multimediaAPI.getMedia({ _id: multimediaID, active: true });
        if (!multimedia) { throw new NotFoundException('Media not found', '8001'); }
        multimedia = multimedia.toObject();
        (multimedia as any).inMyList = tokenUser.myList.map(media => media._id.toString()).indexOf((multimedia._id.toString() as any)) !== -1;
        return multimedia;
    }

    @Get('play/:id')
    @UseGuards(AuthQueryGuard)
    async playMedia(@Param('id', new ValidateObjectIdPipe()) multimediaID, @Req() req, @Res() res): Promise<Multimedia> {
        const tokenUser = req.token.user as User;
        const multimedia = await this.multimediaAPI.getMedia({ _id: multimediaID, active: true });
        if (!multimedia) { throw new NotFoundException('Media not found', '8001'); }
        if (!multimedia.file) {
            throw new NotFoundException('Media not found', '8001');
        } else if (tokenUser.role !== 'ADMIN' && (multimedia.status as any) !== 'published') {
            throw new NotFoundException('Media not found', '8001');
        }

        if (tokenUser.role !== 'ADMIN' && tokenUser.role !== 'SUPERVISOR') {
            if (!tokenUser.customer) {
                throw new NotFoundException('Customer not found', 'S002');
            }

            const customer = await this.subAPI.getFullCustomer(tokenUser.customer);
            if (!customer && customer.deleted) { throw new NotFoundException('Customer not found', 'S002'); }
            if (!customer.subscriptions || !customer.subscriptions.data || customer.subscriptions.data.length <= 0) {
                throw new NotFoundException('Subscription not found', 'S004');
            }
            if (customer.subscriptions.data[0].status !== 'active') {
                throw new UnauthorizedException('Subscription not found', 'S003');
            }
        }

        const route = multimedia.file.split('/');
        const file = route.pop();
        const headers = await this.filesAPI.getHeadObject(route.join('/'), file);
        const stream = this.filesAPI.getFileStream(route.join('/'), file);

        res.set('Content-Type', headers.ContentType);
        res.set('Content-Length', headers.ContentLength);
        res.set('Last-Modified', headers.LastModified);
        res.set('ETag', headers.ETag);

        return stream.pipe(res);
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseFilters(ValidationErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async createMultimedia(@Req() req, @Body(new ValidateObjectIdPipe(['genres', 'directors', 'actors', 'classification', 'language']),
        new PickPipe(['name', 'tagline', 'description', 'genres', 'directors', 'actors', 'classification', 'keywords', 'language']))
    multimedia: CreateMultimediaDTO): Promise<Multimedia> {
        const tokenUser = req.token.user as User;
        Object.assign(multimedia, { insertBy: tokenUser._id });
        return await this.multimediaAPI.createMultimedia(multimedia);
    }

    @Put(':id')
    @UseFilters(MongoErrorFilter)
    @UseFilters(ValidationErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async updateMultimedia(@Req() req, @Param('id', new ValidateObjectIdPipe()) multimediaID, @Body(
        new ValidateObjectIdPipe(['genres', 'directors', 'actors', 'classification', 'language']),
        new PickPipe(['name', 'tagline', 'description', 'genres', 'directors', 'actors', 'classification',
            'keywords', 'language', 'status', 'rejectedReason']))
    multimedia: UpdateMultimediaDTO): Promise<Multimedia> {
        const tokenUser = req.token.user as any;
        const media = await this.multimediaAPI.getMedia({ _id: multimediaID });
        if (!media) { throw new NotFoundException('Media not found', '8001'); }
        if (tokenUser.role !== 'ADMIN') {
            delete multimedia.rejectedReason;
            if (multimedia.status) {
                media.rejectedReason = '';
                multimedia.status = multimedia.status !== 'pending' ? 'draft' : 'pending';
            } else {
                multimedia.status = 'draft';
            }
        }
        return await this.multimediaAPI.updateMultimedia(multimedia, { _id: multimediaID });
    }

    @Put('setmaster/:id')
    @UseFilters(MongoErrorFilter)
    @UseFilters(ValidationErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async setMasterFiles(@Param('id', new ValidateObjectIdPipe()) id, @Body(new PickPipe(['file', 'trailer']))
    masterFiles: SetMasterSourceDTO): Promise<Multimedia> {
        const media = await this.multimediaAPI.getMedia({ _id: id });
        if (!media) { throw new NotFoundException('Media not found', '8001'); }

        // if (media.file && media.file !== '' && masterFiles.file && masterFiles.file !== '') {
        //     throw new BadRequestException('You cannot set the master file once processed. Please contact support to make this change', '8003');
        // }

        if (masterFiles.trailer && masterFiles.trailer !== '') {
            // const tmpTrailerFile = masterFiles.trailer.split('/');
            // const trailerName = tmpTrailerFile.pop();
            // const trailerExist = await this.filesAPI.fileExist(tmpTrailerFile.join('/'), trailerName, true);
            // if (!trailerExist) { throw new NotFoundException('Trailer not found', '8004'); }

            media.trailer = masterFiles.trailer;
        }

        if (masterFiles.file && masterFiles.file !== '') {
            const tmpMasterFile = masterFiles.file.split('/');
            const masterName = tmpMasterFile.pop();
            // const masterExist = await this.filesAPI.fileExist(tmpMasterFile.join('/'), masterName, false);
            // if (!masterExist) { throw new NotFoundException('Master file not found', '8005'); }

            // const output = 'movies/' + id + '/transcoded/';
            // const jobID = await this.transcoderAPI.createJob(masterFiles.file, output, masterName.split('.')[0]);

            // media.jobID = jobID.Job.Id;
            media.file = masterFiles.file;
            // const task = await this.taskAPI.createTask({ source: media._id, isMaster: true });
            // await task.save();
            media.transcoding = true;
        }

        return await media.save();
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN']))
    deleteMultimedia(@Param('id', new ValidateObjectIdPipe()) multimediaID): Promise<Multimedia> {
        return this.multimediaAPI.deleteMultimedia(multimediaID);
    }

    @Put('upload/:id/cover')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    @UseInterceptors(FileInterceptor('file', { limits: { fileSize: 5000000 } }),
        new ValidFiledExtensionsInterceptor(['image/jpeg', 'image/png']))
    async uploadCover(@UploadedFile('file') file: Express.Multer.File, @Param('id') id, @Query() query: UpdateCoverDTO): Promise<Multimedia> {
        const media = await this.multimediaAPI.getMedia({ _id: id });
        if (!media) { throw new NotFoundException('Media not found', '8001'); }
        if (!file) { throw new BadRequestException('Missing file', '7001'); }

        const dimensions = imageSize(file.buffer);
        if (query.type === 'horizontal') {
            if (dimensions.width !== 1200 || dimensions.height !== 800) { throw new BadRequestException('Image must be 1200x800', '8006'); }
        } else {
            if (dimensions.width !== 800 || dimensions.height !== 1200) { throw new BadRequestException('Image must be 800x1200', '8007'); }
        }

        file.originalname = uuid.v4() + path.extname(file.originalname);
        const uploaded = await this.filesAPI.uploadFile(file, `movies/${media._id}/cover/main/`, true);
        if (!uploaded) { throw new InternalServerErrorException('Cant Upload', '7002'); }

        query.type === 'horizontal' ? media.coverHorizontal = uploaded.Key : media.coverVertical = uploaded.Key;

        return await media.save();
    }

    @Post('upload/:id/media')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    @UseInterceptors(FileInterceptor('file', { limits: { fileSize: 300000000 } }),
        new ValidFiledExtensionsInterceptor(['video/mp4']))
    async uploadMasterFile(@UploadedFile('file') file: Express.Multer.File, @Param('id') id, @Res() res, @Query() query) {
        const media = await this.multimediaAPI.getMedia({ _id: id });
        if (!media) { throw new NotFoundException('Media not found', '8001'); }
        if (!file) { throw new BadRequestException('Missing file', '7001'); }

        const isTrailer = query.hasOwnProperty('trailer') && query.trailer;
        file.originalname = uuid.v4() + path.extname(file.originalname);

        file.originalname = uuid.v4() + path.extname(file.originalname);
        this.filesAPI.uploadFile(file, 'movies/' + id + '/raw/', isTrailer);
        return res.json({ key: `movies/${media._id.toString()}/raw/${file.originalname}` });
    }

    @Post('addsource/:multimedia')
    @UseFilters(MongoErrorFilter)
    @UseFilters(ValidationErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async setSource(@Param('multimedia', new ValidateObjectIdPipe()) id, @Body(new ValidateObjectIdPipe(['language']),
        new PickPipe(['name', 'tagline', 'description', 'keywords', 'language'])) source: AddSourceDTO): Promise<Multimedia> {
        const media = await this.multimediaAPI.getMedia({ _id: id });
        if (!media) { throw new NotFoundException('Media not found', '8001'); }

        let exist = false;
        media.sources.forEach((src) => {
            if (src.language.toString() === src.language) { exist = true; }
        });

        if (exist) { throw new ConflictException('The current language already exist in sources', '8002'); }
        media.sources.push(source as any);
        return await media.save();
    }
}
