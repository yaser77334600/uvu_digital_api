import { IsNotEmpty, IsOptional, MaxLength, IsArray, IsString, MinLength } from 'class-validator';

export class UpdateSourceDTO {
    @IsOptional()
    @IsString()
    @MaxLength(120)
    name: string;

    @IsOptional()
    @MaxLength(80)
    @IsString()
    tagline: string;

    @IsOptional()
    @MaxLength(800)
    @IsString()
    description: string;

    @IsOptional()
    @IsArray()
    keywords: string[];
}
