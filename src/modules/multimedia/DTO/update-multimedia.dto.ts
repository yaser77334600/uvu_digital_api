import { IsNotEmpty, IsOptional, MaxLength, IsArray, IsDateString, IsString, IsNumber, Max, Min, IsEnum } from 'class-validator';

export class UpdateMultimediaDTO {
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    @MaxLength(120)
    name: string;

    @IsOptional()
    @MaxLength(80)
    @IsString()
    tagline: string;

    @IsOptional()
    @MaxLength(800)
    @IsString()
    description: string;

    @IsOptional()
    @IsNotEmpty()
    @IsArray()
    genres: string[];

    @IsOptional()
    @IsArray()
    directors: string[];

    @IsOptional()
    @IsArray()
    actors: string[];

    @IsOptional()
    @IsNotEmpty()
    @IsString()
    classification: string;

    @IsOptional()
    @IsArray()
    keywords: string[];

    @IsOptional()
    @IsEnum(['draft', 'pending', 'published', 'rejected'])
    status: string;

    @IsOptional()
    @IsString()
    @MaxLength(3000)
    rejectedReason: string;
}
