import { IsNotEmpty, IsOptional, MaxLength, IsArray, IsString, MinLength } from 'class-validator';

export class AddSourceDTO {
    @IsNotEmpty()
    @IsString()
    @MaxLength(120)
    name: string;

    @IsOptional()
    @MaxLength(80)
    @IsString()
    tagline: string;

    @IsOptional()
    @MaxLength(800)
    @IsString()
    description: string;

    @IsNotEmpty()
    @MaxLength(24)
    @MinLength(24)
    @IsString()
    language: string;

    @IsOptional()
    @IsArray()
    keywords: string[];
}
