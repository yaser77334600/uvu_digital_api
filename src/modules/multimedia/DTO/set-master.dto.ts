import { IsNotEmpty, IsOptional, MaxLength, IsArray, IsString, MinLength } from 'class-validator';

export class SetMasterSourceDTO {
    @IsOptional()
    @IsString()
    file: string;

    @IsOptional()
    @IsString()
    trailer: string;
}
