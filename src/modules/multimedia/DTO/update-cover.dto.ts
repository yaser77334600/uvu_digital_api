import { IsNotEmpty, IsEnum } from 'class-validator';

export class UpdateCoverDTO {
    @IsNotEmpty()
    @IsEnum(['horizontal', 'vertical'])
    type: string;
}
