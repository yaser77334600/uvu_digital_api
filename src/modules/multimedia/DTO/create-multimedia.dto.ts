import { IsNotEmpty, IsOptional, MaxLength, IsArray, IsDateString, IsString, IsNumber, Max, Min } from 'class-validator';

export class CreateMultimediaDTO {
    @IsNotEmpty()
    @IsString()
    @MaxLength(120)
    name: string;

    @IsOptional()
    @MaxLength(80)
    @IsString()
    tagline: string;

    @IsOptional()
    @MaxLength(800)
    @IsString()
    description: string;

    @IsNotEmpty()
    @IsArray()
    genres: string[];

    @IsOptional()
    @IsArray()
    directors: string[];

    @IsOptional()
    @IsArray()
    actors: string[];

    @IsNotEmpty()
    @IsString()
    classification: string;

    @IsOptional()
    @IsArray()
    keywords: string[];
}
