import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Multimedia } from './interfaces/multimedia.interface';
import { CreateMultimediaDTO } from './DTO/create-multimedia.dto';

@Injectable()
export class MultimediaService {
    constructor(@InjectModel('Multimedia') private MultimediaModel: Model<Multimedia>) {}

    async getMultimedia(query?, exclude = '') {
        const { limit, skip, options } = query;
        return await this.MultimediaModel.find(options)
        .limit(Number(limit) > 100 ? 100 : Number(limit))
        .skip(Math.abs(Number(skip)) || 0)
        .populate('genres', 'name')
        .populate('directors', 'name category')
        .populate('actors', 'name category')
        .populate('language', 'code')
        .populate('sources.language', 'code')
        .populate('classification', 'name')
        .populate('insertBy', 'name')
        .select(exclude);
    }

    async getMedia(options) {
        return await this.MultimediaModel.findOne(options)
        .populate('genres', 'name')
        .populate('directors', 'name category')
        .populate('actors', 'name category')
        .populate('language', 'code')
        .populate('sources.language', 'code')
        .populate('classification', 'name')
        .populate('insertBy', 'name');
    }

    async countMedia(query?) {
        const { options } = query; 
       // return await this.MultimediaModel.countDocuments(options);
        return await this.MultimediaModel.estimatedDocumentCount(options);
    }

    async createMultimedia(multimedia: CreateMultimediaDTO): Promise<Multimedia> {
        const multimediaDB = new this.MultimediaModel(multimedia);
        return await multimediaDB.save();
    }

    async updateMultimedia(multimedia, query): Promise<Multimedia> {
        return await this.MultimediaModel.findOneAndUpdate(query, multimedia, { new: true, runValidators: true, context: 'query' })
        .populate('genres', 'name')
        .populate('directors', 'name category')
        .populate('actors', 'name category')
        .populate('language', 'code')
        .populate('sources.language', 'code')
        .populate('classification', 'name');
    }

    async deleteMultimedia(ID: string): Promise<Multimedia> {
        const multimedia = await this.getMedia({ _id: ID, active: true });
        const update = { active: false, code: multimedia.name + '-disabled' };
        return await this.MultimediaModel.findOneAndUpdate({ _id: multimedia._id }, update, { new: true, runValidators: true, context: 'query' });
    }
}
