import { Controller, Put, UseFilters, UseGuards, Body, Param, Get, NotFoundException, Post,
    UseInterceptors, UploadedFile, Res, Query, BadRequestException, ConflictException, InternalServerErrorException, Logger } from '@nestjs/common';
import { MultimediaService } from './multimedia.service';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { ValidationErrorFilter } from '../../common/filters/validation-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { FileInterceptor } from '@nestjs/platform-express';
import { ValidFiledExtensionsInterceptor } from '../../common/interceptors/valid-filed-extensions.interceptor';
import { FilesService } from '../files/files.service';
import * as path from 'path';
import * as uuid from 'uuid';
import { imageSize } from 'image-size';
import { TranscoderService } from '../transcoder/transcoder.service';
import { TasksService } from '../tasks/tasks.service';
import { ActivesubGuard } from '../../common/guard/activesub.guard';
import { LanguagesService } from '../languages/languages.service';
import * as webvtt from 'node-webvtt';
import { SourcesService } from './sources.service';
import { Document } from 'mongoose';
import { Source } from './interfaces/source.interface';
import { UpdateCoverDTO } from './DTO/update-cover.dto';
import { Multimedia } from './interfaces/multimedia.interface';
import * as uniqid from 'uniqid';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { UpdateSourceDTO } from './DTO/update-source.dto';

@Controller('sources')
export class SourcesController {
    constructor(private multimediaAPI: MultimediaService, private filesAPI: FilesService, private langAPI: LanguagesService,
                private transcoderAPI: TranscoderService, private taskAPI: TasksService, private sourceAPI: SourcesService) {}

    @Get(':id')
    @UseGuards(AuthGuard, ActivesubGuard)
    async getSoure(@Param('id', new ValidateObjectIdPipe()) source): Promise<Source> {
        const media = await this.multimediaAPI.getMedia({ 'sources._id': source });
        if (!media) { throw new NotFoundException('Source not found', '8008'); }
        return (media.sources as unknown as Document).id(source);
    }

    @Put(':source')
    @UseFilters(MongoErrorFilter)
    @UseFilters(ValidationErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN']))
    async updateSource(@Param('source', new ValidateObjectIdPipe()) sourceID,
                       @Body(new PickPipe(['name', 'tagline', 'description', 'keywords'])) source: UpdateSourceDTO): Promise<Source> {
        const media = await this.multimediaAPI.getMedia({ 'sources._id': sourceID });
        if (!media) { throw new NotFoundException('Source not found', '8008'); }

        const sourceDB = (media.sources as unknown as Document).id(source);
        const update = {};
        for (const [key, value] of Object.entries(source)) {
            update[`sources.$.${ key }`] = value;
        }
        await this.multimediaAPI.updateMultimedia({ $set: update }, { 'sources._id': sourceID });
        return await sourceDB;
    }

    @Get(':movie/*')
    async getSourceStream(@Res() res, @Param() query) {
        const movieRoute = query['0'].split('/');
        let route = '';
        let file = '';
        if (movieRoute.length <= 1) {
            route = `movies/${ query.movie }/transcoded`;
            file = movieRoute.join();
        } else {
            file = movieRoute.pop();
            route = `movies/${ query.movie }/transcoded/${ movieRoute.join('/') }`;
        }
        const headers = await this.filesAPI.getHeadObject(route, file);
        const stream = await this.filesAPI.getFileStream(route, file);

        res.set('Content-Type', headers.ContentType);
        res.set('Content-Length', headers.ContentLength);
        res.set('Last-Modified', headers.LastModified);
        res.set('ETag', headers.ETag);

        stream.pipe(res);
    }

    @Post('uploadMedia/:id')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR', 'USER']))
    @UseInterceptors(FileInterceptor('file', { limits: { fileSize: 300000000 }}),
    new ValidFiledExtensionsInterceptor(['video/mp4']))
    async uploadMedia(@UploadedFile('file') file: Express.Multer.File, @Param('id') id, @Res() res, @Query() query) {
        const media = await this.multimediaAPI.getMedia({ _id: id });
        if (!media) { throw new NotFoundException('Media not found', '8001'); }
        if (!file) { throw new BadRequestException('Missing file', '7001'); }

        const isTrailer = query.hasOwnProperty('trailer') && query.trailer;
        const exist = await this.filesAPI.fileExist('movies/' + id + '/raw/', file.originalname, isTrailer);
        if (exist) { throw new ConflictException('There is a file with this name, rename the file you are trying to upload and try again', '7003'); }

        file.originalname = uuid.v4() + path.extname(file.originalname);
        const uploaded = await this.filesAPI.uploadFile(file, 'movies/' + id + '/raw/', isTrailer);
        if (!uploaded) { throw new InternalServerErrorException('Cant Upload', '7002'); }

        Logger.log(uploaded);
        return res.json({ key: uploaded.Key });
    }

    @Put('subtitles/:source')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    @UseInterceptors(FileInterceptor('file', { limits: { fileSize: 5000000 }}))
    async uploadSubtitles(@UploadedFile('file') file: Express.Multer.File, @Param('source') source) {
        const media = await this.multimediaAPI.getMedia({ 'sources._id': source });
        if (!media) { throw new NotFoundException('Source not found', '8008'); }
        if (!file) { throw new BadRequestException('Missing file', '7001'); }

        const sourceDB = (media.sources as unknown as Document).id(source);

        file.originalname = uuid.v4() + path.extname(file.originalname);

        // // Create and upload master list of subtitles
        const masterList = webvtt.hls.hlsSegmentPlaylist(file.buffer.toString(), 10);
        const masterFile = { buffer: Buffer.from(masterList), originalname: 'index.m3u8', mimetype: 'application/x-mpegURL' };
        const uri = `movies/${ media._id.toString() }/transcoded/subs/${ (sourceDB.language as any).code }/${ uniqid.process() }/`;
        const subURI = await this.filesAPI.uploadFile(masterFile as Express.Multer.File, uri);

        if (!subURI.hasOwnProperty('Key')) {
            throw new InternalServerErrorException('Cant upload master list', '7004');
        }

        // Create and upload segments of subtitles
        const segments = webvtt.hls.hlsSegment(file.buffer.toString(), 10, 0);
        for (let i = 0; i <= segments.length; i++) {
            if (segments[i]) {
                const segmentFile = { buffer: Buffer.from(segments[i].content), originalname: segments[i].filename, mimetype: 'text/vtt' };
                await this.filesAPI.uploadFile(segmentFile as Express.Multer.File, uri);
            }
        }

        // sourceDB.subtitle = `${ uri }index.m3u8`;
        await this.sourceAPI.rebuildMasterList(media);
        return await media.save();
    }

    @Put('audios/:source')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    @UseInterceptors(FileInterceptor('file', { limits: { fileSize: 5000000 }}),
    new ValidFiledExtensionsInterceptor(['audio/mpeg', 'audio/mp3']))
    async setAudio(@UploadedFile('file') file: Express.Multer.File, @Param('source') source) {
        const media = await this.multimediaAPI.getMedia({ 'sources._id': source });
        if (!media) { throw new NotFoundException('Source not found', '8008'); }
        if (!file) { throw new BadRequestException('Missing file', '7001'); }

        const sourceDB = (media.sources as unknown as Document).id(source);

        file.originalname = uuid.v4() + path.extname(file.originalname);
        const uploaded = await this.filesAPI.uploadFile(file, `movies/${media._id.toString()}/raw/audios/${sourceDB.language.code}/`);
        if (!uploaded) { throw new InternalServerErrorException('Cant Upload', '7002'); }

        const output = `movies/${ media._id.toString() }/transcoded/tracks/${ sourceDB.language.code }/${ uniqid.process() }/`;
        const jobID = await this.transcoderAPI.createAudioJob(uploaded.Key, output, file.originalname.split('.')[0]);

        sourceDB.jobID = jobID.Job.Id;
        sourceDB.audio = output + file.originalname.split('.')[0] + '.m3u8';

        const task = await this.taskAPI.createTask({ jobID: jobID.Job.Id, source: sourceDB._id, isMaster: false });
        await task.save();

        await this.sourceAPI.rebuildMasterList(media);
        return await media.save();
    }

    @Put('upload/:source/cover')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    @UseInterceptors(FileInterceptor('file', { limits: { fileSize: 5000000 }}),
    new ValidFiledExtensionsInterceptor(['image/jpeg', 'image/png']))
    async uploadCover(@UploadedFile('file') file: Express.Multer.File, @Param('source') source, @Query() query: UpdateCoverDTO): Promise<Multimedia> {
        const media = await this.multimediaAPI.getMedia({ 'sources._id': source });
        if (!media) { throw new NotFoundException('Source not found', '8008'); }
        if (!file) { throw new BadRequestException('Missing file', '7001'); }

        const sourceDB = (media.sources as unknown as Document).id(source);

        const dimensions = imageSize(file.buffer);
        if (query.type === 'horizontal') {
            if (dimensions.width !== 1200 || dimensions.height !== 800) { throw new BadRequestException('Image must be 1200x800', '8006'); }
        } else {
            if (dimensions.width !== 800 || dimensions.height !== 1200) { throw new BadRequestException('Image must be 800x1200', '8007'); }
        }

        file.originalname = uuid.v4() + path.extname(file.originalname);
        const uploaded = await this.filesAPI.uploadFile(file, `movies/${ media._id }/cover/${ sourceDB.language.code }/`, true);
        if (!uploaded) { throw new InternalServerErrorException('Cant Upload', '7002'); }

        query.type === 'horizontal' ? sourceDB.coverHorizontal = uploaded.Key : sourceDB.coverVertical = uploaded.Key;
        await media.save();
        return sourceDB;
    }
}
