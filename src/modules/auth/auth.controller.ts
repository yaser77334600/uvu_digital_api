import { Controller, Post, Body, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { LoginUserDTO } from './DTO/login-user.dto';
import { AuthService } from './auth.service';
import { SubscriptionsService } from '../subscriptions/subscriptions.service';

@Controller('auth')
export class AuthController {
    constructor(private userAPI: UsersService, private authAPI: AuthService, private subscriptionAPI: SubscriptionsService) {}

    @Post('login')
    async login(@Body(new PickPipe(['email', 'password'])) user: LoginUserDTO) {
        const userDB = await this.userAPI.getFullUser({ email: user.email, active: true });
        if (!userDB) { throw new UnauthorizedException('Invalid email or password', '0001'); }
        if (!this.authAPI.mathpassword(user.password, userDB.password)) {
            throw new UnauthorizedException('Invalid email or password', '0001');
        }

        if (!userDB.verifiedEmail) { throw new UnauthorizedException(`${ userDB._id.toString() }`, '0002'); }
        return { auth: this.authAPI.getToken(userDB) };
    }
}
