import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { GenreDTO } from './DTO/create-genre.dto';
import { Genre } from './interfaces/genre.interface';

@Injectable()
export class GenresService {
    constructor(@InjectModel('Genre') private GenreModel: Model<Genre>) {}

    async getGenres(query?): Promise<Genre[]> {
        const { limit, skip, options } = query;  
        return await this.GenreModel.find(options)
        .limit(Number(limit) > 100 ? 100 : Number(limit))
        .skip(Math.abs(Number(skip)) || 0)
        .populate({
            path: 'featuredMedia',
            select: 'name coverHorizontal coverVertical description',
            match: { status: 'published' },
        });
    }

    async getGenre(options) {
        return await this.GenreModel.findOne(options)
        .populate({
            path: 'featuredMedia',
            select: 'name coverHorizontal coverVertical description',
            match: { status: 'published' },
        });
    }

    async createGenre(gender: GenreDTO): Promise<Genre> {
        const genderDB = new this.GenreModel(gender);
        return await genderDB.save();
    }

    async updateGenre(gender: GenreDTO, query): Promise<Genre> {
        return await this.GenreModel.findOneAndUpdate(query, gender, { new: true, runValidators: true, context: 'query' })
        .populate({
            path: 'featuredMedia',
            select: 'name coverHorizontal coverVertical description',
            match: { status: 'published' },
        });
    }

    async deleteGenre(ID: string): Promise<Genre> {
        return await this.GenreModel.findOneAndDelete({ _id: ID });
    }
}
