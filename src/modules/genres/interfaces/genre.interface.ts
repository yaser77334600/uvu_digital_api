import { Document } from 'mongoose';
import { Pagination } from '../../../common/interfaces/pagination.interface';

export interface Genre extends Document, Pagination {
    name: string;
    active: boolean;
    coverHorizontal?: string;
    _id: string;
    featuredMedia: Genre[];
}
