import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GenresController } from './genres.controller';
import { GenreSchema } from './schemas/genres.schema';
import { GenresService } from './genres.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Genre', schema: GenreSchema }])],
  providers: [GenresService],
  controllers: [GenresController],
  exports: [GenresService],
})
export class GenresModule {}
