import { Controller, Get, Post, Put, Delete, Body, Param,
    Query, UseFilters, NotFoundException, ConflictException, UseGuards, Logger } from '@nestjs/common';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { MatchQueryPipe } from '../../common/pipes/match-query.pipe';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { Genre } from './interfaces/genre.interface';
import { GenreDTO } from './DTO/create-genre.dto';
import { UpdateGenreDTO } from './DTO/update-genre.dto';
import { GenresService } from './genres.service';
import { MultimediaService } from '../multimedia/multimedia.service';

@Controller('genres')
export class GenresController {
    constructor(private genreAPI: GenresService, private mediaAPI: MultimediaService) {}

    @Get()
    @UseGuards(AuthGuard)
    async getGenres(@Query(new MatchQueryPipe(['name'])) query): Promise<Genre[]> {
        const genresRaw = await this.genreAPI.getGenres(query); 
        const genres = [];
        for (const genre of genresRaw) {
            const count = await this.mediaAPI.countMedia({ options: { status: 'published', active: true, genres: { $in: [genre._id] } } }); 
            if (count > 0) { genres.push(genre); }
        }

        return genres;
    }

    @Get(':id')
    @UseGuards(AuthGuard)
    async getGenre(@Param('id', new ValidateObjectIdPipe()) genreID): Promise<Genre> {
        const genre = await this.genreAPI.getGenre({ _id: genreID, active: true });
        if (!genre) { throw new NotFoundException('Genre not found', '5001'); }
        return genre;
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async createGenre(@Body() genre: GenreDTO): Promise<Genre> {
        return await this.genreAPI.createGenre(genre);
    }

    @Put(':id')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async updateGenre(@Param('id', new ValidateObjectIdPipe()) genreID, @Body() genre: UpdateGenreDTO): Promise<Genre> {
        const genreDB = await this.genreAPI.getGenre({ _id: genreID, active: true });
        if (!genreDB) { throw new NotFoundException('Genre not found', '5001'); }

        if (genre.name && genre.name !== '') {
            const exist = await this.genreAPI.getGenre({ name: genre.name.trim() });
            if (exist) { throw new ConflictException('This genre already exist', '5001'); }
            genreDB.name = genre.name.trim();
        }

        if (genre.featuredMedia && genre.featuredMedia.length > 0) {
            const uniqueIDs = new Set(genre.featuredMedia);
            const IDsDB = genreDB.featuredMedia.map(media => media._id.toString());
            for (const uniqueID of uniqueIDs.values()) {
                if (IDsDB.indexOf(uniqueID) === -1) {
                    genreDB.featuredMedia.push(uniqueID as any);
                }
            }
        }

        if (genre.removeFromFeatured && genre.removeFromFeatured.length > 0) {
            const uniqueIDs = new Set(genre.removeFromFeatured);
            const IDsDB = genreDB.featuredMedia.map(media => media._id.toString());
            for (const uniqueID of uniqueIDs.values()) {
                const pos = IDsDB.indexOf(uniqueID);
                if (pos !== -1) { genreDB.featuredMedia.splice(pos, 1); }
            }
        }
        return await genreDB.save();
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN']))
    async deleteGenre(@Param('id', new ValidateObjectIdPipe()) genreID): Promise<Genre> {
        return await this.genreAPI.deleteGenre(genreID);
    }
}
