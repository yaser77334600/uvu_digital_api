import { Length, IsString, IsOptional, IsMongoId, IsNotEmpty } from 'class-validator';

export class UpdateGenreDTO {
    @IsOptional()
    @IsNotEmpty()
    @Length(1, 20)
    @IsString()
    name: string;

    @IsOptional()
    @IsMongoId({ each: true })
    featuredMedia: string[];

    @IsOptional()
    @IsMongoId({ each: true })
    removeFromFeatured: string[];
}
