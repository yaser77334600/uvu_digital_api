import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { S3 } from 'aws-sdk';
import { PutObjectRequest } from 'aws-sdk/clients/s3';

@Injectable()
export class FilesService {
    constructor() {
        AWS.config.update({
            secretAccessKey: process.env.AWS_SC_KEY,
            accessKeyId: process.env.AWS_KEY,
            region: process.env.AWS_REGION,
        });
        this.S3 = new AWS.S3();
    }

    private S3: S3;

    async uploadFile(file: Express.Multer.File, folder: string, publicResource = false): Promise<S3.ManagedUpload.SendData> {
        if (!file) { return null; }
        const params: PutObjectRequest  = {
            Bucket: process.env.BUCKET,
            Key: folder + file.originalname,
            Body: file.buffer,
            ContentType: file.mimetype,
        };
        if (publicResource) { params.Bucket = process.env.PUBLIC_BUCKET; }
        return await this.S3.upload(params).promise();
    }

    async fileExist(folder, file, publicResource = false): Promise<boolean> {
        folder = folder.charAt(folder.length - 1 ) === '/' ?  folder.substr(0, folder.length - 1) : folder;
        const fileList = await this.listFiles(folder, publicResource);
        if (fileList.KeyCount > 0) {
            let exist = false;
            fileList.Contents.forEach(fileS3 => fileS3.Key === `${folder}/${file}` ? exist = true : '');
            return exist;
        } else {
            return false;
        }
    }

    async listFiles(folder, publicResource = false): Promise<S3.ListObjectsV2Output> {
        const params: S3.ListObjectsV2Request =  {
            Bucket: process.env.BUCKET,
            Prefix: folder,
        };
        if (publicResource) { params.Bucket = process.env.PUBLIC_BUCKET; }
        return await this.S3.listObjectsV2(params).promise();
    }

    async deleteFile(folder, file, publicResource = false) {
        const params: S3.DeleteObjectRequest = {
            Bucket: process.env.BUCKET,
            Key: `${ folder }/${ file }`,
        };
        if (publicResource) { params.Bucket = process.env.PUBLIC_BUCKET; }
        return await this.S3.deleteObject(params).promise();
    }

    async getFile(folder, file, publicResource = false) {
        const params: S3.DeleteObjectRequest = {
            Bucket: process.env.BUCKET,
            Key: `${ folder }/${ file }`,
        };
        if (publicResource) { params.Bucket = process.env.PUBLIC_BUCKET; }
        return await this.S3.getObject(params).promise();
    }

    getFileStream(folder, file) {
        const params: S3.DeleteObjectRequest = {
            Bucket: process.env.BUCKET,
            Key: `${ folder }/${ file }`,
        };
        return this.S3.getObject(params).createReadStream();
    }

    async getHeadObject(folder, file) {
        const params: S3.DeleteObjectRequest = {
            Bucket: process.env.BUCKET,
            Key: `${ folder }/${ file }`,
        };
        return this.S3.headObject(params).promise();
    }
}
