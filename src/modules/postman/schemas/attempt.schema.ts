import { Schema } from 'mongoose';

export const attemptSchema = new Schema({
    triedAt: {
        type: Date,
        required: true,
    },
    succes: {
        type: Boolean,
        required: true,
    },
    reason: {
        type: String,
        required: true,
    },
    triedBy: {
        type: Schema.Types.ObjectId,
        ref: 'Sender',
        required: true,
    },
});
