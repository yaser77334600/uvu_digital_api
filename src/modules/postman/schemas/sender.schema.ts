import { Schema } from 'mongoose';

export const senderSchema = new Schema({
    address: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
        select: false,
    },
    lastUsed: {
        type: Date,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    relays: {
        type: Number,
        default: 0,
        max: 250,
    },
    insertBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
});
