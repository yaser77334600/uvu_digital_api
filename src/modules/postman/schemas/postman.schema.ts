import { Schema } from 'mongoose';
import { attemptSchema } from './attempt.schema';

export const PostmanSchema = new Schema({
    body: {
        type: String,
        required: true,
    },
    from: {
        type: String,
        default: 'no-reply@uvu.digital',
    },
    to: [String],
    attachments: [String],
    sended: {
        type: Boolean,
        default: false,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    insertBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: false,
    },
    subject: {
        type: String,
        trim: true,
        maxlength: 260,
    },
    sendedTo: [String],
    attempts: [attemptSchema],
});
