import { IsNotEmpty, IsString, MaxLength, IsEmail, MinLength, IsEnum } from 'class-validator';

export class CreateSenderDTO {
    @IsNotEmpty()
    @IsString()
    @MaxLength(120)
    address: string;

    @IsNotEmpty()
    @IsString()
    password: string;
}
