import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Postman } from './interfaces/postman.interface';

@Injectable()
export class PostmanService {
    constructor(@InjectModel('Postman') private PostmanModel: Model<Postman>) {}

    async createMail(mail: Postman | any) {
        const mailDB = new this.PostmanModel(mail);
        return await mailDB.save();
    }

    async getMails(query?): Promise<Postman[]> {
        const { limit, skip, options } = query;
        return await this.PostmanModel.find(options)
        .limit(Number(limit) > 100 ? 100 : Number(limit))
        .skip(Math.abs(Number(skip)) || 0)
        .populate('insertBy', 'name role')
        .sort({ createdOn: 'desc' });
    }

    async getMail(options: object): Promise<Postman> {
        return await this.PostmanModel.findOne(options)
        .populate('insertBy', 'email name');
    }

    async countMails(query) {
        const { options } = query;
        return await this.PostmanModel.countDocuments(options);
    }
}
