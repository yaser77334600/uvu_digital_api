import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Sender } from './interfaces/sender.interface';
import { CreateSenderDTO } from './DTO/create-sender.dto';

@Injectable()
export class SenderService {
    constructor(@InjectModel('Sender') private SenderModel: Model<Sender>) {}

    async createSender(sender: Sender | CreateSenderDTO) {
        const senderDB = new this.SenderModel(sender);
        return await senderDB.save();
    }

    async getSenders(options?): Promise<Sender[]> {
        return await this.SenderModel.find(options)
        .sort({ createdOn: 'desc' });
    }

    async getSender(options): Promise<Sender> {
        return await this.SenderModel.findOne(options);
    }

    async getFullSender(options): Promise<Sender> {
        return await this.SenderModel.findOne(options).select('+password');
    }

    async getFullSenders(options?): Promise<Sender[]> {
        return await this.SenderModel.find(options).select('+password');
    }

    async deleteSender(options): Promise<Sender> {
        return await this.SenderModel.findOneAndRemove(options);
    }
}
