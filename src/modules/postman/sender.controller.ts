import { Controller, Get, UseFilters, UseGuards, Delete, Param, Post, Body, Req } from '@nestjs/common';
import { SenderService } from './sender.service';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { CastErrorFilter } from '../../common/filters/cast-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { Sender } from './interfaces/sender.interface';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { CreateSenderDTO } from './DTO/create-sender.dto';

@Controller('sender')
export class SenderController {
    constructor(private senderAPI: SenderService) { }

    @Get()
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN']))
    async getSenders(): Promise<Sender[]> {
        return await this.senderAPI.getSenders();
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN']))
    async deleteSender(@Param('id', new ValidateObjectIdPipe()) id: string): Promise<Sender> {
        return await this.senderAPI.deleteSender({ _id: id });
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN']))
    async createSender(@Body(new PickPipe(['address', 'password'])) sender: CreateSenderDTO,
                       @Req() req): Promise<Sender> {
        const tokenUser = req.token.user as any;
        Object.assign(sender, { insertBy: tokenUser._id });
        return await this.senderAPI.createSender(sender);
    }
}
