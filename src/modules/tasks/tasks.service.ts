import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Task } from './interfaces/task.interface';
import { Interval, NestSchedule, Timeout } from 'nest-schedule';
import { TranscoderService } from '../transcoder/transcoder.service';
import { MultimediaService } from '../multimedia/multimedia.service';
import { SourcesService } from '../multimedia/sources.service';
import { FilesService } from '../files/files.service';

@Injectable()
export class TasksService extends NestSchedule {
    constructor(@InjectModel('Task') private TaskModel: Model<Task>, private transcoderAPI: TranscoderService,
                private multimediaAPI: MultimediaService, private sourceAPI: SourcesService, private filesAPI: FilesService) {
        super();
    }

    async createTask(task): Promise<Task> {
        const taskDB = new this.TaskModel(task);
        return await taskDB.save();
    }

    async getTasks(query) {
        return await this.TaskModel.find(query);
    }

    @Interval(30000, { waiting: true })
    async getList() {
        this.search();
    }

    async search() {
        const task = await this.TaskModel.findOne({ completed: false });
        if (!task) { return true; }
        if (!task.isUploaded && task.isMaster) {
            const multimedia = await this.multimediaAPI.getMedia({ _id: task.source });
            const tmpMasterFile = multimedia.file.split('/');
            const masterName = tmpMasterFile.pop();
            const masterExist = await this.filesAPI.fileExist(tmpMasterFile.join('/'), masterName, false);
            if (masterExist) {
                task.isUploaded = true;
                const output = 'movies/' + multimedia._id.toString() + '/transcoded/';
                const jobID = await this.transcoderAPI.createJob(multimedia.file, output, masterName.split('.')[0]);
                task.jobID = jobID.Job.Id;
                await task.save();
                Logger.log('Tarea actualizada');
            } else {
                return true;
            }
        } else {
            const status = await this.transcoderAPI.getJobStatus(task.jobID);
            if (!status) { return true; }
            if (status.Job.Status === 'Complete') {
                const transcoded = status.Job.OutputKeyPrefix;
                const query = task.isMaster ? { _id: task.source } : { 'sources._id': task.source };
                const multimedia = await this.multimediaAPI.getMedia(query);

                if (!multimedia) { return true; }
                if (!task.isMaster) {
                    const source = multimedia.sources.find(s => s.jobID === task.jobID);
                    if (!source) { return true; }
                    source.audioTranscoded = `${ transcoded }${ status.Job.Output.Key }.m3u8`;
                } else {
                    multimedia.transcoded = `${ transcoded }index.m3u8`;
                }
                task.completed = true;
                task.transcoded = transcoded;
                multimedia.transcoding = false;

                // Save task
                await task.save();
                await multimedia.save();
                this.sourceAPI.rebuildMasterList(multimedia);

                Logger.log(`${ multimedia.name }, has been successfully transcoded`);
            }
            return true;
        }
    }
}
