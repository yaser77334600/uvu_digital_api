import { Schema } from 'mongoose';

export const TaskSchema = new Schema({
    jobID: {
        type: String,
    },
    source: {
        type: Schema.Types.ObjectId,
        ref: 'Multimedia',
        required: true,
    },
    transcoded: {
        type: String,
    },
    completed: {
        type: Boolean,
        default: false,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    isMaster: {
        type: Boolean,
        default: false,
    },
    isUploaded: {
        type: Boolean,
        default: false,
    },
});
