import { Module, Global } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { MongooseModule } from '@nestjs/mongoose';
import { TaskSchema } from './schemas/task.schema';
import { ScheduleModule } from 'nest-schedule';
import { TranscoderModule } from '../transcoder/transcoder.module';
import { FilesModule } from '../files/files.module';

@Global()
@Module({
  providers: [TasksService],
  imports: [ScheduleModule.register(), MongooseModule.forFeature([{ name: 'Task', schema: TaskSchema }]), TranscoderModule,
  FilesModule],
  exports: [TasksService],
})
export class TasksModule {
}
