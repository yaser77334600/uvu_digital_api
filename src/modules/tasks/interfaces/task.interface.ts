import { Document } from 'mongoose';

export interface Task extends Document {
    jobID: string;
    source: string;
    transcoded?: string;
    completed?: boolean;
    createdOn?: string;
    isMaster: boolean;
    isUploaded: boolean;
}
