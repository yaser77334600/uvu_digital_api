import { Schema } from 'mongoose';

export const ClassificationSchema = new Schema({
    name: {
        type: String,
        minlength: 1,
        maxlength: 29,
        required: true,
        unique: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
});
