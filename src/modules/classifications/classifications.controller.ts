import { Controller, Get, Post, Put, Delete, Body, Param, Query, UseFilters, NotFoundException, ConflictException, UseGuards } from '@nestjs/common';
import { CreateClassificationDTO } from './DTO/create-classification.dto';
import { ClassificationsService } from './classifications.service';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { Classification } from './interfaces/classification.interface';
import { MatchQueryPipe } from '../../common/pipes/match-query.pipe';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';

@Controller('classifications')
export class ClassificationsController {
    constructor(private classificationAPI: ClassificationsService) {}

    @Get()
    @UseGuards(AuthGuard)
    async getClassifications(@Query(new MatchQueryPipe([''])) query): Promise<Classification[]> {
        return await this.classificationAPI.getClassifications(query);
    }

    @Get(':id')
    @UseGuards(AuthGuard)
    async getClassification(@Param('id', new ValidateObjectIdPipe()) classificationID): Promise<Classification> {
        const classification = await this.classificationAPI.getClassification({ _id: classificationID, active: true});
        if (!classification) { throw new NotFoundException('Classification not found', '3001'); }
        return classification;
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async createClassification(@Body() classification: CreateClassificationDTO): Promise<Classification> {
        return await this.classificationAPI.createClassification(classification);
    }

    @Put(':id')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async updateClassification(@Param('id', new ValidateObjectIdPipe()) classificationID,
                               @Body() classification: CreateClassificationDTO): Promise<Classification> {
        const exist = await this.classificationAPI.getClassification({ name: classification.name, active: true});
        if (exist) { throw new ConflictException('This classification already exist', '3002'); }
        return await this.classificationAPI.updateClassification(classification, classificationID);
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async deleteClassification(@Param('id', new ValidateObjectIdPipe()) classificationID): Promise<Classification> {
        return await this.classificationAPI.deleteClassification(classificationID);
    }
}
