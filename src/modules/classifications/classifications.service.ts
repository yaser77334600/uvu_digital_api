import { Injectable, NotFoundException } from '@nestjs/common';
import { Classification } from './interfaces/classification.interface';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateClassificationDTO } from './DTO/create-classification.dto';

@Injectable()
export class ClassificationsService {
    constructor(@InjectModel('Classification') private ClassificationModel: Model<Classification>) {}

    async getClassifications(query?) {
        const { limit, skip, options } = query;
        return await this.ClassificationModel.find(options);
    }

    async getClassification(options) {
        return await this.ClassificationModel.findOne(options);
    }

    async createClassification(classification: CreateClassificationDTO): Promise<Classification> {
        const classificationDB = await new this.ClassificationModel(classification);
        return await classificationDB.save();
    }

    async updateClassification(classification: CreateClassificationDTO, ID): Promise<Classification> {
        return await this.ClassificationModel.findOneAndUpdate({ _id: ID }, classification, { new: true, runValidators: true, context: 'query' });
    }

    async deleteClassification(ID: string): Promise<Classification> {
        return await this.ClassificationModel.findOneAndDelete({ _id: ID });
    }
}
