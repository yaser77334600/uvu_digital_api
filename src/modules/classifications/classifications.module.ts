import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { ClassificationsController } from './classifications.controller';
import { AuthMiddleware } from '../../middlewares/auth.middleware';
import { ClassificationsService } from './classifications.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ClassificationSchema } from './schemas/classifications.schema';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'Classification', schema: ClassificationSchema }])],
    controllers: [ClassificationsController],
    providers: [ClassificationsService],
})
export class ClassificationsModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(AuthMiddleware).forRoutes('classifications');
    }
}
