import { IsNotEmpty, Length, IsString } from 'class-validator';

export class CreateClassificationDTO {
    @IsNotEmpty()
    @Length(1, 20)
    @IsString()
    name: string;
}
