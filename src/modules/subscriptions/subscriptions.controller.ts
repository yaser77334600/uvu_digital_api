import { Controller, Get, NotFoundException, Post, Body, UseGuards, Param,
    Res, Req, BadRequestException, InternalServerErrorException, Logger } from '@nestjs/common';
import { SubscriptionsService } from './subscriptions.service';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { UsersService } from '../users/users.service';
import * as Stripe from 'stripe';
import { UpdatePaymentDTO } from './DTO/update-payment.dto';
import { PickPipe } from '../../common/pipes/pick.pipe';

@Controller('subscriptions')
export class SubscriptionsController {
    stripe = new Stripe(process.env.STRIPE_API);
    constructor(private billinAPI: SubscriptionsService, private userAPI: UsersService) {}

    @Get(':id')
    @UseGuards(AuthGuard)
    async getSubscription(@Param('id') ID, @Req() req): Promise <any> {
        const userToken = req.token.user;
        if (userToken.role !== 'ADMIN') {
            if (userToken.hasOwnProperty('customer')) {
                ID = userToken.customer;
            } else {
                const user = await this.userAPI.getUser({ _id: userToken._id });
                ID = user.customer ? user.customer : '';
            }
        }
        return await this.billinAPI.getCustomer(ID);
    }

    @Post()
    @UseGuards(AuthGuard)
    async createSubscription(@Req() req, @Res() res, @Body(new PickPipe(['paymentMethod'])) auth: UpdatePaymentDTO) {
        let StripeError;
        const userToken = req.token.user;
        const user = await this.userAPI.getUser({ _id: userToken._id });
        if (!user) { throw new NotFoundException('User not found', '1001'); }

        let details;
        let customer;
        if (!user.customer) {
            details = {
                email: user.email, name: user.name, description: 'UVU Basic', payment_method: auth.paymentMethod,
                invoice_settings: { default_payment_method: auth.paymentMethod },
            };
            customer = await this.stripe.customers.create(details).catch(error => StripeError = error);
            if (customer) {
                user.customer = customer.id;
                await user.save();
            }
        } else {
            const existCustomer = await this.billinAPI.getFullCustomer(user.customer);
            if (existCustomer && existCustomer.deleted) {
                details = {
                    email: user.email, name: user.name, description: 'UVU Basic', payment_method: auth.paymentMethod,
                    invoice_settings: { default_payment_method: auth.paymentMethod },
                };
                customer = await this.stripe.customers.create(details).catch(error => StripeError = error);
                if (customer) {
                    user.customer = customer.id;
                    await user.save();
                }
            } else {
                const paymentMethod = await this.stripe.paymentMethods.attach(auth.paymentMethod, { customer: user.customer })
                .catch(error => StripeError = error);
                details = { invoice_settings: { default_payment_method: paymentMethod.id } };
                customer = await this.stripe.customers.update(user.customer, details).catch(error => StripeError = error);
            }
        }

        if (StripeError) {
            Logger.log(StripeError);
            if (StripeError.hasOwnProperty('raw') && StripeError.raw.param === 'payment_method') {
                throw new BadRequestException('Invalid payment method', 'S001');
            } else if (StripeError.hasOwnProperty('raw') && StripeError.raw.param === 'customer') {
                throw new NotFoundException('Customer not found', 'S002');
            } else {
                throw new InternalServerErrorException('An error has occurred', '0000');
            }
        }

        if (customer.subscriptions.data.length > 0) {
            res.json({ ok: true });
        } else {
            await this.stripe.subscriptions.create({ customer: customer.id, items: [{ plan: 'plan_G9j4AyTXU1d9ml' }]})
            .catch(e => StripeError = e);
            if (StripeError) {
                throw new InternalServerErrorException('An error has occurred', '0000');
            }
            res.json({ ok: true });
        }
    }
}
