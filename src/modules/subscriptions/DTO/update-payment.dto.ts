import { IsNotEmpty, IsOptional, MaxLength, IsArray, IsString, MinLength } from 'class-validator';

export class UpdatePaymentDTO {
    @IsNotEmpty()
    @IsString()
    @MaxLength(60)
    paymentMethod: string;
}
