import { Injectable, Logger } from '@nestjs/common';
import * as stripe from 'stripe';
import { User } from '../users/interfaces/user.interface';

@Injectable()
export class SubscriptionsService {
    private stripeAPI = new stripe(process.env.STRIPE_API);

    async getFullCustomer(userID): Promise<any> {
        let error;
        const customer = await this.stripeAPI.customers.retrieve(userID).catch((e) => error = null);
        return error === null ? error : customer;
    }

    async getCustomer(userID): Promise<any> {
        const subscription = { current_period_end: '', last4: '' };
        let error;
        const customer = await this.stripeAPI.customers.retrieve(userID).catch((e) => error = null);
        if (!error && customer && customer.subscriptions && customer.subscriptions.data && customer.subscriptions.data.length > 0) {
            subscription.current_period_end = customer.subscriptions.data[0].current_period_end;
            // Get Payment Method
            if (customer.invoice_settings &&
                customer.invoice_settings.default_payment_method &&
                customer.invoice_settings.default_payment_method !== '') {
                    const PM = await this.getPaymentDetails(customer.invoice_settings.default_payment_method).catch(e => error = null);
                    if (!error && PM.card) {  subscription.last4 = PM.card.last4; }
            }
        }
        return subscription;
    }

    async deleteSub(customerID) {
        const existCustomer = await this.getFullCustomer(customerID);
        if (existCustomer && !existCustomer.deleted) {
            if (existCustomer.subscriptions && existCustomer.subscriptions.data && existCustomer.subscriptions.data.length > 0) {
                const subID = existCustomer.subscriptions.data[0].id;
                await this.stripeAPI.subscriptions.del(subID).catch(e => Logger.log(e));
            }
        }
    }

    async createCustomer(user: User, token) {
        await this.stripeAPI.customers.create({ email: user.email, source: token });
    }

    async createSubscription(customer: string) {
        return await this.stripeAPI.subscriptions.create({ customer, items: [{ plan: 'prod_F2e6n0OzpfBXJk' }] });
    }

    async getSources(customer) {
        let error;
        const cards = await this.stripeAPI.customers.listSources(customer, { } as any).catch((e) => error = null);
        return error === null ? error : cards;
    }

    async getPaymentDetails(id) {
        let error;
        const paymentMethods = await this.stripeAPI.paymentMethods.retrieve(id).catch((e) => error = null);
        return error === null ? error : paymentMethods;
    }
}
