import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Person } from './interfaces/person.interface';
import { CreatePersonDTO } from './DTO/create-person.dto';
import { UpdatePersonDTO } from './DTO/update-person.dto';

@Injectable()
export class PersonsService {
    constructor(@InjectModel('Person') private PersonModel: Model<Person>) {}

    async getPeople(query?) {
        const { limit, skip, options } = query;
        return await this.PersonModel.find(options)
        .limit(Number(limit) > 100 ? 100 : Number(limit))
        .skip(Math.abs(Number(skip)) || 0);
    }

    async countPeople(query?) {
        const { options } = query;
        return await this.PersonModel.countDocuments(options);
    }

    async getPerson(options) {
        return await this.PersonModel.findOne(options);
    }

    async createPerson(person: CreatePersonDTO): Promise<Person> {
        const personDB = await new this.PersonModel(person);
        return await personDB.save();
    }

    async updatePerson(person: UpdatePersonDTO | {}, query): Promise<Person> {
        return await this.PersonModel.findOneAndUpdate(query, person, { new: true, runValidators: true, context: 'query' });
    }

    async deletePerson(ID: string): Promise<Person> {
        const person = await this.getPerson({ _id: ID, active: true});
        const update = { active: false, name: person.name + '-disabled' };
        return await this.PersonModel.findOneAndUpdate({ _id: person._id }, update, { new: true, runValidators: true, context: 'query' });
    }
}
