import { IsNotEmpty, Length, IsOptional, IsDateString, MaxLength, IsIn } from 'class-validator';

export class UpdatePersonDTO {
    @IsOptional()
    @IsNotEmpty()
    @Length(1, 60)
    name: string;

    @IsOptional()
    @IsNotEmpty()
    @IsIn(['male', 'female'])
    gender: string;

    @IsOptional()
    @IsNotEmpty()
    @IsDateString()
    birthdate: Date;

    @IsOptional()
    @IsOptional()
    @MaxLength(2000)
    biography: string;

    @IsOptional()
    @IsNotEmpty()
    @IsIn(['actor', 'director'])
    category: string;
}
