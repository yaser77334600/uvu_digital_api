import { IsNotEmpty, Length, IsOptional, IsDateString, MaxLength, IsIn } from 'class-validator';

export class CreatePersonDTO {
    @IsNotEmpty()
    @Length(1, 60)
    name: string;

    @IsNotEmpty()
    @IsIn(['male', 'female'])
    gender: string;

    @IsNotEmpty()
    @IsDateString()
    birthdate: Date;

    @IsOptional()
    @MaxLength(2000)
    biography: string;

    @IsNotEmpty()
    @IsIn(['actor', 'director'])
    category: string;
}
