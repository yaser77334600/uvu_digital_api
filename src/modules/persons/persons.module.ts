import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PersonsService } from './persons.service';
import { PersonsController } from './persons.controller';
import { PersonSchema } from './schemas/persons.schema';
import { FilesModule } from '../files/files.module';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'Person', schema: PersonSchema }]), FilesModule],
    providers: [PersonsService],
    controllers: [PersonsController],
  })
export class PersonsModule {}
