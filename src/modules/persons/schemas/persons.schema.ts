import { Schema } from 'mongoose';

export const PersonSchema = new Schema({
    name: {
        type: String,
        maxlength: 60,
        trim: true,
        required: true,
        unique: true,
    },
    gender: {
        type: String,
        enum: {
            values: ['male', 'female'],
        },
        default: 'male',
    },
    birthdate: {
        type: Date,
        required: true,
    },
    biography: {
        type: String,
        maxlength: 2000,
        trim: true,
    },
    category: {
        type: String,
        enum: {
            values: ['actor', 'director'],
            message: 'Invalid type',
        },
        default: 'actor',
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    profileImg: {
        type: String,
    },
    uuid: {
        type: String,
        required: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
});
