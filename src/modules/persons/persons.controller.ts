import { Controller, Get, Post, Put, Param, Body, Query, Delete, UseFilters, UseInterceptors,
         UploadedFile, BadRequestException, NotFoundException, Res, UseGuards } from '@nestjs/common';
import { FileInterceptor  } from '@nestjs/platform-express';
import { PersonsService } from './persons.service';
import { MatchQueryPipe } from '../../common/pipes/match-query.pipe';
import { Person } from './interfaces/person.interface';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { CreatePersonDTO } from './DTO/create-person.dto';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { UuidPipe } from '../../common/pipes/uuid.pipe';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { UpdatePersonDTO } from './DTO/update-person.dto';
import { FilesService } from '../files/files.service';
import { ValidFiledExtensionsInterceptor } from '../../common/interceptors/valid-filed-extensions.interceptor';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import * as path from 'path';
import * as uuid from 'uuid';

@Controller('persons')
export class PersonsController {
    constructor(private peopleAPI: PersonsService, private filesService: FilesService) {}

    @Get()
    @UseGuards(AuthGuard)
    async getPeople(@Query(new MatchQueryPipe(['category', 'name'])) query): Promise<{ people: Person[], total: number }> {
        const people = await this.peopleAPI.getPeople(query);
        const total = await this.peopleAPI.countPeople(query);
        return { people, total };
    }

    @Get('byname/:name')
    @UseGuards(AuthGuard)
    async getPeopleByName(@Param('name') name): Promise<Person> {
        return await this.peopleAPI.getPerson({ name });
    }

    @Get(':id')
    @UseGuards(AuthGuard)
    async getPerson(@Param('id', new ValidateObjectIdPipe()) personID): Promise<Person> {
        const person = await this.peopleAPI.getPerson({ _id: personID, active: true });
        if (!person) { throw new NotFoundException('Person not found', '9001'); }
        return person;
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async createPerson(@Body(new UuidPipe(),
                       new PickPipe(['name', 'genre', 'birthdate', 'biography', 'category', 'uuid'])) person: CreatePersonDTO): Promise<Person> {
        return await this.peopleAPI.createPerson(person);
    }

    @Put(':id')
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async updatePerson(@Param('id', new ValidateObjectIdPipe()) personID,
                       @Body(new PickPipe(['name', 'genre', 'birthdate', 'category', 'biography'])) person: UpdatePersonDTO): Promise<Person> {
        return await this.peopleAPI.updatePerson(person, personID);
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async deletePerson(@Param('id', new ValidateObjectIdPipe()) personID): Promise<Person> {
        return await this.peopleAPI.deletePerson(personID);
    }

    @Put(':id/upload')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    @UseInterceptors(FileInterceptor('file', { limits: { fileSize: 5000000 }}),
    new ValidFiledExtensionsInterceptor(['image/jpeg', 'image/png']))
    async uploadFile(@UploadedFile('file') file: Express.Multer.File, @Param('id', new ValidateObjectIdPipe()) id, @Res() res) {
        const person = await this.getPerson(id);
        if (!person) { throw new NotFoundException('Person not found', '9001'); }

        file.originalname = uuid.v4() + path.extname(file.originalname);
        const uploaded = await this.filesService.uploadFile(file, `people/${ person._id }/`, true);
        if (!uploaded) { throw new BadRequestException('Can\t upload', '7002'); }
        const update = await this.peopleAPI.updatePerson(({ profileImg: uploaded.Key }), { _id: person._id });
        let existFile = false;
        let dir;
        if (person.profileImg) {
            dir = person.profileImg.split('/');
            existFile = await this.filesService.fileExist(dir.slice(0, 2).join('/'), dir[dir.length - 1], true);
        }
        if (existFile) {
            await this.filesService.deleteFile(dir.slice(0, 2).join('/'), dir[dir.length - 1], true);
            return res.json(update);
        } else {
            return res.json(update);
        }

    }
}
