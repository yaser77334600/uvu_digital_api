import { Document } from 'mongoose';
import { Pagination } from '../../../common/interfaces/pagination.interface';

export interface Person extends Document, Pagination {
    name: string;
    gender: string;
    birthdate: Date;
    biography: string;
    category: string;
    createdOn: Date;
    profileImg?: string;
    uuid: string;
    active: boolean;
    _id: string;
}
