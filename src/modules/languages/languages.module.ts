import { Module } from '@nestjs/common';
import { LanguagesService } from './languages.service';
import { MongooseModule } from '@nestjs/mongoose';
import { LanguageSchema } from './schemas/languages.schema';
import { LanguagesController } from './languages.controller';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Language', schema: LanguageSchema }])],
  providers: [LanguagesService],
  controllers: [LanguagesController],
  exports: [LanguagesService],
})
export class LanguagesModule {}
