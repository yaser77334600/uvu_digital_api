import { IsNotEmpty, Length, IsAlpha, IsUppercase } from 'class-validator';

export class LanguageDTO {
    @IsNotEmpty()
    @Length(3)
    @IsAlpha()
    @IsUppercase()
    code: string;
}
