import { Schema } from 'mongoose';

export const LanguageSchema = new Schema({
    code: {
        type: String,
        maxlength: 3,
        minlength: 3,
        required: true,
        unique: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
});
