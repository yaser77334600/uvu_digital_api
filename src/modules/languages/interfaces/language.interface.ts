import { Document } from 'mongoose';
import { Pagination } from '../../../common/interfaces/pagination.interface';

export interface Language extends Document, Pagination {
    code: string;
    active: boolean;
    _id: string;
}
