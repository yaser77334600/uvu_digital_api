import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Language } from './interfaces/language.interface';
import { LanguageDTO } from './DTO/language.dto';

@Injectable()
export class LanguagesService {
    constructor(@InjectModel('Language') private LanguageModel: Model<Language>) {}

    async getLanguages(query?) {
        const { limit, skip, options } = query;
        return await this.LanguageModel.find(options);
    }

    async getLanguage(options): Promise<Language> {
        return await this.LanguageModel.findOne(options);
    }

    async createLanguage(language: LanguageDTO): Promise<Language> {
        const languageDB = await new this.LanguageModel(language);
        return await languageDB.save();
    }

    async updateLanguage(language: LanguageDTO, query): Promise<Language> {
        return await this.LanguageModel.findOneAndUpdate(query, language, { new: true, runValidators: true, context: 'query' });
    }

    async deleteLanguage(ID: string): Promise<Language> {
        return await this.LanguageModel.findOneAndDelete({ _id: ID });
    }
}
