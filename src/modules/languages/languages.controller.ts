import { Controller, Get, Post, Put, Delete, Body, Param, Query, UseFilters, NotFoundException, ConflictException, UseGuards } from '@nestjs/common';
import { LanguagesService } from './languages.service';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { Language } from './interfaces/language.interface';
import { LanguageDTO } from './DTO/language.dto';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';

@Controller('languages')
export class LanguagesController {
    constructor(private languagesAPI: LanguagesService) {}

    @Get()
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async getLanguages(@Query() query): Promise<Language[]> {
        return await this.languagesAPI.getLanguages(query);
    }

    @Get(':id')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async getLanguage(@Param('id', new ValidateObjectIdPipe()) languageID): Promise<Language> {
        const language = await this.languagesAPI.getLanguage({ _id: languageID, active: true});
        if (!language) { throw new NotFoundException('Language not found', '6001'); }
        return language;
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async createLanguage(@Body() language: LanguageDTO): Promise<Language> {
        console.log(language);
        return await this.languagesAPI.createLanguage(language);
    }

    @Put(':id')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async updateLanguage(@Param('id', new ValidateObjectIdPipe()) languageID, @Body() language: LanguageDTO): Promise<Language> {
        const exist = await this.languagesAPI.getLanguage({ code: language.code, active: true});
        if (exist) { throw new ConflictException('This language already exist', '6002'); }
        return await this.languagesAPI.updateLanguage(language, { _id: languageID });
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['ADMIN']))
    async deleteLanguage(@Param('id', new ValidateObjectIdPipe()) languageID): Promise<Language> {
        return await this.languagesAPI.deleteLanguage(languageID);
    }
}
