import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { ElasticTranscoder } from 'aws-sdk';

@Injectable()
export class TranscoderService {
    constructor() {
        AWS.config.update({
            secretAccessKey: process.env.AWS_SC_KEY,
            accessKeyId: process.env.AWS_KEY,
            region: process.env.AWS_REGION,
        });
        this.transcoder = new AWS.ElasticTranscoder();
    }
    private transcoder: ElasticTranscoder;

    async createJob(input, output, name) {
        const params: ElasticTranscoder.CreateJobRequest = {
            PipelineId: '1572034412520-2mm5du',
            Input: { Key: input },
            OutputKeyPrefix: output,
            Outputs: [
                { PresetId: '1578927537583-0ob6fe', SegmentDuration: '10', Key: 'audio/audio' + name },
                { PresetId: '1587679275412-4d64t7', SegmentDuration: '10', Key: 'hd/mov' + name },
                // { PresetId: '1578930940508-73zc38', SegmentDuration: '10', Key: 'hd/mov' + name },
                { PresetId: '1578930956549-3wb29t', SegmentDuration: '10', Key: 'fhd/mov' + name },
            ],
            Playlists: [{ Name: 'index', Format: 'HLSv4', OutputKeys: ['hd/mov' + name, 'fhd/mov' + name, 'audio/audio' + name ] }],
        };

        return await this.transcoder.createJob(params).promise();
    }

    async createAudioJob(input, output, name) {
        const params: ElasticTranscoder.CreateJobRequest = {
            PipelineId: '1572034412520-2mm5du',
            Input: { Key: input },
            OutputKeyPrefix: output,
            Outputs: [{ PresetId: '1578927537583-0ob6fe', SegmentDuration: '10', Key: name }],
        };

        return await this.transcoder.createJob(params).promise();
    }

    async getJobStatus(Id) {
        return await this.transcoder.readJob({ Id }).promise();
    }
}
