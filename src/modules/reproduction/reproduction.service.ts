import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Reproduction } from './interfaces/reproduction.interface';
import { CreateReproduction } from './DTO/create-reproduction';

@Injectable()
export class ReproductionService {
    constructor(@InjectModel('reproduction') private ReprocutionModel: Model<Reproduction>) { }


    //list to repodcution
    async getReproduction(query?) {
        console.log(query)
        const { limit, skip, options } = query;
        return await this.ReprocutionModel.find(options)
            .limit(Number(limit) > 100 ? 100 : Number(limit))
            .skip(Math.abs(Number(skip)) || 0);
    }
    //create new repoduction
    async postReproduction(Reproduction: CreateReproduction): Promise<Reproduction> {
        const reproductionDB = await new this.ReprocutionModel(Reproduction);
        console.log(reproductionDB)
        return await reproductionDB.save();
    }
}
