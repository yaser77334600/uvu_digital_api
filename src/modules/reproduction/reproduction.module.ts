import { Module } from '@nestjs/common';
import { ReproductionService } from './reproduction.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ReproductionSchema } from './schemas/reproduction.schemas';
import { ReproductionController } from './reproduction.controller';

@Module({
 imports:[MongooseModule.forFeature([{name:'reproduction',schema: ReproductionSchema}])],
  providers: [ReproductionService],
  controllers:[ReproductionController]
})
export class ReproductionModule {}
