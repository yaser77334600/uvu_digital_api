import { Controller, Get, UseGuards, Query, Post, UseFilters, Body } from '@nestjs/common';
import { ReproductionService } from './reproduction.service';
import { AuthGuard } from '../../common/guard/auth.guard';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { MatchQueryPipe } from '../../common/pipes/match-query.pipe';
import { RoleGuard } from '../../common/guard/role.guard';
import { UuidPipe } from '../../common/pipes/uuid.pipe';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { CreateReproduction } from './DTO/create-reproduction';
import { Reproduction } from './interfaces/reproduction.interface';
@Controller('reproduction')
export class ReproductionController {
    constructor(private reproductionAPI: ReproductionService) { }
    
    @Get()
    @UseGuards(AuthGuard)
    async getPeople(@Query(new MatchQueryPipe(['videoView', 'watchTime'])) query): Promise<{ reproduction: Reproduction[], }> {
        console.log(query)
        const reproduction = await this.reproductionAPI.getReproduction(query);
        return { reproduction };
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    //  @UseGuards(AuthGuard, new RoleGuard(['ADMIN', 'CREATOR']))
    async CreateReproduction(@Body(new UuidPipe(),
        new PickPipe(['idCotenido', 'videoView', 'watchTime', 'fecha'])) reproduction: CreateReproduction): Promise<Reproduction> {
        console.log(reproduction)
        return await this.reproductionAPI.postReproduction(reproduction);
    }

}
