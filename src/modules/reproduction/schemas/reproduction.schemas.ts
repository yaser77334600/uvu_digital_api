import { Schema } from 'mongoose';

export const ReproductionSchema = new Schema({
    idCotenido:{
        type: String,
        required: true,
        unique: true,
    },
    videoView:{
        type: Boolean,
        required: true,
    },
    watchTime:{
        type: Number,
        required: true,
    },
    fecha:{
        type: Date,
        required: true,
    }
});
