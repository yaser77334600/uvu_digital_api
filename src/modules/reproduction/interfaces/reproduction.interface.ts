import { Pagination } from '../../../common/interfaces/pagination.interface';
import { Document } from 'mongoose';
export interface Reproduction extends Document, Pagination {
    idCotenido: string;
    videoView: boolean;
    watchTime: number;
    fecha: Date;
}
