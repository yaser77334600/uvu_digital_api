import { IsNotEmpty, IsOptional, MaxLength, IsArray, IsDateString, IsString, IsNumber, Max, Min } from 'class-validator'


export class CreateReproduction {

    @IsNotEmpty()
    idCotenido: string;

    @IsNotEmpty()
    videoView: boolean;

    @IsNotEmpty()
    @IsNumber()
    watchTime: number;

    @IsNotEmpty()  
    fecha: Date;

}