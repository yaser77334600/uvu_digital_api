import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { JwtErrorFilter } from './common/filters/jwt-error.filter';
import * as helmet from 'helmet';
import * as compression from 'compression';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(helmet());
  app.enableCors();
  app.useGlobalFilters(new JwtErrorFilter());
  app.useGlobalPipes(new ValidationPipe());
  app.use(compression());
  await app.listen(process.env.PORT);
}
bootstrap();
