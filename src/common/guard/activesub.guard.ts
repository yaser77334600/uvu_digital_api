import { CanActivate, ExecutionContext, Injectable, UnauthorizedException, NotFoundException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { UsersService } from '../../modules/users/users.service';
import { SubscriptionsService } from '../../modules/subscriptions/subscriptions.service';

@Injectable()
export class ActivesubGuard implements CanActivate {
  constructor(private userAPI: UsersService, private subscribeAPI: SubscriptionsService) {}
  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    const resp = context.switchToHttp().getResponse();
    if (!request.token) { throw new UnauthorizedException('Subscription expired or nonexistent', '7404'); }
    const userToken = request.token.user;
    // if (userToken.role === 'ADMIN') { return true; }
    // Check if have active subscription
    const user = await this.userAPI.getUser({ _id: userToken._id });

    if (!user) { throw new NotFoundException('User not found', '8404'); }
    if (!user.customer) { throw new UnauthorizedException('Subscription expired or nonexistent', '7404'); }
    await this.checkCustomer(user.customer);

    return true;
  }

  async checkCustomer(customerID) {
    const customer = await this.subscribeAPI.getCustomer(customerID);
    if (!customer) {
      throw new UnauthorizedException('Subscription expired or nonexistent', '7404');
    } else if (customer.subscriptions.data.length <= 0 || customer.subscriptions.data[0].status !== 'active') {
      throw new UnauthorizedException('Subscription expired or nonexistent', '7404');
    }
  }
}
