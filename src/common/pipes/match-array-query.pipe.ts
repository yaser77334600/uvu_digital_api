import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class MatchArrayQueryPipe implements PipeTransform {
  constructor(private keys: string[]) {}
  transform(value: any) {
    if (!value.hasOwnProperty('options')) {
      Object.assign(value, { options: {} });
    }
    this.keys.forEach(key => {
      if (value[key]) {
        const keysArr = value[key].split(',');
        value.options[key] = { $in: keysArr };
      }
    });
    return value;
  }
}
