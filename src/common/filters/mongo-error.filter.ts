import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { MongoError } from 'mongodb';

@Catch(MongoError)
export class MongoErrorFilter implements ExceptionFilter {
  catch(exception: MongoError, host: ArgumentsHost) {
    const response = host.switchToHttp().getResponse();
    switch (exception.code) {
      case 11000:
        response.status(400).json({ message: 'This entry already exists', error: { error: '0009' } });
        break;
      default:
        response.status(500).json({ message: 'Internal server error' });
        break;
    }
  }
}
