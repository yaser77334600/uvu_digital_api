import { CallHandler, ExecutionContext, Injectable, NestInterceptor, Logger } from '@nestjs/common';
import { Request } from 'express';
import { AuthService } from '../../modules/auth/auth.service';
import * as mongoose from 'mongoose';
import { UsersService } from '../../modules/users/users.service';

@Injectable()
export class LoggerInterceptor implements NestInterceptor {
  constructor(private authAPI: AuthService, private userAPI: UsersService) {}
  async intercept(context: ExecutionContext, next: CallHandler) {
    const HandlerName = context.getHandler().name;
    if (HandlerName === 'getSourceStream') {
        return next.handle();
    } else {
        const req: Request = context.switchToHttp().getRequest();
        const token = req.get('Authorization');
        try {
        const decoded = this.authAPI.verifyToken(token);
        const _id = (decoded as any).user._id;
        if (mongoose.Types.ObjectId.isValid(_id)) {
            const userDB = await this.userAPI.getUser({ _id });
            if (userDB) {
            userDB.lastActivity = new Date();
            userDB.save();
            }
        }
        } catch (e) { Logger.log(e); }
        return next.handle();
        }
    }
}
