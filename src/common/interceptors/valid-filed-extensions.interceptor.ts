import { CallHandler, ExecutionContext, Injectable, NestInterceptor, BadRequestException } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class ValidFiledExtensionsInterceptor implements NestInterceptor {
  constructor(private mimetypes: string[]) {}
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    if ('file' in context.switchToHttp().getRequest()) {
      const mime = context.switchToHttp().getRequest().file.mimetype;
      if (this.mimetypes.indexOf(mime) === -1) {
        throw new BadRequestException('Invalid file type', '4001');
      }
    }
    return next.handle();
  }
}
