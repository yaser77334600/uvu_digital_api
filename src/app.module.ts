import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClassificationsModule } from './modules/classifications/classifications.module';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './modules/auth/auth.module';
import { UsersModule } from './modules/users/users.module';
import { LanguagesModule } from './modules/languages/languages.module';
import { GenresModule } from './modules/genres/genres.module';
import { PersonsModule } from './modules/persons/persons.module';
import { MultimediaModule } from './modules/multimedia/multimedia.module';
import { FilesModule } from './modules/files/files.module';
import { SubscriptionsModule } from './modules/subscriptions/subscriptions.module';
import { TranscoderModule } from './modules/transcoder/transcoder.module';
import { TasksModule } from './modules/tasks/tasks.module';
import { ActivationModule } from './modules/activation/activation.module';
import { ReproductionModule } from './modules/reproduction/reproduction.module';


@Module({
  imports: [ClassificationsModule,
    MongooseModule.forRoot(process.env.URIDB,
      { useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true }),
    AuthModule,
    UsersModule,
    LanguagesModule,
    GenresModule,
    PersonsModule,
    MultimediaModule,
    FilesModule,
    SubscriptionsModule,
    TranscoderModule,
    TasksModule,
    ActivationModule,
    ReproductionModule,],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
