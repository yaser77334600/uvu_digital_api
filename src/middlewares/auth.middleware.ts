import { Injectable, NestMiddleware, BadRequestException } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    // const token = req.get('Authorization');
    // jwt.verify(token, this.config.get('SEED'), (err, decoded) => {
    //   if (err) { throw new BadRequestException('invalid token'); }
    //   req.token = decoded;
    //   next();
    // });
    next();
  }
}
